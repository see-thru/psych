import makeActionCreator from '../utils/make-action-creator'
import * as constants from '../constants/auth'
import * as endpoints from '../constants/endpoints'
import { auth, getAuthPersisted } from '../firebase'
import axiosWithAuth from '../api/axios-with-auth';
import urlGenerator from '../api/url-generator'
import { setFCUser } from '../utils/fresh-chat'

// Actions creators:
export const toggleSignInModal = makeActionCreator(constants.TOGGLE_SIGN_IN, 'modalIsOpen', 'continueTo')
export const toggleSignUpModal = makeActionCreator(constants.TOGGLE_SIGN_UP, 'modalIsOpen', 'continueTo')
export const toggleForgotPasswordModal = makeActionCreator(constants.TOGGLE_FORGOT_PASSWORD, 'modalIsOpen')
export const authLoading = makeActionCreator(constants.AUTH_LOADING)
export const loginFailed = makeActionCreator(constants.LOGIN_FAILED, 'error')
export const signupFailed = makeActionCreator(constants.SIGNUP_FAILED, 'error')
export const passwordResetFailed = makeActionCreator(constants.PASSWORD_RESET_FAILED, 'error')
export const passwordResetDone = makeActionCreator(constants.PASSWORD_RESET_DONE, 'data')
export const loggedIn = makeActionCreator(constants.LOGGED_IN)
export const loggedOut = makeActionCreator(constants.LOGGED_OUT)
// export const setToken = makeActionCreator(constants.SET_TOKEN, 'token') // unused
export const userFailed = makeActionCreator(constants.USER_FAILED, 'error')
export const setUser = makeActionCreator(constants.SET_USER, 'data')

const getAuthErrorMessage = e => {
  switch(e.code){
    case 'auth/user-not-found':
      return 'There is no user with that email. Find the link to sign up for SeeThru below.'
    case 'auth/email-already-exists':
      return 'This email address is already in use.'
    default:
      return e.message || 'Sorry, there was an error. PLease try again or contact help@seethru.healthcare.'
  }
}

// async actions 
export const login = (e, history) => {
  const { email, password, rememberMe } = e
  return async (dispatch, getState) => {
    try {
      if(!email || !password){
        dispatch(loginFailed("There was an error logging you in. Please try again or contact help@seethru.healthcare."))
      } else {
        dispatch(authLoading())
        const auth = await getAuthPersisted(rememberMe)
        const userCredential = await auth.signInWithEmailAndPassword(email, password)
        const ax = await axiosWithAuth(userCredential.user)
        const response = await ax.get(urlGenerator(endpoints.USER))
        
        const { continueTo } = getState().AuthReducer // get this before clearing it with setUser()
        setFCUser(response.data.user)
        dispatch(setUser(response.data.user))
        if(continueTo && history){
          history.push(continueTo)
        }
      }
    } catch (e) {
      console.error(e)
      dispatch(loginFailed(getAuthErrorMessage(e)))
    }
  }
}

export const signup = (e, history) => {
  const { email, password, firstName, lastName, dob, rememberMe, phone } = e
  return async (dispatch, getState) => {
    try {
      if(!email || !password){
        dispatch(signupFailed('There was an error signing you up. Please try again or contact help@seethru.healthcare.'))
      } else {
        dispatch(authLoading())
        const auth = await getAuthPersisted(rememberMe)
        const userCredential = await auth.createUserWithEmailAndPassword(email, password)
        const ax = await axiosWithAuth(userCredential.user)
        let newUser = { email, firstName, lastName, dob, phone }
        // catch this one and delete firebase user if it fails:
        const response = await ax.post(urlGenerator(endpoints.CREATE_USER), newUser)
      
        const { continueTo } = getState().AuthReducer
        setFCUser(response.data.user)
        dispatch(setUser(response.data.user))
        // history.push('/intake-form')
        if(continueTo && history){
          history.push(continueTo)
        }
      }
    } catch (e) {
      console.error(e)
      dispatch(signupFailed(getAuthErrorMessage(e))) 
    }
  }
}

export const logout = history => {
  return dispatch => {
    dispatch(authLoading())
    return auth.signOut()
      .catch(error => console.error(error))
      // .then(() => {
      //   if (history && history.push) { // why?
      //     history.push('/')
      //   } else {
      //     window.location.replace('/')
      //   }
      //   // dispatch(loggedOut()) // no need. firebase listener will handle AuthReducer.isLogged
      // })
  }
}

// export const refreshToken = () => {
//   return dispatch => {
//     auth.currentUser.getIdToken()
//       .then( idToken => { 
//         dispatch(setToken(idToken))
//         return idToken
//       })
//       .catch( error => dispatch(loggedOut()) )
//   } 
// }

export const sendPasswordReset = email => {
  const url = window.location.origin
  return dispatch => {
    dispatch(authLoading())
    return auth.sendPasswordResetEmail(email, { url }) // set to open login in URL. Needs work
      .then(data => {
        console.log('passwordResetDone', data)
        dispatch(passwordResetDone('Your password has been reset successfully. Check your email for further instructions.')) 
      })
      .catch(error => {
        console.error(error)
        dispatch(passwordResetFailed(getAuthErrorMessage(error)))
      })
  }
} 