import axios from 'axios'
import makeActionCreator from '../utils/make-action-creator'
import * as constants from '../constants/blog'

// Actions creators:
export const getBlogsLoading = makeActionCreator(constants.GET_BLOGS_LOADING)
export const getBlogsSuccess = makeActionCreator(constants.GET_BLOGS_SUCCESS, 'data')
export const getBlogsFail = makeActionCreator(constants.GET_BLOGS_FAIL, 'error')

const url = "https://cdn.contentful.com/spaces/zsfwrd1whoeg/environments/master/entries?access_token=01c0f056f5fea76e7f37d29a8489f7a8f877d79447c2bf78603f32a75995c574&include=1"

export const getBlogs = (dispatch) => {
    const blogsDataCombined = []
    return async dispatch => {
    dispatch(getBlogsLoading())
    axios.get(url)
        .then(response => {
            const blogs = response.data.items
            const assets = response.data.includes.Asset
            blogs.forEach(blog => {
                const blogimgUrl = assets.find(asset => {
                    return asset.sys.id === blog.fields.heroImage.sys.id
                })
                blog.heroURL = blogimgUrl.fields.file.url
                blogsDataCombined.push(blog)
             })
             console.log(blogsDataCombined)
            dispatch(getBlogsSuccess(blogsDataCombined))
        })
        .catch(error => {
            console.error(error)
            dispatch(getBlogsFail('There was an error with the server'))
        })
    }

}
