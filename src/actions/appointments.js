import axiosWithAuth from '../api/axios-with-auth'
import urlGenerator from '../api/url-generator'
import makeActionCreator from '../utils/make-action-creator'
import * as endpoints from '../constants/endpoints'
import * as constants from '../constants/appointments'
import trackAction from '../utils/analytics'
import { setAppointment } from './provider';
// Actions creators:
export const bookingLoading = makeActionCreator(constants.BOOKING_LOADING)
export const bookingFailed = makeActionCreator(constants.BOOKING_FAILED, 'error')
export const bookingDone = makeActionCreator(constants.BOOKING_DONE)
export const checkInLoading = makeActionCreator(constants.CHECK_IN_LOADING)
export const checkInFailed = makeActionCreator(constants.CHECK_IN_FAILED, 'error')
export const checkInDone = makeActionCreator(constants.CHECK_IN_DONE, 'data')
export const appointmentsLoading = makeActionCreator(constants.APPOINTMENTS_LOADING)
export const appointmentsFailed = makeActionCreator(constants.APPOINTMENTS_FAILED, 'error')
export const setAppointments = makeActionCreator(constants.SET_APPOINTMENTS, 'data')

export const bookAppointment = (appointment, stripeSource, appointmentDetails) => {
  return async dispatch => {
    try {
      const url = urlGenerator(endpoints.CREATE_APPOINTMENT)
      // check if totalCost and stripeSource
      // create proper data object. Like providerId
      const { totalCost, services, provider } = appointment
      let data = { totalCost, services, providerId: provider._id, details: appointmentDetails }
      if(stripeSource){
        data.payment = {
          source: stripeSource.id
        }
      }
      const axios = await axiosWithAuth()
      const response = await axios.post(url, data)
      const appointmentId = response.data.appointmentId
      Object.assign(appointment, { _id: appointmentId, booked: true, details: appointmentDetails })
      dispatch(setAppointment(appointment)) // only clear this when we go back to the provider page...?
      const link = `https://admin.seethru.healthcare/appointments/${appointmentId}/show`
      trackAction('Booked Appointment', { 
        category: 'Appointments',
        label: link,
        detail: appointment.provider.providerInfo.name,
        link
      })
      return appointmentId
    } catch(error) {
      console.error(error)
      dispatch(bookingFailed('There was an error booking the appointment. Please try again or contact team@seethru.healthcare.')) 
    }
  }
}

export const loadAppointments = () => {
  return async dispatch => {
    try {
      const axios = await axiosWithAuth()
      const response = await axios.get(urlGenerator(endpoints.LOAD_APPOINTMENTS))
      dispatch(setAppointments(response.data.appointments))
      return true
    } catch(error) {
      return Promise.reject(error.message)
    }
  }
}

export const checkIn = (appointmentId) => {
  return async dispatch => {
    try {
      const axios = await axiosWithAuth()
      const response = await axios.post(`${ urlGenerator(endpoints.CHECK_IN) }/${ appointmentId }`)
      // dispatch(checkInDone(response.data.appointment)) <- this should replace the current appointment in state
      // dispatch(checkInDone(response.data.appointment))
    } catch(e) {
      console.error('check in failed', e)
      // dispatch(checkInFailed(error.message))
    }
  }
}