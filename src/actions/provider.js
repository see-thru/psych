import axios from 'axios'
import { toggleSignUpModal } from './auth'
import urlGenerator from '../api/url-generator'
import makeActionCreator from '../utils/make-action-creator'
import { getErrorMessage } from '../utils/errors'
import trackAction, { PLATFORM } from '../utils/analytics';
import * as endpoints from '../constants/endpoints'
import * as constants from '../constants/provider'
// Actions creators:
export const setProvider = makeActionCreator(constants.SET_PROVIDER, 'data')
export const setProviderServices = makeActionCreator(constants.SET_PROVIDER_SERVICES, 'data')
export const providerLoading = makeActionCreator(constants.PROVIDER_LOADING)
export const providerFailed = makeActionCreator(constants.PROVIDER_FAILED, 'error')
export const toggleService = makeActionCreator(constants.TOGGLE_SERVICE, 'index')
export const toggleDrawer = makeActionCreator(constants.TOGGLE_DRAWER)
export const setAppointment = makeActionCreator(constants.SET_APPOINTMENT, 'appointment')

export const getProvider = id => {
  const providerUrl = `${ urlGenerator(endpoints.PROVIDER) }/${ id }`
  const servicesUrl = `${ urlGenerator(endpoints.PROVIDER) }/${ id }/services`
  return (dispatch, getState) => {
    dispatch(providerLoading())
    axios.get(providerUrl)
      .then(response => {
        dispatch(setProvider(response.data))
        if(response.data && response.data.providerInfo){
          trackAction('Viewed Provider', {
            detail: response.data.providerInfo.name,
            link: window.location.href
          }, PLATFORM.ST, PLATFORM.FC)
        }
      })
      .catch( e => {
        console.error(e.response)
        dispatch(providerFailed(getErrorMessage(e.response) || 'There was an error finding this provider.'))
      })
    axios.get(servicesUrl)
      .then(response => {
        let services = (response.data.services || []).map((s, index) => {
          s.index = index
          if(s.isDefault){
            s.selected = true
            s.locked = true
          }
          return s
        })
        dispatch(setProviderServices(services))
      })
      .catch ( e => {
        console.error(e.response)
        if(!getState().ProviderReducer){
          dispatch(providerFailed(getErrorMessage(e.response) || 'There was an error loading the provider‘s services'))
        }
      })
  }
}

// helper for us. not used by the ui
export const getProviderIds = () => {
  const url = urlGenerator(endpoints.PROVIDER_ALL)
  return axios.get(url)
    .catch(e => {
      console.error(e)
    })
}

export const createAppointment = (history, services = []) => {
  return (dispatch, getState) => {
    const { isLogged } = getState().AuthReducer
    
    const { data: p } = getState().ProviderReducer
    const provider = { _id: p._id, providerInfo: p.providerInfo }
    const selected = services.map(s => { // could be empty
      return { _id: s._id, name: s.name, cost: s.cost, time: s.time, includes: s.includes }
    })
    const totalCost = selected.reduce((total, s) => total + (s.cost || 0), 0)
    const totalTime = Math.min(selected.reduce((total, s) => total + (s.time || 0), 0) || 15, 60)
    // create appointment details here.
    const appointment = {
      provider,
      services: selected,
      totalCost, 
      totalTime,
    }
    dispatch(setAppointment(appointment))
    if(!isLogged){
      dispatch(toggleSignUpModal(true, '/book'))
    } else {
      history.push('/book')
    }
  }
}