import axios from 'axios'
import urlGenerator from '../api/url-generator'
import makeActionCreator from '../utils/make-action-creator'
import * as endpoints from '../constants/endpoints'
import * as constants from '../constants/search'
import trackAction from '../utils/analytics'
import { isArrayLength } from '../utils/arrays'

// Actions creators:
export const searchLoading = makeActionCreator(constants.SEARCH_LOADING)
export const setSearchResults = makeActionCreator(constants.SET_SEARCH_RESULTS, 'providers', 'additionalProviders', 'noMoreResults', 'clearResults')
export const searchFailed = makeActionCreator(constants.SEARCH_FAILED, 'error')

export const setFilters = makeActionCreator(constants.SET_TAGS, 'state', 'searchText', 'tags')
export const setUsState = makeActionCreator(constants.SET_US_STATE, 'state')
export const clearFilters = makeActionCreator(constants.CLEAR_FILTERS)

const searchUrl = urlGenerator(endpoints.SEARCH_PROVIDERS)
const search = (dispatch, state, searchText, tags, skip = 0, limit = 25) => {
  dispatch(searchLoading())
  const data = { state, searchText, tags, skip, limit }
  if(!skip){ 
    trackAction('Searched Providers', { 
      category: 'Search',
      detail: `“${searchText || ''} (${state})”`,
      link: window.location.href
    })
  }
  axios.post(searchUrl, data).then(response => {
    const { providers, additionalProviders } = response.data
    var noMoreResults = !providers || (providers.length < limit)
    // if skip, concat. Else replace
    dispatch(setSearchResults(providers, additionalProviders, noMoreResults, !skip))
  }).catch( error => {
    console.error(error)
    dispatch(searchFailed('There was an error searching the providers')) 
  })
}

export const newSearch = (state, searchText, tags = {}) => {
  return dispatch => {
    dispatch(setUsState(state))
    dispatch(setFilters(state, searchText, tags))
    search(dispatch, state, searchText, tags)
    window.scrollTo(0, 0)
  }
}

export const toggleTag = tag => {
  return (dispatch, getState) => {
    const { state, searchText, tags: oldTags } = getState().FiltersReducer
    let catTags = oldTags[tag.category] ? [...oldTags[tag.category]] : [] // copy the old array for this category
    if(isArrayLength(catTags)){
      const i = catTags.indexOf(tag.option)
      if(i > -1){
        catTags.splice(i, 1)
      } else {
        catTags.push(tag.option)
      }
    } else {
      catTags = [ tag.option ]
    }
    const tags = {
      ...oldTags,
      [tag.category]: catTags
    }
    dispatch(newSearch(state, searchText, tags))
  }
}

export const loadMore = skip => {
  return (dispatch, getState) => {
    const { state, searchText, tags } = getState().FiltersReducer
    search(dispatch, state, searchText, tags, skip)
  }
}

export const getUsState = () => {
  const url = urlGenerator(endpoints.GET_US_STATE)
  return async dispatch => {
    try {
      const response = await axios.get(url)
      if(response.data.state){
        dispatch(setUsState(response.data.state))
      }
    } catch(error) {
      console.error('Error loading current US state', error)
    }
  }
}