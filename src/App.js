import React from 'react'
import { Provider } from 'react-redux'
import { Router, Route, Switch } from 'react-router-dom'
import { PersistGate } from 'redux-persist/lib/integration/react'
import history from './history'
import { store, persistor } from './store'
import { GlobalStyle } from './styles/global'
import { MuiPickersUtilsProvider } from 'material-ui-pickers'
import MomentUtils from '@date-io/moment'

////////////////////////////////
// Generic Components
////////////////////////////////
import PrivateRoute from './components/PrivateRoute'
import SignInModal from './components/modals/signin/modal'
import SignUpModal from './components/modals/signup/modal'
import ForgotPasswordContainer from './components/modals/forgot-password/container'

import Footer from './components/footer/container'
import ScrollToTop from './components/common/ScrollToTop'

////////////////////
// Platform
////////////////////
import Header from './components/header/container'
import Filters from './components/search/filters/container'
import HomeContainer from './components/home/container'
import ProviderContainer from './components/provider/show/container'
import BookAppointmentContainer from './components/provider/book/container'
import Services from './components/provider/services/container'
import SearchContainer from './components/search/results/container'
import AppointmentsContainer from './components/appointments/container'
import CalculatorContainer from './components/calculator/container'
import ProvidersContainer from './components/providers/container'
import BlogIndex from './components/blog/BlogIndex'
import BlogItem from './components/blog/BlogItem'
import ClinitianZoneContainer from './components/provider/clinicianZone/container'
import NoMatch from './components/common/NoMatch'

// add the icons we're going to need
import { library } from '@fortawesome/fontawesome-svg-core'
import { 
  faChevronDown, faChevronUp, faChevronLeft, faMapMarkerAlt, faSearch, faInfoCircle, 
  faCalendarAlt, faClock, faCheck, faPlus, faLock, faSpinner, faFilter, faTimes
} from '@fortawesome/free-solid-svg-icons'
import { faCalendarCheck, faCalendarPlus } from '@fortawesome/free-regular-svg-icons'

import { withStyles } from '@material-ui/core';
import AboutPage from './components/static/AboutPage';
import ProviderFaqPage from './components/static/ProviderFaqPage';
import FAQPage from './components/static/FAQPage';
import IntakeForm from './components/IntakeForm/IntakeForm';
import ThankYou from './components/IntakeForm/ThankYou';
import newsletterSubscribe from './components/static/newsletter';

library.add(
  faChevronDown, faChevronUp, faChevronLeft, faMapMarkerAlt, faSearch, faInfoCircle, 
  faCalendarAlt, faClock, faCheck, faPlus, faLock, faSpinner, faFilter, faTimes,
  faCalendarCheck, faCalendarPlus
)

window.SeeThru = { stackSize: 0 }

const MainRoute = ({ children }) => (
  <Route path="/" 
    render={ props => (
      <main style={props.match.isExact ? {backgroundColor: '#517990'} : {padding: '0 10px'}}>
        { children }
      </main>
    )} />
)

const styles = theme => ({
  root: {
    display: 'flex',
    position: 'relative',
    flexWrap: 'wrap',
    [theme.breakpoints.up('sm')]: {
      flexWrap: 'nowrap'
    },
  },
  content: {
    flexGrow: 1,
    // padding: theme.spacing.unit * 3,
  },
})

// create sidebars here. with route
const App = ({ classes }) => {
  // use redux state for mobileOpen, or useContext. so that we don't have to pass it through. dispatch..
  return (
    <Provider store={ store }> 
      <PersistGate loading={ null } persistor={ persistor }>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <Router history={ history }>
            <ScrollToTop { ...history }>
              <GlobalStyle />
              <Header />
              <div className={classes.root}>
                {/* <Route exact path="/search" component={Filters} /> */}
                <div className={`full-height-container ${classes.content}`}>
                  <MainRoute>
                    <Switch>
                      <Route exact path="/" component={ HomeContainer } />
                      <Route exact path="/provider/:providerId" component={ ProviderContainer } />
                      <Route exact path="/provider-zone" component={ ClinitianZoneContainer } />
                      <PrivateRoute exact path="/book" component={ BookAppointmentContainer } />
                      <Route exact path="/search"	component={ SearchContainer } />	
                      <PrivateRoute exact path="/appointments" component={ AppointmentsContainer } />
                      <Route path="/calculator"	component={ CalculatorContainer } />	
                      <Route path="/for-providers"	component={ ProvidersContainer } />	
                      <Route path="/about"	component={ AboutPage } />	
                      <Route path="/faq"	component={ FAQPage } />	
                      <Route path="/provider-faq"	component={ ProviderFaqPage } />		
                      <Route exact path="/intake-form"	component={ IntakeForm } />		
                      <Route exact path="/intake-form/thank-you"	component={ ThankYou } />	
                      <Route exact path="/blog" component={ BlogIndex } />	
                      <Route exact path="/newsletter" component={ newsletterSubscribe } />	
                      <Route path="/blog/:blogId" component={ BlogItem } />	
                      <Route component={ NoMatch } />
                    </Switch>
                  </MainRoute>
                  <Footer />
                </div>
                <Route exact path="/provider/:providerId" component={Services} />
              </div>
              <SignInModal history={history} />
              <SignUpModal history={history} />
              <ForgotPasswordContainer />
            </ScrollToTop>
          </Router>
        </MuiPickersUtilsProvider>
      </PersistGate>
    </Provider>
  )
}

export default withStyles(styles)(App)
