import axios from 'axios'
import { store } from '../store'
import { loggedOut } from '../actions/auth'
import { getCurrentUser } from '../firebase'

const axiosWithAuth = async user => {
  try {
    if(!user){
      user = await getCurrentUser()
    }
    if(user){
      const idToken = await user.getIdToken()
      var instance = axios.create({ headers: { Authorization: `Bearer ${idToken}` } })
      instance.defaults.headers.post['Content-Type'] = 'application/json'
      return instance
    } else {
      // called axiosWithAuth, but no user! might be in trackAction
      store.dispatch(loggedOut()) // he might already be logged out...?
      // return Promise.resolve(axios)
      return Promise.reject({ error: 'User is not logged' });
    }
  } catch(e){
    console.error(e)
    return Promise.reject({ error: 'Error getting user authorization for the request' })
  }
  
}

export default axiosWithAuth