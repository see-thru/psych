import getApiHost from './get-api-host'

const urlGenerator = (endpoint = '', domain) => {
  return `${ domain || getApiHost(window.location.host) }${ endpoint }`
}

export default urlGenerator