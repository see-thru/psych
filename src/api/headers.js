const checkIfIsAStringOrNumber = value => {
  return typeof value === 'string' || typeof value === 'number'
}

export const generateHeaders = (contentType = 'application/json') => {
  return {
    'Content-Type': contentType,
  }
}

const generateAuthHeaders = (token, contentType = 'application/json') => {
  if(token){
    if(checkIfIsAStringOrNumber(token)){
      return {
        'Content-Type': contentType,
        'Authorization': `Bearer ${ token }`
      }
    } else {
      console.error('The `token` parameter should be an string or number')
    }
  }
  return generateHeaders(contentType);
}

export default generateAuthHeaders