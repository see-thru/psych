import * as HOSTS from '../constants/hosts'

// For deployed project, we serve the functions through the hosting's /api/ mount point (see firebase.json)
// For local development, we serve the functions through a proxy at localhost:5000 (firebase serve --only functions)
const getApiHost = baseUrl => {
  switch (baseUrl) {
    case HOSTS.LOCAL1:
    case HOSTS.LOCAL2:
      return '/'
    default:
      return '/api/'
  }
}

export default getApiHost