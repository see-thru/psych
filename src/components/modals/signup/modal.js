import React from 'react'
import { connect } from 'react-redux'
import { BootstrapModal } from '../styles'
import SignUpContent from './content'
import { signup, toggleSignUpModal, toggleSignInModal } from '../../../actions/auth'

const SignUpModal = ({
  history,
  errors,
  isOpen,
  loading,
  signup,
  toggleSignUpModal,
  toggleSignInModal
}) => {
  const closeModal = () => toggleSignUpModal(false)  
  const showSignInModal = () => {
    closeModal()
    toggleSignInModal(true)
  }
  const doSignup = e => signup(e, history)
  return (
    <BootstrapModal
      isOpen={ isOpen }
      onRequestClose={closeModal}
      contentLabel="Sign Up">
      <SignUpContent
        errors={ errors }
        loading={ loading }
        onSubmit={ doSignup }
        showSignInModal={ showSignInModal } />
    </BootstrapModal>
  )
}

const mapStateToProps = (state) => ({
  isOpen : state.AuthReducer.signUpModalIsOpen,
  errors : state.AuthReducer.errors,
  loading: state.AuthReducer.loading
})

export default connect(
  mapStateToProps,
  { 
    signup,
    toggleSignUpModal,
    toggleSignInModal
  }
)(SignUpModal)