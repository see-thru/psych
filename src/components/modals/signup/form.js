import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import { createTextMask } from 'redux-form-input-masks'
import { email, length, required, confirmation, date } from 'redux-form-validators'
import { FormGroup, Label } from 'reactstrap'
import { Input, FormWrapper } from '../../common/form'
import { Checkbox } from '../../common/reactstrap'
import { compose } from 'redux';

const phoneMask = createTextMask({
  pattern: '(999) 999-9999',
  allowEmpty: true,
  stripMask: false,
  guide: true
})

const dobMask = createTextMask({
  pattern: '99/99/9999',
  allowEmpty: true,
  stripMask: false,
  guide: false
})

// const styles = theme => ({
//   textField: {
//     marginLeft: theme.spacing.unit,
//     marginRight: theme.spacing.unit,
//     backgroundColor: 'rgba(255,255,255,.5)',
//   },
// })

// // const renderTextField = withStyles(styles)(({
// //   classes,
// //   input,
// //   label,
// //   meta: { touched, error },
// //   ...custom
// // }) => (
// //   <TextField
// //     fullWidth
// //     label={label}
// //     error={touched && !!error}
// //     helperText={touched && error}
// //     className={classes.textField}
// //     margin="normal"
// //     variant="filled"
// //     {...input}
// //     {...custom}
// //   />
// //   )
// // )

const SignupForm = ({
  hasPasswordValue,
  onSubmit,
  handleSubmit
}) => (
  <FormWrapper id="signUpForm" onSubmit={ handleSubmit(onSubmit) }>
    <Field
      label="Email address"
      title="Email address"
      placeholder="Email address"
      name="email"
      component={ Input }
      type="email"
      validate={ [
        email(),
        length({ max: 255 }),
        required()
      ] }
    />
    <Field
      label="First name"
      title="First name"
      placeholder="First name"
      name="firstName"
      component={ Input }
      validate={ [
        required()
      ]}
    />
    <Field
      title="Last name"
      label="Last name"
      placeholder="Last name"
      name="lastName"
      component={ Input }
      validate={ [
        required()
      ] }
    />    
    <Field
      title="Password"
      label="Password"
      placeholder="Password"
      name="password"
      component={ Input }
      type="password"
      validate={ [
        length({ minimum: 6 }),
        required()
      ] }
    />
    { hasPasswordValue &&
      <Field
        title="Password"
        placeholder="Confirm Password"
        name="confirm"
        component={ Input }
        type="password"
        validate={ [
          required(),
          confirmation({ field: 'password' })
        ] }
      />
    }
    <Field
      title="Date of Birth"
      label="Date of Birth"
      name="dob"
      placeholder="Date of Birth (mm/dd/yyyy)"
      component={ Input }
      validate={ [
        date({ format: 'mm/dd/yyyy' }),
        required()
      ] }
      { ...dobMask }
    />
    <Field
      title="Phone Number"
      label="Phone Number"
      placeholder="Phone number"
      name="phone"
      placeholder = "(xxx)-(xxx)-(xxxx)"
      component={ Input }
      validate={ [
        required()
      ] }
      { ...phoneMask }
    />
    <FormGroup check>
      <Label check>
        <Field
          name="remember"
          type="checkbox"
          component={ Checkbox } /> Remember me
      </Label>
    </FormGroup>
  </FormWrapper>
)

SignupForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

const selector = formValueSelector('signUpForm')

export default compose(
  reduxForm({
    form: 'signUpForm'
  }),
  connect(state => {
    const hasPasswordValue = selector(state, 'password')
    return {
      hasPasswordValue
    }
  })
)(SignupForm)