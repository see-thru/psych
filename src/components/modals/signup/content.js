import React from 'react'
import PropTypes from 'prop-types'
import Form from './form'
import { ErrorMessage } from '../../common/form'
import { Loader, SubmitButton } from '../../common/elements'
import { ModalBlue, ModalHeader, ModalFooter, ModalLink } from '../styles'

const SignUpContent = ({
  errors,
  onSubmit,
  showSignInModal,
  loading
}) => (
  <ModalBlue className="modal-content">
    { loading ? <Loader /> : undefined }
    <div className="modal-body">
      <ModalHeader>
        <h1>Sign Up</h1>
        <img 
          src="/images/logo-white.png" 
          alt="Seethru Logo"
          height="40" />
      </ModalHeader>
      <div className="modal-content-text">
        <p>Book your transparent care now.</p>
      </div>
      { errors.signup && <ErrorMessage lightColor>{ errors.signup }</ErrorMessage> }
      <Form onSubmit={ onSubmit } />
      <ModalFooter>
        <div>
          <p>Already registered? <ModalLink onClick={showSignInModal}>Log in</ModalLink></p>
        </div>
        <SubmitButton type="submit" form="signUpForm">Sign Up</SubmitButton>
      </ModalFooter>
    </div>
  </ModalBlue>
)

SignUpContent.propTypes = {
  errors: PropTypes.object,
  onSubmit: PropTypes.func.isRequired,
  showSignInModal: PropTypes.func.isRequired,
  loading: PropTypes.bool
}

export default SignUpContent