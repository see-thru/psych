import React from 'react'
import { connect } from 'react-redux'
import { BootstrapModal } from '../styles'
import { login } from '../../../actions/auth'
import { toggleSignInModal, toggleSignUpModal, toggleForgotPasswordModal } from '../../../actions/auth'
import SignInContent from './content'

const SignInModal = ({
  history,
  errors, 
  isOpen,
  loading,
  login,
  toggleSignInModal,
  toggleSignUpModal,
  toggleForgotPasswordModal
}) => {
  // React says it's okay. But we could memoize these with useCallback
  const closeModal = () => toggleSignInModal(false)
  const showSignUpModal = () => {
    closeModal() // this is done automatically by the action, right?
    toggleSignUpModal(true)
  }
  const showForgotPasswordModal = () => {
    closeModal() // this is done automatically by the action, right?
    toggleForgotPasswordModal(true)
  }
  const doLogin = e => login(e, history)
  return (
    <BootstrapModal
      isOpen={ isOpen }
      onRequestClose={closeModal}
      contentLabel="Sign In">
      <SignInContent
        errors={ errors }
        loading={ loading }
        onSubmit={doLogin}
        showSignUpModal={ showSignUpModal }
        showForgotPasswordModal={ showForgotPasswordModal } />
    </BootstrapModal>
  )
}

const mapStateToProps = (state) => ({
  errors: state.AuthReducer.errors,
  isOpen: state.AuthReducer.signInModalIsOpen,
  loading: state.AuthReducer.loading
})

export default connect(
  mapStateToProps,
  { 
    login,
    toggleSignInModal, 
    toggleSignUpModal,
    toggleForgotPasswordModal
  }
)(SignInModal)