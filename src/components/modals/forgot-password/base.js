import React from 'react'
import PropTypes from 'prop-types'

import { Icon, SubmitButton } from '../../common/elements'
import { ErrorMessage } from '../../common/form'
import Form from './form'
import { ModalBlue, ModalHeader, ModalFooter, ModalLink } from '../styles'

const ForgotPasswordContent = ({
  messages,
  onSubmit,
  closeModal,
  showSignInModal
}) => (
  <ModalBlue className="modal-content">
    <div className="modal-body">
      <ModalHeader>
        <h2>Forgot Password</h2>
        <button className="close" onClick={ closeModal }>
          <Icon 
            fill="#fff"
            width="16"
            name="cancel"
            height="16" />
        </button>
      </ModalHeader>
      { messages.error && <ErrorMessage lightColor>{ messages.error }</ErrorMessage> }
      { messages.success ? <ErrorMessage lightColor>{ messages.success }</ErrorMessage> : (
        <Form onSubmit={ onSubmit } />
      )}
      <ModalFooter>
        <div>
          <p><ModalLink onClick={showSignInModal}>Back</ModalLink></p>
        </div>
        { messages.success ? (
          <SubmitButton onClick={closeModal}>Close</SubmitButton> 
        ) : (
          <SubmitButton type="submit" form="forgotPasswordForm">Submit</SubmitButton>
        )}
      </ModalFooter>
    </div>
  </ModalBlue>
)

ForgotPasswordContent.propTypes = {
  errors: PropTypes.object,
  onSubmit: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  showSignInModal: PropTypes.func.isRequired
}

export default ForgotPasswordContent