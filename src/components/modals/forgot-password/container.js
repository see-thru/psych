import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { BootstrapModal } from '../styles'
import { toggleSignInModal, toggleForgotPasswordModal, sendPasswordReset } from '../../../actions/auth'
import ForgotPasswordContent from './base'

export class ForgotPasswordModal extends Component {
  onSubmit = e => {
    this.props.sendPasswordReset(e.email)
  }

  closeModal = () => {
    this.props.toggleForgotPasswordModal(false)
  }

  showSignInModal = () => {
    this.closeModal()
    this.props.toggleSignInModal(true)
  }

  render () {
    const { isOpen, messages } = this.props
    return (
      <BootstrapModal
        isOpen={ isOpen }
        onRequestClose={this.closeModal}
        contentLabel="Forgot Password" >
        <ForgotPasswordContent 
          isOpen={ isOpen }
          messages={ messages }
          onSubmit={ this.onSubmit }
          closeModal={ this.closeModal }
          showSignInModal={ this.showSignInModal } />
      </BootstrapModal>
    )
  }
}

ForgotPasswordModal.propTypes = {
  isOpen: PropTypes.bool,
  messages: PropTypes.object,
  toggleSignInModal: PropTypes.func.isRequired,
  toggleForgotPasswordModal: PropTypes.func.isRequired,
  sendPasswordReset: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  isOpen : state.AuthReducer.resetModalIsOpen,
  messages : state.AuthReducer.resetModal
})

export default connect(
  mapStateToProps,
  { 
    toggleSignInModal,
    toggleForgotPasswordModal,
    sendPasswordReset
  }
)( ForgotPasswordModal )