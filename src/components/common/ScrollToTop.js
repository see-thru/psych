import { useEffect } from 'react'
import { withRouter } from 'react-router-dom'

const ScrollToTop = ({ location, children }) => {
  useEffect(() => {
    if(location.pathname !== '/search'){ // if location pathname changes when we update the filters (it is not atm, but we handle this manually there)
      window.scrollTo(0, 0)
    }
  }, [location])
  return children
}

export default withRouter(ScrollToTop)