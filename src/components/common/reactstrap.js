import React from 'react'
import styled from 'styled-components'
import { Container as BSContainer, Input } from 'reactstrap'

export const Checkbox = ({ input: { value, onChange } }) => (
  <Input type="checkbox" checked={!!value} onChange={onChange} />
)

export const Container = styled(BSContainer)`
  margin-top: 30px;
  margin-bottom: 15px;
`