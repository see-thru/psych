import React from 'react'
import classNames from 'classnames';
import Drawer from '@material-ui/core/Drawer'
import Button from '@material-ui/core/Button'
import Hidden from '@material-ui/core/Hidden'
import { withStyles } from '@material-ui/core'
import MenuButton from '@material-ui/icons/Menu';
import { values } from '../../utils/settings'
import { useToggleField } from '../../hooks/useMergeState';

const drawerWidth = 240; // make this variable with styled-components, or inline styles?

const styles = theme => ({
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  filtersContainer: {
    padding: '10px 10px 0 20px'
  },
  drawerPaper: {
    width: drawerWidth,
    paddingTop: '40px',
    [theme.breakpoints.up('sm')]: {
      height: `calc(100% - ${values.headerHeight})`,
      marginTop: values.headerHeight,
      paddingTop: '0',
      zIndex: 1,
    },
  },
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

const DrawerContainer = ({ classes, anchor, children }) => {
  const [mobileOpen, toggleMobileOpen] = useToggleField()
  return (
    <nav className={classes.drawer}>
      <Hidden smUp implementation="css">
        <Drawer
          variant="temporary"
          anchor={anchor}
          open={mobileOpen}
          onClose={toggleMobileOpen}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          { children }
        </Drawer>
        <Button variant="outlined" size="small" className={classes.button} onClick={toggleMobileOpen}>
          <MenuButton className={classNames(classes.leftIcon, classes.iconSmall)} />
          Filters
        </Button>
      </Hidden>
      <Hidden xsDown implementation="css">
        <Drawer
          classes={{
            paper: classes.drawerPaper,
          }}
          variant="permanent"
          anchor={anchor}
          open
        >
          { children }
        </Drawer>
      </Hidden>
    </nav>
  )
}

export default withStyles(styles)(DrawerContainer)