import styled from 'styled-components'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { createElement as e } from 'react'
import { colors, fonts } from '../../../utils/settings'

export const DefaultButton = styled(({tag, children, ...props}) => e(tag, props, children))`
  background-color: transparent;
  border: 0;
  border-radius: 0;
  text-decoration: none;
  &:focus {
    outline: 0;
  }
`
DefaultButton.defaultProps = {
 tag: 'button'
}
DefaultButton.propTypes = {
  tag: PropTypes.oneOf(['button', 'a', Link])
}

export const SubmitButton = styled(DefaultButton)`
  background-color: ${ colors.darkButton }; 
  border-radius: 4px;
  color: white;
  font-size: 13px;
  font-weight: ${ fonts.weightBold };
  transition: .35s background-color;
  padding: 8px 32px;
  line-height: 20px;
  margin: 0 10px;
  &:hover {
    background-color: ${ colors.blue };
    color: white;
  }
  &[disabled] {
    background-color: rgba(0, 70, 121, 0.5);
    pointer-events: none;
  }
  svg {
    vertical-align: middle;
  }
`

// TODO: Move this to a prop in submit button
export const SubmitButtonScale = styled(SubmitButton)`
  background-color: ${ colors.blue }
  transition: transform .2s ease-out;
  &:hover {
    transform: scale(1.05);
    background-color: ${ colors.blue }
  }
  
`