import React from 'react'
import { withRouter, Link } from 'react-router-dom'

export const HistoryLink = withRouter(({ to, history, staticContext, ...rest }) => (
  <Link to={to} replace={history.location.pathname === to} {...rest} />
))