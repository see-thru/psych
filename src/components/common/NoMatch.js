import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { GridContainer } from './layout';
import { SubmitButton } from './elements';
import Media from '../../utils/media'
import { fonts, colors } from '../../utils/settings'

const StyledNotMatch = styled.div`
  max-width: 1000px;
  margin: 20px auto;
  h3, strong {
    font-weight: ${ fonts.weightBold };
  }
  h3 {
    color: ${ colors.successButton };
    font-size: 1.12rem;
    margin-bottom: 10px;
    text-transform: uppercase;
    ${ Media.medium`
      font-size: .9rem;
    ` }    
  }
  p {
    font-size: 1.25rem;
    margin-top: 0;
    ${ Media.medium`
      font-size: 1rem;
    ` }
  }
  a {
    display: inline-block;
    margin-top: 10px;
    text-align: center;
    width: 180px;
  }
`

const NoMatch = () => (
  <StyledNotMatch>
    <GridContainer>
      <img 
        alt=""
        src="/images/404.png"
        width="100%" />
      <SubmitButton
        to="/"
        tag={ Link }>
        Back to Home page
      </SubmitButton>
    </GridContainer>
  </StyledNotMatch>
)

export default NoMatch