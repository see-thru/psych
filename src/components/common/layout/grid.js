import PropTypes from 'prop-types'
import styled from 'styled-components'

export const GridContainer = styled.div`
  display: grid;
  grid-gap: ${ props => props.gridGap };
  grid-template-rows: ${ props => props.templateRows };
  grid-template-areas: ${ props => props.templateAreas };
  grid-template-columns: ${ props => props.templateColumns };
  justify-items: ${ props => props.justifyItems };
`

GridContainer.propTypes = {
  gridGap: PropTypes.string,
  templateRows: PropTypes.string,
  templateAreas: PropTypes.string,
  templateColumns: PropTypes.string,
  justifyItems: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.oneOf([
      'stretch', 
      'center', 
      'start', 
      'end'
    ])
  ])
}

export const GridItem = styled.div`
  align-self: ${ props => props.alignSelf };
  grid-row: ${ props => props.gridRow };
  grid-area: ${ props => props.gridArea };
  grid-column: ${ props => props.gridColumn };
  justify-self: ${ props => props.justifySelf };
`

GridItem.propTypes = {
  alignSelf: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.oneOf([
      'auto',
      'stretch',
      'center',
      'flex-start',
      'flex-end',
      'baseline',
      'initial',
      'inherit'
    ])
  ]),
  gridRow: PropTypes.string,
  gridArea: PropTypes.string,
  gridColumn: PropTypes.string,
  justifySelf: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.oneOf([
      'stretch', 
      'center', 
      'start', 
      'end'
    ])
  ])
}