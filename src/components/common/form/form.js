import React from 'react'
import PropTypes from 'prop-types'

const FormWrapper = ({
  onSubmit,
  id,
  children
}) => (
  <form id={id}
    onSubmit={ onSubmit }
    autoComplete="off">
    { children }
  </form>
)

FormWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  onSubmit: PropTypes.func.isRequired
}

export default FormWrapper
