import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import styled from 'styled-components'

import { colors } from '../../../utils/settings'

const StyledInput = styled.input`
  font-size: .9rem;
  &.is-invalid {
    border: 2px solid #cc4b57;
  }
  ::placeholder {
    color: ${ colors.placeholderFields };
  }
`

const Input = ({
  meta: { touched, error },
  type,
  title,
  input,
  label,
  placeholder
}) => (
  <div className="form-group">
    { label && 
      <label htmlFor={ input.name }>{ label }</label>
    }
    <StyledInput
      title={ title }
      name={ input.name }
      type={ type }
      className={classnames('form-control', {
        'is-invalid': touched && error,
      })}
      placeholder={ placeholder }
      { ...input } />
    { touched && error &&
      <small className="form-text text-danger">{ error }</small>
    }
  </div>
)

Input.defaultProps = {
  type: 'text'
}

Input.propTypes = {
  meta: PropTypes.object.isRequired,
  type: PropTypes.string,
  title: PropTypes.string.isRequired,
  input: PropTypes.shape({
    name: PropTypes.string
  }).isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string
}

export default Input
