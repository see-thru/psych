import React from 'react'
import PropTypes from 'prop-types'

import StyledLabel from './styles'

const Label = ({ 
  htmlFor, 
  children
}) => (
  <StyledLabel htmlFor={ htmlFor }>
    { children }
  </StyledLabel>
)

Label.propTypes = {
  children: PropTypes.node.isRequired,
  htmlFor: PropTypes.string
}

export default Label