import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { getBlogs } from '../../actions/blog'
import PropTypes from 'prop-types'
import { withStyles, Grid } from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  blogsContainer: {
    margin:"auto", 
    
  },
  gridItem: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    margin:"20px 0 20px 20px",
  },
  title:{
    color: "#004679",
    marginBottom:"1em"
  },
  button: {
    marginTop: "2em",
    marginBottom:"1.5em",
    textAlign: "center !important",
  },
  img:{
    width:"95%",
    margin:"auto",
    marginBottom:"1em"
  },
  description:{
    paddingRight:"2em",
    paddingLeft:"0.5em"
  }

})

class BlogIndex extends Component {
  static propTypes = {
    history: PropTypes.object,
  }
  
    componentDidMount() {
      const { getBlogs } = this.props
      getBlogs()
      }
  
    onClick = (blog) => {
      const { history } = this.props
      history.push({ pathname: `/blog/${blog.fields.slug}`, state: {currentBlog: blog}})
    }

    render() {
      const { blogPosts, classes } = this.props
      const sortedBlogPosts = blogPosts.sort((a,b) => {
        const dateA = new Date(a.fields.published).getTime() 
        const dateB = new Date(b.fields.published).getTime()
        if(dateA < dateB) {
          return 1
        } 
        if(dateA > dateB) {
          return -1
        }
        return 0
      })
        return(
            <React.Fragment>
              <Grid container spacing={24} alignItems="streach">
                {
                  sortedBlogPosts.map((blog) => {
                    return (
                    <Grid item xs={12} sm={6} md={4} key={blog.sys.id}>
                      <Paper className={classes.gridItem}>
                        <Typography className={classes.title} variant="h6">{blog.fields.title}</Typography>
                        <img className={classes.img} src={blog.heroURL} />
                        <Typography className={classes.description} variant="body1">{blog.fields.description}</Typography>
                        <Button 
                          className={classes.button} 
                          variant="contained" 
                          color="primary"
                          onClick={ () => this.onClick(blog) }
                          >Read More</Button>
                        <Typography variant="caption" >Date: {new Date(blog.fields.published).toDateString()}</Typography>
                      </Paper>
                    </Grid>
                    )
                })}
              </Grid>
            </React.Fragment>
        )
    }
  };

const mapStateToProps = (state) => ({
  error: state.BlogReducer.error,
  blogPosts: state.BlogReducer.blogPosts

})

export default compose(
    connect(
        mapStateToProps, 
        {getBlogs}
    ), 
    withStyles(styles)      
)(BlogIndex)