import React, { Component } from 'react'
import { connect } from 'react-redux'
import Markdown from 'markdown-to-jsx';

import { compose } from 'redux'
import PropTypes from 'prop-types'
import { withStyles, Grid } from '@material-ui/core'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
  blogContainer: {
    maxWidth: "600px", 
    margin: "auto", 
    padding: "2em 0"
  },
  dateBy: {
    marginBottom: "1em", 
    lineHeight: "1.5em"
  }
})

class BlogItem extends Component {

  static propTypes = {
    history: PropTypes.object,
  }


  render() {
    const { location, classes } = this.props
    console.log(location)
    const currentBlog = location.state.currentBlog
      return(
        <React.Fragment>
          <Grid container spacing={24} className={classes.blogContainer}>
          <Typography variant="caption" className={classes.dateBy}>On {new Date(currentBlog.fields.published).toDateString()}, By {currentBlog.fields.author}</Typography>
          <Typography variant="h3" style={{maxWidth:"600px"}}>{currentBlog.fields.title}</Typography>
            <div style={{margin:"2em 0", lineHeight:"1.5em"}}>
              <Markdown>{currentBlog.fields.content}</Markdown>
            </div>   
          </Grid>

        </React.Fragment>
        )
    }
  };

const mapStateToProps = (state) => ({
  blogPosts: state.BlogReducer.blogPosts

})

export default compose(
    connect(
      mapStateToProps
    ), 
    withStyles(styles)      
)(BlogItem)