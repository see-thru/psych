import React, { useState } from 'react';
import MailchimpSubscribe from "react-mailchimp-subscribe"

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid'

function useFormInput(initialValue) {
  const [value, setValue] = useState(initialValue);

  function handleChange(e) {
    setValue(e.target.value);
  }

  return {
    value,
    onChange: handleChange
  };
}

const Form = ({ status, message, onValidated }) => {
    
  const email = useFormInput("");
  const fname = useFormInput("");
  const lname = useFormInput("")

  const submit = () =>
    email &&
    fname &&
    lname &&
    email.value.indexOf("@") > -1 &&
    onValidated({
      EMAIL: email.value,
      FNAME: fname.value,
      LNAME: lname.value
    });



  return (
    <div>
      {status === "sending" && <div style={{ color: "blue" }}>sending...</div>}
      {status === "error" && (
        <div
          style={{ color: "red" }}
          dangerouslySetInnerHTML={{ __html: message }}
        />
      )}
      {status === "success" && (
        <div
          style={{ color: "green" }}
          dangerouslySetInnerHTML={{ __html: message }}
        />
      )}
      <TextField
        margin = "normal"
        label = "First Name"
        variant = "outlined"
        name="fname"
        // ref={node => (fname = node)}
        type="text"
        fullWidth
          InputLabelProps={{
            shrink: true,
          }} 
        {...fname}        
      />
      <br />
      <TextField
        margin = "normal"
        variant = "outlined"
        name= "lname"
        // ref={node => (lname = node)}
        type="text"
        label = "Last Name"
        fullWidth
          InputLabelProps={{
            shrink: true,
          }}  
        {...lname}
      />
      <br />
      <TextField
        margin = "normal"
        variant = "outlined"
        name= "email"
        // ref={node => (email = node)}
        type="email"
        label = "Email"
        fullWidth
          InputLabelProps={{
            shrink: true,
          }}  
        {...email}
      />
      <br />
      <Button style={{ fontSize: "2em", padding: 5, color:"#FFF", backgroundColor:"#0F1F4D", marginTop:"1em"}} variant="contained" fullWidth onClick={submit}>
        Subscribe
      </Button>
    </div>
  );
};

class newsletterSubscribe extends React.Component {
  render() {
    const url =
      "https://healthcare.us12.list-manage.com/subscribe/post?u=4dfab781dd628292b67be0896&amp;id=1e3e8949a2";
    return (
      <Grid container style={{padding:"1em", paddingBottom:"3em"}}>
        <Grid item xs={12} style={{marginLeft:"5px", marginTop:"2em"}}>
            <h3>Subscribe to Our Newsletter </h3>
        </Grid>
        <Grid item xs={12} sm={8} md={6}>
            <MailchimpSubscribe
            url={url}
            render={({ subscribe, status, message }) => (
                <Form
                status={status}
                message={message}
                onValidated={formData => subscribe(formData)}
                />
            )}
            />
        </Grid>
      </Grid>
    );
  }
}

export default newsletterSubscribe







{/* <form action="https://healthcare.us12.list-manage.com/subscribe/post?u=4dfab781dd628292b67be0896&amp;id=1e3e8949a2" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate> */}
