import React from 'react'
import { compose } from 'redux'
import { PageContainer } from '../common/layout'

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
  root: {
      width: '100%',
      marginBottom: "2em"
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '70%',
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    pageHeader: {
      margin: "2em 1em 1em 1.2em",
      color: "#214679"
    },
    pageSubHeader: {
      margin: "2em 1em 1em 2em",
      color: "#214679"
    },
    panel: {
      maxWidth: "800px",
      marginLeft: "2em",
      marginRight: "2em"
    }
})

class ProviderFaqPage extends React.Component {
  state = {
    expanded: null,
  };

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  render() {
    const { classes } = this.props;
    const { expanded } = this.state;

    return (
      <div className={classes.root}>
       <Typography variant="h5" className={classes.pageHeader}>Clinician Frequently Asked Questions</Typography>
       <Typography variant="subtitle1" className={classes.pageSubHeader}>Before Joining</Typography>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Why should I become a SeeThru clinician?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              With SeeThru, you can get rid of the hassles of running a practice and focus on taking care of your patients. 
              We handle all of the billing for SeeThru patients, and we help you fill your hours. 
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>How does SeeThru make money?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              At SeeThru, we don’t make money, unless you make money. There are no monthly or annual fees to be listed in our marketplace; 
              we collect a percentage of the amount that is billed for each visit 
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>How do patients find me?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Our clinician results populate based on a patient’s initial search terms and their location. 
              Once they are on the search results page, they can further refine their results using filters such as issues treated, 
              modalities utilized, or even clinician’s self-disclosed identities.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>What are the advantages of online therapy?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              The advantages of online therapy are endless. 
              First and foremost, patients are able to have their appointment wherever they feel most comfortable and don’ t have to worry about time spent commuting or sitting in a waiting room.
              They also have access to clinicians that could be physically located hours away from them, that they might never have access to otherwise.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Can I offer a sliding scale?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Yes! It is completely at your discretion if you want to offer a sliding fee scale; you set your prices!
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>I don't currently offer a telehealth option. Can I still be a SeeThru clinician?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Absolutely! We’d love to help you launch your practice into the future by offering a telehealth option.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Is a special license required to practice telemedicine?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              No. You can see patients in any state(s) in which you are currently licensed.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Can I set my own prices?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Yes! We will never dictate what your prices should be; you charge however much you want to make. 
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <Typography variant="subtitle1" className={classes.pageSubHeader}>After Joining</Typography>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>How do I set my availability?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              If you utilize SeeThru’s platform, you will be able to set your availability through there. 
              If you utilize a separate video platform, you will need to set your availability through your personal clinician liaison. 
              We are currently working on calendar integrations and hope to offer this feature very soon!
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>How and when do I get paid?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Your payments will be directly deposited into whatever checking or savings account you specify via ACH (direct deposit). 
              After the first visit you complete, payment will be deposited into your account within 3 - 5 business days. 
              Once you have received the first  payment from us, future payments should be received within 1 - 3 business days from the date of the patient session.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Am I able to screen my patients for appropriateness?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Yes! We require all clinician/patient partnerships to start with a free 10-minute consultation. This gives the patient a chance to meet you and see if they feel like you’re the right fit for them. 
              This also gives you the chance to see if the patient is someone you feel you can help. We will follow up with you immediately after your consultation to see if you are comfortable moving forward with that patient.  
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Can I set my own cancellation policy?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Yes! This is your practice, we’re just here to bring you patients and make sure you get paid! 
              Your cancellation policy is 100% up to you and what works best for you, your practice, and your patients.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>How do I contact my patients?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              If you utilize SeeThru’s telehealth platform, secure messaging is a built-in feature.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>How will I know if I have an appointment request?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Your personal clinician liaison will contact you whenever there is an appointment request.             
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    );
  }
}

export default compose(
  withStyles(styles)
)(ProviderFaqPage)