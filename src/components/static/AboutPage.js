import React from 'react'
import { compose } from 'redux'
import { withStyles, Typography } from '@material-ui/core'
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { PageContainer } from '../common/layout'

const styles = theme => ({
  root: {
      flexGrow: 1,
    },
    header: {
      color: "#214679",
      marginTop: theme.spacing.unit *10,
      marginBottom: theme.spacing.unit * 2,
      fontSize: "2.5em",
      lineHeight:"1.5em",
      letterSpacing: "1px"
    },
    title:{
      color: "#214679",
      marginBottom: "0.75em",
      marginTop:"1em"
    },
    section:{
      margin:"auto",
      width: "70%",
      fontSize:"1.1em",
      lineHeight:"2em",
      marginBottom: "3em"
    },
    contactUs:{
      width:"70%",
      margin: "auto",
      marginTop:"3em",
      lineHeight: "1.5em",
      marginBottom:"3em",
      backgroundColor: ""
    },
    paragraph:{
      marginBottom: "1.5em",
      lineHeight:"1.25em",
      color: "#1D181A"
    }
})

const AboutPage = ({ classes }) => {
  return (
    <PageContainer>
      <Typography className={classes.header} align="center" component="h1" variant="overline"><b>We Believe<br/> That Health Care Can Be Done<br/> Differently</b></Typography>
      <Grid   
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
        style={{ margin:"2em 0 3em 1.5em"}}>
        <img src="/images/jill-and-joe.png" alt="SeeThru provider and patient" style={{maxWidth: "500px"}}/>
      </Grid>
        <Typography className={classes.title} align="center" component="h6" variant="h4"><b>OUR STORY</b></Typography>
        <Typography className={classes.section} component="h6" variant="body1">
          <p className={classes.paragraph}>
            On his way to medical school SeeThru founder Adi Segal, was required to submit immunity detection and titer testing blood work.
            These labs were not covered on his high deductible health plan purchased on the Connecticut exchange.<br/>
          </p>
          <p className={classes.paragraph}>
            Being a poor student with not much in the way of savings, Adi spent two weeks calling every lab, primary care office, and urgent care center within a 20-mile radius of his home to find the best price. 
          </p>
          <p className={classes.paragraph}>
            To his disbelief, the cost of these tests varied by as much as a $100! Having spent his previous year studying the cost of cancer treatments he knew that most of the labs were all being processed in the same few regional facilities, and all the tests cost the same to conduct.
          </p>
          <p className={classes.paragraph}>
            At that moment, Adi realized the extent to which the health care system was inaccessible and disenfranchising to patients. If he, a medical student and knowledgeable patient, had to spend days of research to find a vaguely affordable blood test, how much much more difficult must it be
            for others who are in emergent situations or do not have the privilege of such knowledge?
          </p>
          <p style={{ lineHeight:"1.25em" }}>
            How could anyone expect to find care that is accessible or affordable when they need it?
            He realized that we needed to create a better system. He dreamed of building a platform which empowered patients and let them have access to the care that was important to them.
          </p>
        </Typography>
        <Typography className={classes.title} align="center" component="h6" variant="h4"><b>OUR MISSION</b></Typography>
        <Typography className={classes.section} component="h6" variant="body1">
          <p className={classes.paragraph}>
            SeeThru's story began in 2015, with a search for care. Today, all of us at SeeThru have joined together to create a health care system which values each and every person. We at SeeThru are committed to using transparency and technology to empower patients and providers to create a platform that gives everyone the freedom to find value and ability to pursue their own health care. 
          </p>
          <p className={classes.paragraph}>
            To make that dream a reality we have taken the first step by building a price transparent marketplace which currently connects those in need of behavioral health care to the clinician best suited for them. To ensure that care is broadly accessible, we offer a video platform so that anyone can get the care they need when and where they want it.          
          </p>
        </Typography>
        <div className={classes.contactUs}>
          <Typography 
            align="left" 
            component="h6" 
            variant="h4"
            style={{color:"#214679"}}
            >Contact Us
          </Typography>
          <br/>
          <p>
            At SeeThru, we don’t give you the runaround. If you need help or have a question we are here for you.
            No B.S. email addresses or impossible to find phone numbers that lead to an answering machine.
            If you want us, send an email to <a href="mailto:partners@seethru.health care" target="_top">partners@seethru.healthcare</a>, and a real person will get back to you within 1 business day.
          </p>
        </div>
    </PageContainer>
  )
}

export default compose(
  withStyles(styles)
)(AboutPage)