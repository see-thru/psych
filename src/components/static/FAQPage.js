import React from 'react'
import { compose } from 'redux'
import { PageContainer } from '../common/layout'

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
  root: {
      width: '100%',
      marginBottom: "2em"
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '70%',
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    pageHeader: {
      margin: "2em 1em 1em 1.2em",
      color: "#214679"
    },
    panel: {
      maxWidth: "800px",
      marginLeft: "2em",
      marginRight: "2em"
    }
})

class FAQPage extends React.Component {
  state = {
    expanded: null,
  };

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  render() {
    const { classes } = this.props;
    const { expanded } = this.state;

    return (
      <div className={classes.root}>
       <Typography variant="h5" className={classes.pageHeader}>Frequently Asked Questions</Typography>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>What is SeeThru?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              SeeThru is a one-stop marketplace for behavioral health care which utilizes price transparency, instant billing, and an integrated video platform to make care more accessible and affordable for patients and more streamlined for clinicians. 
              Our marketplace brings behavioral health care the value-based experience found in every other industry.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>I don’t really understand how your service works. Who can help me?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              At SeeThru, we don’t give you the runaround. 
              If you need help or have a question, we are here for you. There are no B.S. email addresses or impossible to find phone numbers that lead to an answering machine. 
              If you want us, send an email to <a href="mailto:team@seethru.health care" target="_top">team@seethru.health care</a>, and a real person will get back to you within 1 business day.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Who are the clinicians?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              The clinicians on SeeThru’s platform are the same types of clinicians you’d find if you searched on your own. 
              Some of them work with larger practices, while others are independent clinicians. 
              We believe that you deserve the right to choose the clinician that is right for you, even if they live on the other side of the country. 
              Rest assured, all of our clinicians are licensed behavioral health professionals.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>How are the clinicians verified?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Once a clinician requests to join our platform, they go through an extensive enrollment process. 
              We verify their credentials, state licensure, current standing, and past experiences. All of their licenses and certifications must be current.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>What are the advantages of online therapy?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              The advantages of online therapy are endless. First and foremost, you are able to have your appointment wherever you feel most comfortable and don’t have to worry about time spent commuting or sitting in a waiting room. 
              You also have access to clinicians that could be physically located hours away from you that you might never have access to otherwise.             
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Can I use my insurance?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              We are working hard to allow you to utilize your insurance benefits seamlessly. 
              Unfortunately at this time, we are not able to take your insurance benefits into account, and you are required to pay for any visit up front. 
              However, we will work with you to get the documentation needed to submit to your insurance company for reimbursement if it is applicable for your plan.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Can I use my FSA/HSA to pay for my appointment?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              <b>Yes!</b> You can utilize your FSA or HSA benefits card to pay for services rendered through the SeeThru platform.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>How do I book a therapy session?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              First, search to find clinicians that specialize in the type of treatment or issues you are seeking. 
              Next, filter through the results to find a clinician that is best for you. 
              Then, book a free 10 - minute consultation with that clinician to make sure you’re the right fit for each other. After your consultation, your clinician will work with you to get your first appointment scheduled.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>How does telemedicine work?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Telemedicine works like any other healthcare visit; it just happens over a video platform. 
              All you need is a computer, tablet, or smartphone with a camera to complete your visit. 
              Once your appointment is booked, you will receive step-by-step instructions on how to log in to your appointment at the scheduled time. 
              It shoudlnt take long  anddon’t worry, if you have any issues, we’re here to help! 
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>When is my payment processed?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              A hold for the cost of your appointment will be placed on the card you provided up to 48 hours prior to your scheduled appointment time. 
              The transaction will not be fully processed until immediately following your appointment. 
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>What is the cancellation policy?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Each clinician has their own cancellation policy. Some are strict while others are more lenient. 
              Please refer to your clinician’s profile for more in-depth details about their cancellation policy. 
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Can my child or teenager book an appointment?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              As we are in the very early stages of our platform and only helping patients 18 years old or older. 
              However, we will be working on making sure minors and dependants can be seen on our platform as soon as possible.  
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Why does it matter what state I'm in?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Healthcare clinicians are licensed by states (and can be licensed in multiple states); they are only allowed to treat patients in states in which they are licensed. 
              If you live in New York, you need to see a clinician that is licensed in New York. 
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>What do the letters after the clinicians' names mean?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              You’ll quickly notice our clinicians have letters after their names like LPC, LCSW, LMSW, and so on. These letters represent the clinicians professional credentials, here are what some of the most common mean:<br/><br/>
            <ul>
              <li>P.N.H.M.P. - B.C.: Psychiatric Mental Health Nurse Practitioner Board Certified </li>
              <li>P.N.H.M.P.: Psychiatric Mental Health Nurse Practitioner</li>
              <li>A.P.R.N.: Advanced Practice Registered Nurse</li>
              <li>L.M.S.W.: Licensed Master Social Worker</li>
              <li>L.C.S.W.: Licensed Clinical Social Worker</li>
              <li>L.P.C.: Licensed Professional Counselor</li>
              <li>L.M.H.C.: Licensed Mental Health Counselor </li>
              <li> L.M.F.T: Licensed Marriage and Family Therapist </li>
              <li>L.A.C.: Licensed Addictions Counselor</li>
              <li>L.C.A.C.: Licensed Clinical Addictions Counselor</li>
              <li>M.S.W.: Master of Social Work</li>
              <li>B.A.: Behavioral Assistant</li>       
            </ul>
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel className={classes.panel}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>What type of clinician do I need?</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              The type of clinitian you need depends on what you want to get from them. For counseling, you’ll want to seek out a practitioner who treats what you’re seeking. 
              Use our search engine filters to find the closest match to the issues you’re facing. 
              If you need medication in addition to counseling you’ll want to consider someone with a medical degree (DO or MD) or an nursing degree (PNHMP-BC, PNHMP, or APRN, NP).
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    );
  }
}

export default compose(
  withStyles(styles)
)(FAQPage)