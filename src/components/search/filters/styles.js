import styled from 'styled-components'
import { Button } from 'reactstrap';
import { colors } from '../../../utils/settings';
import { ContentBox } from '../../common/layout';

export const StyledFilterCategory = styled.div`
  ${ props => props.showAll ? `
  `: `
    li:not(.show-option) { display: none; }
  `}
  ul { 
    list-style: none;
    padding-left: 15px;
  }
    cursor: pointer;
    transition: color 200 ms linear; &
    : hover {
      color: $ {
        colors.darkBlue
      };
`

export const StyledSelectedFilter = styled.p`
  color: ${ colors.darkBlue };
  cursor: pointer;
  font-size: 0.9rem;
  strong {
    font-weight: 700;
  }
`

export const StyledFilterOption = styled.li`
  font-size: 0.9rem;
  position: relative;
  padding: 1px 0;
  ${ props => props.selected ? `
    color: ${ colors.darkBlue };
    font-weight: 700;
    &::after {
      content: 'x';
      color: #aaa;
      position: absolute;
      left: -13px;
      top: -1px;
    }
  ` : `
    color: ${colors.blue};
  `}
  cursor: pointer;
  transition: color 200ms linear;
  &:hover {
    color: ${ colors.darkBlue };
  }

  &.show-toggle {
    color: #ababab;
    font-weight: 200;
  }
  &.show-subcategory {
    color: #ababab;
  }
`

export const StyledButton = styled(Button)`
  &.btn-secondary {
    color: ${ colors.text };
    background: white;
    border-color: #ddd;
     
    &:focus,
    &:not(:disabled):not(.disabled):active,
    :not(:disabled):not(.disabled).active {
      color: ${ colors.text };
      background-color: #eee;
      border-color: #ddd;
      box-shadow: none;
    }
  }
`

export const StyledFilterElement = styled(ContentBox)`
  width: auto;
`