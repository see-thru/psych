import React from 'react'
import { Button, Dialog, DialogTitle, TextField, withStyles } from '@material-ui/core';

const styles = theme => ({
  buttonContainer: {
    marginTop: '10px',
    textAlign: 'right',
  },
  button: {
    margin: theme.spacing.unit,
  },
  content: {
    padding: '0 30px 10px'
  }
})

const FiltersDialog = ({ open, onClose, classes }) => {
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Search Providers</DialogTitle>
      <div className={classes.content}>
        <TextField
          id="standard-uncontrolled"
          label="Search"
          defaultValue=""
          className={classes.textField}
          margin="normal"
          fullWidth
        />
        <TextField
          id="standard-multiline-static"
          label="State"
          defaultValue=""
          className={classes.textField}
          margin="normal"
          fullWidth
        />
        <div className={classes.buttonContainer}>
          <Button className={classes.button} onClick={onClose}>Cancel</Button>
          <Button className={classes.button} variant="contained" color="primary" >Search</Button>
        </div>
      </div>
    </Dialog>
  )
}

export default withStyles(styles)(FiltersDialog)