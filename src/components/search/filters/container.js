import React from 'react';
import { connect } from 'react-redux'
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { useToggleState, useToggleField } from '../../../hooks/useMergeState'
import { compose } from 'redux';
import { filterCats } from '../../../constants/filters'
import { toggleTag } from '../../../actions/search'
import { StyledFilterCategory, StyledFilterOption, StyledSelectedFilter } from './styles'
import DrawerContainer from '../../common/DrawerContainer';
import FiltersDialog from './FiltersDialog';
import { isArrayLength } from '../../../utils/arrays';

const styles = {
  filtersContainer: {
    padding: '10px 10px 0 20px'
  },
}

const FiltersContainer = ({ classes, toggleTag, state, searchText, tags, mobileOpen, toggleMobileOpen }) => {
  const [showCategory, toggleField] = useToggleState({})
  const [modalOpen, toggleModal] = useToggleField()

  const renderOptions = (categoryId, options) => {
    let catTags = tags[categoryId] || []
    return options.map((option, index) => {
      if(isArrayLength(option.options)){
        return (
          <>
            <StyledFilterOption className="show-subcategory">
              { option.name }
            </StyledFilterOption>
            { renderOptions(categoryId, option.options) }
          </>
        )
      } else {
        let selected = catTags.indexOf(option.id) > -1
        return (
          <StyledFilterOption 
            key={index}
            selected={selected}
            className={option.show || selected ? 'show-option' : ''}
            onClick={() => toggleTag({category: categoryId, option: option.id})}>
            { option.name || option.id } 
          </StyledFilterOption>
        )
      }
    })
  }

  return (
    <>
      <DrawerContainer anchor="left" mobileOpen={mobileOpen} toggleMobileOpen={toggleMobileOpen}>
        <div className={classes.filtersContainer}>
          <Typography style={{color:"#1F5181", marginBottom: "10px", marginTop:"10px"}}>
            Your Search: 
          </Typography>
          { searchText ? <StyledSelectedFilter>Specialty: <strong>{ searchText }</strong></StyledSelectedFilter> : <StyledSelectedFilter>Specialty: <strong>All Specialties</strong></StyledSelectedFilter>
          }
            <StyledSelectedFilter>State: <strong>{ state }</strong></StyledSelectedFilter>
          {/* { searchText && 
            <div onClick={toggleModal}>
              <StyledSelectedFilter>Search: <strong>{ searchText }</strong></StyledSelectedFilter>
            </div>
          }
          <div onClick={toggleModal}>
            <StyledSelectedFilter>State: <strong>{ state }</strong></StyledSelectedFilter>
          </div> */}
          <Typography style={{color:"#1F5181", marginBottom: "10px", marginTop:"30px"}}>
            Filter Results By: 
          </Typography>
          { filterCats.map((category, index) => (
            <StyledFilterCategory 
              onClick={() => toggleField(category.id)} 
              key={index} showAll={category.showAll || showCategory[category.id]}
              >
              <Typography color="textSecondary">{ `${category.name} ${showCategory[category.id] ? ' -' : ' ⌄ '}` } </Typography>
              <ul>
                { category.showAll || !showCategory[category.id] || 
                  <StyledFilterOption
                    className="show-option show-toggle"
                    onClick={() => toggleField(category.id)}>   
                  </StyledFilterOption>
                }
                { renderOptions(category.id, category.options) }
                { category.showAll || 
                  <StyledFilterOption
                    className="show-option show-toggle"
                    onClick={() => toggleField(category.id)}>
                  </StyledFilterOption>
                }
              </ul>
            </StyledFilterCategory>
          ))}
        </div>
      </DrawerContainer>
      <FiltersDialog open={modalOpen} onClose={toggleModal} />
    </>
  )
}

FiltersContainer.defaultProps = {
  tags: {}
}

const mapStateToProps = (state) => ({
  state: state.FiltersReducer.currentUsState,
  searchText: state.FiltersReducer.searchText,
  tags: state.FiltersReducer.tags
})

export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    { toggleTag }
  )
)(FiltersContainer);