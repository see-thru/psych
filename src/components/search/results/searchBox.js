import React, { useCallback, useEffect } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { withRouter } from 'react-router-dom'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import { required } from 'redux-form-validators'
// import qs from 'qs'
import { newSearch, getUsState } from '../../../actions/search'
import { StyledInputs } from './styles'
import { BigInput, BigTypeahead } from './inputs'
import { usStates } from '../../../constants/filters';
import { Button, withStyles } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { colors } from '../../../utils/settings';

const styles = theme => ({
  button: {
    backgroundColor: colors.darkBlue,
    '&:hover': {
      backgroundColor: '#003861',
    },
    height: "50px",
    width:"20%",
    marginLeft:"5px",
    padding:"5px 80px"
  },
  buttonIcon: {

  },
  inlineForm: {
    display:"flex",
    flexFlow: "column",
    alignItems: "left",
    margin: "0 5px !important",
    maxWidth: "700px"
  }
})

const Form = ({ 
  classes,
  history,
  currentUsState,
  chosenUsState,
  getUsState,
  change,
  newSearch,
  handleSubmit
}) => {
  useEffect(() => {
    if(currentUsState){
      if(!chosenUsState) { change('state', currentUsState) }
    } else { // check if chosen?
      getUsState()
    }
  }, [currentUsState])

  const onSubmit = useCallback(e => {
    // const query = qs.stringify({ issue: e.searchText, state: e.state })
    const query = ''
    history.push({ 
      pathname: '/search',
      search: query ? `?${query}` : undefined
    })
    newSearch(e.state, e.searchText)
  }, [ history ]);
  
  return (
    <form
    className={classes.inlineForm}
      onSubmit={ handleSubmit(onSubmit) }
      autoComplete="off">
      <StyledInputs>
        <Field
        style={{width:"98%" }}
          name="searchText"
          component={ BigInput }
          placeholder="Specialty" 
          formText="e.g. Addiction, Depression, Couples Counseling" />
        <Field
        style={{width:"30%", marginRight:"2%" }}
          name="state"
          options={ usStates }
          component={ BigTypeahead }
          validate={ [ required() ] }
          placeholder="choose a state..." 
          formText="Where you will be receiving care" />
        <Button 
        variant="contained"  
        color="primary" 
        size="large" 
        type="submit"
        className={classes.button}
        >
        <SearchIcon className={classes.buttonIcon} />Search
      </Button>
      </StyledInputs>

    </form>
  )
}

const formName = 'bannerForm'
const selector = formValueSelector(formName)
const mapStateToProps = state => ({
  currentUsState: state.FiltersReducer.currentUsState,
  chosenUsState: selector(state, 'state')
})

export default compose(
  reduxForm(
    { form: formName }
  ),
  connect(
    mapStateToProps,
    { newSearch, getUsState }
  ),
  withRouter,
  withStyles(styles)
)(Form)