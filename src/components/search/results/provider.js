import React from 'react'
import PropTypes from 'prop-types'
import { Rating, ServiceCost, ProviderName } from '../../common/elements'
import { StyledProviderCard } from './styles'

const RatingSpecialties = ({ info }) => {
  let ratingSpecialties = []
  if(info.rating){
    ratingSpecialties.push(<Rating rating={info.rating} />)
  }
  if(info.specialty){
    ratingSpecialties.push(info.specialty)
  }
  return ratingSpecialties.length ? (
    ratingSpecialties
      .map((item, i) => <span key={i}>{ item }</span>)
      .reduce((el, item) => [el, ' | ', item])
   ) : null
}

const Provider = ({
  item,
  onSelect
}) => (
  <StyledProviderCard onClick={() => onSelect(item._id)}>
    <div className="provider-image">
      <img src={ item.providerInfo.photo } alt="Provider" />
    </div>
    <div>
      <div className="provider-detail">
        <div className="provider-title">
          <ProviderName info={item.providerInfo} tag="h2" />
        </div>
        <p className="provider-rating">
          <RatingSpecialties info={ item.providerInfo } />
        </p>
      </div>
      { !!item.baseCost &&
        <ServiceCost className="service-cost" cost={ item.baseCost } />
      }
    </div>
  </StyledProviderCard>
)

Provider.propTypes = {
  item: PropTypes.shape({
    providerInfo: PropTypes.shape({
      name: PropTypes.string.isRequired,
      photo: PropTypes.string,
      specialty: PropTypes.string
    }).isRequired,
  }).isRequired,
  active: PropTypes.bool,
  onHover: PropTypes.func,
  onSelect: PropTypes.func
}

export default Provider