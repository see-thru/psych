import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { loadMore } from '../../../actions/search'
import { isArrayLength } from '../../../utils/arrays'
import { AlertContainer } from '../../common/elements'
import Provider from './provider'
import { StyledResultsContainer, StyledLoadMore } from './styles'
import { withStyles, Grid } from '@material-ui/core';
import { compose } from 'redux';

import BannerSearchForm from './searchBox'
import Filters from '../filters/container'

const styles = {
  gridItem: {
    flexBasis: '100%'
  },
  searchBox: {
    width:"100%",
  }
}
class ResultsContainer extends Component {
  static propTypes = {
    history: PropTypes.object,
    results: PropTypes.array,
    loading: PropTypes.bool,
    noMoreResults: PropTypes.bool,
    loadMore: PropTypes.func.isRequired
  }
  static defaultProps = {
    results: []
  }

  componentDidMount(){
    const { loadMore, loading, results, noMoreResults } = this.props
    if(!isArrayLength(results) || loading || noMoreResults){
      loadMore(0)
    }
  }

  onProviderSelect = (providerId, service) => {
    if(!providerId) return
    const { history } = this.props
    const search = service && service.name ? `?select=${service.name}` : undefined
    history.push({ pathname: `/provider/${providerId}`, search })
  }

  onClickLoadMore = () => {
    const { loadMore, loading, results } = this.props
    if(loading){
      return
    }
    loadMore(results.length)
  }

  render(){
    const { classes, error, results, additionalResults, loading, noMoreResults } = this.props
    return (
      <Grid container>
      <Grid item sm={3}><Filters /></Grid>
      <Grid item sm={9}>
      <div className={classes.searchBox}>
        <BannerSearchForm />
      </div>
      <hr></hr>
        <StyledResultsContainer className="search-container">
          { error && <AlertContainer>{ error }</AlertContainer>}
          <div className="title">
            { isArrayLength(results) ? (
              <span className="result-count">Results:</span>
            ) : (
              <span style={{ marginBottom: "20px", color:"#214679"}}>{ loading ? 'Loading...' : 'We are sorry but no results found matching your filters. Please try to broaden your search' }</span>
            )}
          </div>
          <Grid container spacing={16} alignItems="stretch">
            { results.map((item, index) => (
              <Grid item sm={12} md={6} key={index} className={classes.gridItem}>
                <Provider key={ index } item={ item } onSelect={ this.onProviderSelect } />
              </Grid>
            ))}
          </Grid>
          { loading && 
            <div className="results-footer">
              <FontAwesomeIcon icon="spinner" spin size="lg" />
            </div>
          }
          { loading || noMoreResults || 
            <div className="results-footer">
              <StyledLoadMore onClick={ this.onClickLoadMore }>
                Load More
              </StyledLoadMore>
            </div>
          }        
          { isArrayLength(additionalResults) && 
            <>
              <div className="title">Other nearby providers:</div>
              <Grid container spacing={16} alignItems="stretch">
                { additionalResults.map((item, index) => (
                  <Grid item sm={12} md={6} key={index} className={classes.gridItem}>
                    <Provider key={ index } item={ item } onSelect={ this.onProviderSelect } />
                  </Grid>
                ))}
              </Grid>
            </>
          }
        </StyledResultsContainer> 
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = (state) => ({
  error: state.SearchReducer.error,
  results: state.SearchReducer.results,
  additionalResults: state.SearchReducer.additionalResults,
  loading: state.SearchReducer.loading,
  noMoreResults: state.SearchReducer.noMoreResults,
  hoveredMarker: state.SearchReducer.hoveredMarker
})

export default compose(
  connect(
    mapStateToProps, 
    { loadMore }
  ),
  withStyles(styles)
)(ResultsContainer)