import styled from 'styled-components'

import Media, { MaxWidth, breakpoints } from '../../../utils/media'
import { colors } from '../../../utils/settings'
import { SubmitButton } from '../../common/elements'
import { borderHover } from '../../common/styles'
import Card from '@material-ui/core/Card';
// import Media, { MaxWidth } from '../../utils/media';

export const StyledResultsContainer = styled.div`
  padding: 5px 0 20px;
  .title {
    color: #999;
    font-size: 1rem;
    padding: 5px 20px 10px;
    margin: 0;
    display: flex;
    justify-content: space-between;
    align-items: center;

    button {
      color: #8181d2;
      font-size: 0.8rem;
    }
    span {
      font-weight: 700;
    }
  }
  .results-footer {
    padding-top: 10px;
    text-align: center;
  }
`

export const StyledLoadMore = styled(SubmitButton)`
  margin: 0 auto;
`

export const StyledProviderCard = styled(Card)`
  padding: 15px 20px;
  position: relative;
  display: flex;
  align-items: flex-start;
  cursor: pointer;
  height: 100%;
  ${ borderHover }

  .provider-image {
    img {
      border-radius: 50%;
      object-fit: cover;
      height: 75px;
      width: 75px;
      margin-right: 15px;
    }
  }
  .provider-detail {
    font-size: .8rem;
    padding-right: 45px;
    .provider-title {
      h2 {
        font-size: 1.2rem;
        color: ${ colors.darkBlue };
        display: inline-block;
        font-weight: 600;
        margin-bottom: 3px;
      }
      span {
        font-size: .8rem;
        color: ${ colors.textMuted };
        margin-left: 5px;
        white-space: nowrap;
      }
    }
    .provider-rating {
      color: #666;
      font-weight: 700;
      margin-bottom: 2px;
    }
    .provider-address {
      color: #888;
      margin-bottom: .4rem;
      svg {
        font-size: .9rem;
        margin-left: 3px;
        margin-right: 5px;
      }
    }
  }
  .base-service {
    font-size: 0.95rem;
    margin-bottom: .5rem;
    svg { 
      color: ${ colors.textMuted }
      font-size: 0.8rem;
      margin-right: 5px;
    }
  }
  .service-cost {
    position: absolute;
    right: 10px;
    top: 10px;
  }

  ${ MaxWidth(breakpoints.extraSmall)`
    display: block;
    text-align: center;
    border-top-width: 3px;
    .provider-detail {
      padding: 0;
      .provider-title h2 {
        font-size: 1rem;
      }
    }
    .provider-image {
      img {
        margin: 0 0 15px;
      }
    }
  `}
`

// export const StyledBanner = styled.div `
//   background: linear-gradient(to bottom, #00507e, #517990);
//   position: relative;
//   padding: 40px 20px;
//   .banner-content {
//     color: white;
//     text-align: center;
//     max-width: 900px;
//     margin: 0 auto;
//     h1 {
//       font-size: 4.3rem;
//       line-height: 4.6rem;
//       text-shadow: 1px 1px rgba(0,0,0,0.4);
//     }
//     h2 {
//       font-weight: 700;
//       font-size: 1.5rem;
//       line-height: 1.8rem;
//       margin-top: 0;
//       text-shadow: 1px 1px rgba(0,0,0,0.4);
//     }
//     > p {
//       font-size: 1.3rem;
//       line-height: 1.6rem;
//       text-shadow: 1px 1px rgba(0,0,0,0.4);
//     }
//   }
//   > img {
//     max-width: 607px;
//     top: 15px;
//     position: relative;
//     width: 100%;
//     align-self: flex-end;
//   }
//   ${
//     Media.medium`
//       padding: 20px 10px;
//       .banner-content {
//         h1 {
//           font-size: 3.2rem;
//           line-height: 3.5rem;
//         }
//         h2 {
//           font-size: 1.3rem;
//           line-height: 1.5rem;
//         }
//         > p {
//           font-size: 1rem;
//           line-height; 1.2rem;
//         }
//       }
//     `
//   }
//   ${
//     MaxWidth(500)`
//       .banner-content {
//         h1 { 
//           font-size: 2.1rem;
//           line-height: 2.3rem;
//         }
//         h2 {
//           font-size: 1.1rem;
//           line-height: 1.3rem
//         }
//         > p {
//           font-size: .9rem;
//           line-height: 1rem
//         }
//       }
//     `
//   }
// `

export const StyledInputs = styled.div `
  display: flex;
  justify-content: space-between;
  position: relative;
  margin: 25px 0 0;
  
  > div {
    display: inline-block;
    flex-basis: 38%;
    &:first-of-type {
      flex-basis: 60%;
    }
  }
  ${
    MaxWidth(500)`
      display: block;
      > div {
        width: 100%;
      }
    `
  }
`