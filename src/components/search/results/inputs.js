import React from 'react'
import styled from 'styled-components'

import { Typeahead } from 'react-bootstrap-typeahead'
import { colors } from '../../../utils/settings'
import { FormGroup, FormFeedback, FormText, Input } from 'reactstrap'

const StyledFormGroup = styled(FormGroup)`
  text-align: left;
  input, .form-control {
    border-radius: 2px;
    font-size: 1rem;
    height: 50px;
    padding: 0 5px;
    input.rbt-input-main {
      padding: 12px 0 !important;
    }
    ::placeholder {
      color: ${ colors.placeholderFields } !important;
    }
    &.is-invalid {
      border: 2px solid #dc3545;
    }
  }
  .invalid-feedback { 
    display: block !important;
    color: #ff9ba5;
    font-size: 16px;
    text-shadow: 1px 1px rgba(0,0,0,0.6);
  }
`

export const BigInput = ({ input, meta, formText, ...props }) => (
  <StyledFormGroup>
    <Input { ...input } { ...props } />
    { formText && 
      <FormText color="#214679">{ formText }</FormText>
    }
  </StyledFormGroup>
)

export const BigTypeahead = ({ input, meta: { touched, error }, formText, ...props }) => {
  const { value, onChange } = input
  return (
    <StyledFormGroup>
      <Typeahead 
        selected={value ? [value] : null}
        onChange={selected => {
          onChange(selected[0] || '')
        }}
        isInvalid={ touched && error }
        clearButton={true}
        onFocus={event => {
          // https://stackoverflow.com/a/48479145/1396726
          const inputNode = event.target; 
          inputNode.setSelectionRange(0, inputNode.value.length);
        }}
        { ...props } />
      { touched && error &&
        <FormFeedback>* required field</FormFeedback>
      }
      { formText && 
        <FormText color="#214679">{ formText }</FormText>
      }
    </StyledFormGroup>
  )
}