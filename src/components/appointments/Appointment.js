import React, { useCallback, useEffect, useState } from 'react'
import { compose } from 'redux';
import { connect } from 'react-redux';
import classnames from 'classnames'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Typography, Button, Card, Grid, withStyles } from '@material-ui/core';
import { checkIn } from '../../actions/appointments';
import { isArrayLength } from '../../utils/arrays';
import { AppointmentTime, RequestedTime } from '../common/elements'
import ProviderInfo from './ProviderInfo';
import ServicesInfo from './ServicesInfo';

const styles = theme => ({
  card: {
    margin: '15px 0',
    padding: '15px 30px',
  },
  cardFaded: {
    // opacity: '.6',
    // pointerEvents: 'none',
    display: 'none'
  },
  cardContent: {
    padding: '10px'
  },
  appointmentDetails: {
    marginTop: '10px',
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
  },
  timesItem: {
  },
  buttonItem: {
    marginTop: '15px',
    [theme.breakpoints.up('sm')]: {
      textAlign: 'right',
      marginTop: 0,
    }
  }
})

const getCheckInState = (status, time, videoLink) => {
  var now = new Date()
  var diff = new Date(time).getTime() - (now.getTime() - now.getTimezoneOffset() * 60000) // < 1800000 (30 minutes)
  if (diff < 1800000) { // if less than 30 minutes away:
    if (diff < -3600000) { // if more than an hour ago:
      return 'expired'
    } else if (videoLink) {
      if((status === 'checkedIn') && (diff < 0)){
        return 'inProgress'
      } else if (status === 'confirmed') {
        return 'begin'
      } // status shouldn't be 'booked' if appt has a confirmedTime
    }
  } 
  return '' // if more than 30 minutes away, nothing to show/hide
}

const Appointment = ({ 
  classes,
  checkIn,
  appointment
}) => {
  const { _id, details, status, videoLink, services, totalCost, provider } = appointment
  const [session, setSession] = useState()
  useEffect(() => {
    if(details.confirmedTime){
      setSession(getCheckInState(status, details.confirmedTime, videoLink))
    }
  }, [appointment])

  const doCheckIn = useCallback(() => {
    window.open(videoLink, '_blank')
    checkIn(_id)
    setSession('inProgress') // this should happen through redux state // useEffect call ^
  }, [appointment])

  return (
    <Card className={classnames(classes.card, { [classes.cardFaded]: session === 'expired' })}>
      <Grid container spacing={8}>
        <Grid item xs={12} md={6}>
          <ProviderInfo info={provider.providerInfo} />
        </Grid>
        <Grid item xs={12} md={6}>
          <div className={classes.appointmentDetails}>
            { details.confirmedTime ? (
              <div className={classes.timesItem}>
                <div className="section-label"><FontAwesomeIcon icon="calendar-alt" /> Confirmed Time</div>
                <Typography><AppointmentTime time={details.confirmedTime} /></Typography>
              </div>
            ) : isArrayLength(details.times) && (
              <div className={classes.timesItem}>
                <div className="section-label"><FontAwesomeIcon icon="calendar-alt" /> Requested Time</div>
                { details.times.map((time, index) => (
                  <Typography key={index}><RequestedTime time={time} setUTC /></Typography>
                ))}
              </div>
            )}
            { session === 'begin' && (
              <div className={classes.buttonItem}>
                <Button variant="contained" color="primary" onClick={doCheckIn}>Begin session</Button>
              </div>
            )}
            { session === 'inProgress' && (
              <div className={classes.buttonItem}>
                <Button variant="contained" onClick={doCheckIn}>Session in progress</Button>
              </div>
            )}
          </div>
        </Grid>
      </Grid>
      { isArrayLength(services) && (
        <div className={classes.cardContent}>
          <ServicesInfo services={ services } totalCost={totalCost} />
        </div>
      )}
    </Card>
  )
}

export default compose(
  connect(
    () => ({}),
    { checkIn }
  ),
  withStyles(styles)
)(Appointment)
