import React from 'react'
import { CardHeader, Avatar, withStyles } from '@material-ui/core';

const styles = {
  avatar: {
    width: 60,
    height: 60,
  },
}

const ProviderInfo = ({ classes, info }) => {
  const providerName = info.name + (info.degrees ? `, ${info.degrees}` : '')
  return (
    <CardHeader
      avatar={<Avatar alt={info.name} src={info.photo} className={classes.avatar} />}
      title={providerName} // TODO: link to provider page
      subheader={info.specialty} // details?
    />
  )
}

export default withStyles(styles)(ProviderInfo)