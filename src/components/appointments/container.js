import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Paper, Typography, withStyles } from '@material-ui/core';
import { loadAppointments } from '../../actions/appointments'
import { AlertContainer, Loader } from '../common/elements'
import { PageContainer } from '../common/layout';
import { isArrayLength } from '../../utils/arrays';
import Appointment from './Appointment';
import { compose } from 'redux';

const styles = {
  emptyCard: {
    marginTop: '20px',
    padding: '20px',
  }
}

// appointments could be in redux state, in case we want to load them whenever accessing the app
// so that we know if there's one coming up, without having to come to this page, or update them from elsewhere
const AppointmentsContainer = ({ classes, loadAppointments, appointments }) => {
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState()
  useEffect(() => {
    // Memory leak: when user is not logged in, axiosWithAuth() is logging us out and redirecting
    // then the call returns and sets state on an unmounted component
    loadAppointments()
      .catch(e => setError(e))
      .finally(() => setLoading(false))
    return () => {}
  }, []) // equivalent of componentDidMount
  return loading ? <Loader /> : (
    <PageContainer>
      { error && <AlertContainer>{ error }</AlertContainer>}
      <Typography variant="h6">My Appointments</Typography>
      { isArrayLength(appointments) ?
          appointments.map((appt, index) => {
            return ( // depending on time/status -> pointer-events: none, opacity: .6 vs "in session" vs "begin session"
              <Appointment key={index} appointment={appt} />
            )
          }) 
        : (
          <Paper className={classes.emptyCard}>
            <Typography>You have no upcoming appointments at this time.</Typography>
          </Paper>
        )
      }
    </PageContainer>
  )
}

const mapStateToProps = (state) => ({
  appointments: state.AppointmentsReducer.appointments
})

export default compose(
  connect(
    mapStateToProps,
    { loadAppointments }
  ),
  withStyles(styles)
)(AppointmentsContainer)