import styled from 'styled-components'
import { Rating, SubmitButton } from '../../common/elements'
import { colors } from '../../../utils/settings'
import { MaxWidth } from '../../../utils/media'

export const StyledAppointmentInfo = styled.div`
    
`

export const StyledProvider = styled.div`
  display: flex;

  .provider-image {
    margin-right: 20px;
    img {
      width: 115px;
      height: 115px;
      object-fit: cover;
      border-radius: 50%;
    }
  }
  .appointment-header {
    flex: 1;
    display: flex;
    flex-direction: column; /* column-reverse? */
    font-size: .8rem;
    color: #666;
    .section-label {
      font-size: .8rem;
    }
    h2 {
      font-size: 1.1rem;
      font-weight: 700;
      color: ${ colors.blue };
      margin: 2px 0 4px;
    }
    p {
      margin-bottom: 0;
      svg {
        margin-right: 9px;
        color: #999;
      }
    }
  }
  ${ MaxWidth(450)`
    display: block;
    text-align: center;
    .provider-image {
      margin: 0 0 15px;
    }
  `}
`

export const StyledRating = styled(Rating)`
  img {
    width: 14px;
    height: 14px;
  }
`

export const StyledAppointmentDetails = styled.div`
  margin-top: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  ${ MaxWidth(450)`
    display: block;
  `}
`

export const StyledServices = styled.div`
  margin-top: 10px;
  > div {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 5px 0;
    font-size: .9rem;
  }
  .service-row {
    border-bottom: 1px solid #eee;
  }
  .services-total {
    font-weight: 800;
    padding: 8px 0;
    .total-cost {
      color: ${ colors.blue }
    }
  }
`

export const StyledCheckInButton = styled(SubmitButton)`
  white-space: nowrap;
  &, &:hover {
    background-color: rgba(0,70,121,0.5);
  }
`