import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { 
  StyledAppointmentInfo,
  StyledProvider,
  StyledAppointmentDetails,
  StyledRating,
  StyledServices,
  StyledCheckInButton
} from './styles'
import { FormattedCost, ServiceCost, SubmitButtonScale, AppointmentTime, RequestedTime, ProviderName, AddressLink } from '../../common/elements'
import { isArrayLength } from '../../../utils/arrays';

const RatingSpecialties = ({ info }) => {
  let ratingSpecialties = []
  if(info.rating){
    ratingSpecialties.push(<StyledRating rating={info.rating} />)
  }
  if(info.specialty){
    ratingSpecialties.push(info.specialty)
  }
  return ratingSpecialties.length ? (
    ratingSpecialties
      .map((item, i) => <span key={i}>{ item }</span>)
      .reduce((el, item) => [el, ' | ', item])
   ) : null
}

const canCheckIn = (status, time) => {
  // return time && status === 'confirmed' && (new Date(time) - new Date() < 1800000)
  // Convert current time to "objective time". Compare current objective time (UTC) to appointment.confirmedTime (UTC)
  if(time && status === 'confirmed'){
    var now = new Date()
    return new Date(time).getTime() - (now.getTime() - now.getTimezoneOffset() * 60000) < 1800000
  }
  return false
}

const AppointmentInfo = ({
  appointment: { _id, provider, services, details, totalCost, checkInCode, status },
  showCheckInButton
}) => {
  // can also pass in onCheckIn, and paymentDetails. otherwise, it's static
  const info = provider.providerInfo
  if(!info){
    console.error('AppointmentInfo: Missing providerInfo')
    return
  }
  return (
    <StyledAppointmentInfo>
      <StyledProvider>
        { info.photo &&
          <div className="provider-image">
            <img src={info.photo} alt="Provider" />
          </div>
        }
        <div className="appointment-header">
          <div className="provider-info">
            <ProviderName info={info} tag="h2" />
            <p className="provider-rating">
              <RatingSpecialties info={ info } />
            </p>
            { provider.address &&
              <p className="provider-location">
                <AddressLink address={provider.address} />
              </p>
            }
          </div>
          <StyledAppointmentDetails>
            { details.confirmedTime ? (
              <div>
                <div className="section-label"><FontAwesomeIcon icon="calendar-alt" /> Confirmed Time</div>
                <p><AppointmentTime time={details.confirmedTime} /></p>
              </div>
            ) : isArrayLength(details.times) &&
              <div>
                <div className="section-label"><FontAwesomeIcon icon="calendar-alt" /> Requested Times</div>
                { details.times.map((time, index) => (
                  <p key={index}><RequestedTime time={time} setUTC /></p>
                ))}
              </div>
            }
            { showCheckInButton && (checkInCode ?  // move this logic to own component
              <StyledCheckInButton tag={Link} to={`/appointments/${_id}/check-in`}>
                <FontAwesomeIcon icon="check" /> Checked In
              </StyledCheckInButton>
              : canCheckIn(status, details.confirmedTime) && (
                <SubmitButtonScale tag={Link} to={`/appointments/${_id}/check-in`}>Check In</SubmitButtonScale>
              )
            )}
          </StyledAppointmentDetails>
        </div>
      </StyledProvider>
      { services && 
        <StyledServices>
          <div className="section-label">
            <span>Visit type</span>
            <span>Cost</span>
          </div>
          { services.map((service, index) => {
            return (
              <div className="service-row" key={index}>
                <span>{ service.name }</span>
                <ServiceCost cost={service.cost} />
              </div>
            )
          })}
          <div className="services-total">
            <span>Total Cost</span>
            <span className="total-cost">
              <FormattedCost cost={totalCost} /> USD
            </span>
          </div>
        </StyledServices>
      }
    </StyledAppointmentInfo>
  )
}

AppointmentInfo.propTypes = {
  appointment: PropTypes.shape({
    provider: PropTypes.object.isRequired,
    services: PropTypes.array.isRequired,
    details: PropTypes.shape({
      times: PropTypes.array
    }),
    payment: PropTypes.object,
    totalCost: PropTypes.number,
    status: PropTypes.string
  }).isRequired,
  showCheckInButton: PropTypes.bool
}

export default AppointmentInfo