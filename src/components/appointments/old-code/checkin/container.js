import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { doCheckIn } from '../../../actions/appointments'
import CheckIn from './base'
import { AlertContainer, Loader } from '../../common/elements'

export class CheckInContainer extends Component {
  state = {}

  componentDidMount(){
    const { match, doCheckIn } = this.props
    if(!match || !match.params){
      console.error('uhoh')
    }
    doCheckIn(match.params.appointmentId)
  }

  render() {
    const { appointment, loading, error } = this.props
    return (
      <>
        { loading && 
          <Loader />
        }
        { error && 
          <AlertContainer>{ error }</AlertContainer>
        }
        { appointment &&
          <CheckIn appointment={ appointment } />
        }
      </>
    )
  }
}

CheckInContainer.propTypes = {
  appointment: PropTypes.object,
  loading: PropTypes.bool,
  error: PropTypes.string
}

const mapStateToProps = (state) => ({
  appointment: state.AppointmentsReducer.checkIn.data,
  loading: state.AppointmentsReducer.checkIn.loading,
  error: state.AppointmentsReducer.checkIn.error
})

export default connect(
  mapStateToProps,
  {
    doCheckIn
  }
)( CheckInContainer )