import React from 'react'
import PropTypes from 'prop-types'
import QRCode from 'qrcode.react'

import { ContentBoxPadded } from '../../common/layout/container'
import { StyledPageContainer, StyledQRContainer } from './styles'
import AppointmentInfo from '../info/base'

const CheckIn = ({ appointment }) => {
  return (
    <StyledPageContainer>
      { appointment.checkInCode && 
        <StyledQRContainer>
          <QRCode
            value={ appointment.checkInCode }
            size={ 230 }
            level="L"
            bgColor="#ffffff"
            fgColor="#000000" /> 
          <p>Show this QR Code when you arrive at your appointment</p>
        </StyledQRContainer>
      }
      <ContentBoxPadded>
        <AppointmentInfo appointment={ appointment } />
      </ContentBoxPadded>
    </StyledPageContainer>
  )
}

CheckIn.propTypes = {
  appointment: PropTypes.object,
  error: PropTypes.string
}

export default CheckIn