import React from 'react'
import classnames from 'classnames'
import { FormattedCost, ServiceCost } from '../common/elements'
import { isArrayLength } from '../../utils/arrays';
import { withStyles } from '@material-ui/core';
import { colors } from '../../utils/settings';

// const canCheckIn = (status, time) => {
//   // return time && status === 'confirmed' && (new Date(time) - new Date() < 1800000)
//   // Convert current time to "objective time". Compare current objective time (UTC) to appointment.confirmedTime (UTC)
//   if(time && status === 'confirmed'){
//     var now = new Date()
//     return new Date(time).getTime() - (now.getTime() - now.getTimezoneOffset() * 60000) < 1800000
//   }
//   return false
// }

const styles = {
  services: {
    marginTop: '10px'
  },
  serviceRow: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '5px 0',
    fontSize: '.9rem'
  },
  service: {
    borderTop: '1px solid #eee',
  },
  servicesTotal: {
    borderTop: '1px solid #eee',
    fontWeight: '800',
    padding: '8px 0'
  },
  totalCost: {
    color: colors.blue
  }
}

const ServicesInfo = ({
  classes, services, totalCost
}) => {
  return isArrayLength(services) ? (
    <div className={classes.services}>
      <div className={classnames(classes.serviceRow, 'section-label')}>
        <span>Visit type</span>
        <span>Cost</span>
      </div>
      { services.map((service, index) => {
        return (
          <div className={classnames(classes.serviceRow, classes.service)} key={index}>
            <span>{ service.name }</span>
            <ServiceCost cost={service.cost} />
          </div>
        )
      })}
      { !!totalCost && 
        <div className={classnames(classes.serviceRow, classes.servicesTotal)} >
          <span>Total Cost</span>
          <span className={classes.totalCost} >
            <FormattedCost cost={totalCost} /> USD
          </span>
        </div>
      }
    </div>
  ) : null
}

export default withStyles(styles)(ServicesInfo)