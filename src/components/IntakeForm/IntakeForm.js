import React from 'react';
import * as typeformEmbed from '@typeform/embed'
import { connect } from 'react-redux'

class IntakeForm extends React.Component {
  constructor(props) {
    super(props);
    this.el = null;
  }
  componentDidMount() {
    if (this.el) {
      typeformEmbed.makeWidget(this.el, `https://arielfeingold.typeform.com/to/Y9ESYk?email=${this.props.userEmail}&name=${this.props.userName}`, {
        hideFooter: false,
        hideHeaders: false,
        opacity: 50,
        onSubmit: () => {this.props.history.push('/intake-form/thank-you')} 
      });
    }
  }
  render() {
    return (
      <div ref={(el) => this.el = el} style={{width: '100%', height: '700px'}} />
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: state.AuthReducer.user.email,
  userName: state.AuthReducer.user.firstName
})

export default connect(mapStateToProps)(IntakeForm);