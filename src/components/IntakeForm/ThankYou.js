import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    width: "70%",
    maxWidth:"500",
    align:"center",
    margin:"auto",
    marginTop:"30px"
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

function ThankYou(props) {
  const { classes } = props;

  return (
    <div>
      <Paper className={classes.root} elevation={1}>
        <Typography variant="h5" component="h3">
          Thank you for filling out the form
        </Typography>
        <Typography component="p">
          Book a consultation now 
        </Typography>
        <Button 
        variant="contained" 
        color="primary" 
        className={classes.button}
        onClick={() => {props.history.push('/')}}
         >Back to Homepage</Button>
      </Paper>
    </div>
  );
}

ThankYou.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ThankYou);
