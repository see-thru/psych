import React, { useState } from 'react'
import { compose } from 'redux';
import axios from 'axios'
import urlGenerator from '../../api/url-generator'
import * as endpoints from '../../constants/endpoints'
import { useForm } from '../../hooks/useTextField';
import { Button, TextField, withStyles, Paper, Typography } from '@material-ui/core';
import { AlertContainer } from '../common/elements'

const styles = theme => ({
  infoForm: {
    padding: 60,
    maxWidth: "750px"
  },
  textField: {
    display: 'block',
  },
  button: {
    marginTop: theme.spacing.unit * 2
  },
  form:{
    maxWidth:"400px"
  }
})

const submitUrl = urlGenerator(endpoints.SUBMIT_INFO)

const ProviderForm = ({ classes }) => {
  const [alert, setAlert] = useState({})
  const { values, handleChange, handleSubmit, clearForm } = useForm(onSubmit)
  let isSubmitting
  function onSubmit(){
    if(isSubmitting){ return }
    isSubmitting = true
    setAlert({})
    axios.post(submitUrl, values)
      .then(response => {
        clearForm()
        setAlert({ 
          show: true, 
          message: 'Thank you! We have received your message and will reach out to you soon.',
          type: 'success'
        })
      })
      .catch(e => {
        console.error('Error submitting provider', e)
        setAlert({ show: true, message: 'There was an error submitting your information. Please try again or contact help@seethru.healthcare.' })
      })
      .finally(() => {
        isSubmitting = false
      })
  }

  return (
    <Paper className={classes.infoForm}>
      <Typography component="h2" variant="display1">Clinician Signup</Typography>
      <Typography variant="subheading">We are excited to start working with you. Once you click “Join,” your personal clinician liaison will be in touch shortly.</Typography>
      
      { alert.show && <AlertContainer type={alert.type}>{ alert.message }</AlertContainer>}
      
      <form className={classes.form} onSubmit={handleSubmit}>
        <TextField id="fname-field" label="First Name" name="firstName" required
          value={values.firstName || ''}
          onChange={handleChange}
          margin="normal" className={classes.textField}
          fullWidth
        />
        <TextField id="title-field" label="Title" name="title" required
          value={values.title|| ''}
          onChange={handleChange}
          margin="normal" className={classes.textField}
          fullWidth
        />
        <TextField id="name-field" label="Last Name" name="lastName" required
          value={values.lastName || ''}
          onChange={handleChange}
          margin="normal" className={classes.textField}
          fullWidth
        />
        <TextField id="email-field" label="Email" name="email" required
          type="email"
          value={values.email || ''}
          onChange={handleChange}
          margin="normal" className={classes.textField}
          fullWidth
        />
        <TextField id="specialty-field" label="Specialty" name="specialty" required
          value={values.specialty || ''}
          onChange={handleChange}
          margin="normal" className={classes.textField}
          fullWidth
        />
        <TextField id="zip-field" label="Zip Code" name="zip" required
          type="number"
          value={values.zip || ''}
          onChange={handleChange}
          margin="normal" className={classes.textField}
          fullWidth
        />
        <TextField id="phone-field" label="Phone Number" name="phoneNumber" required
          type="text"
          value={values.phoneNumber || ''}
          onChange={handleChange}
          margin="normal" className={classes.textField}
          fullWidth
        />
        <Button className={classes.button} variant="contained" type="submit" color="primary" fullWidth>Join</Button>
      </form>
    </Paper>
  )
}

export default compose(
  withStyles(styles)
)(ProviderForm)