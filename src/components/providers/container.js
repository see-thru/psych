import React from 'react'
import { compose } from 'redux'
import { withStyles, Typography } from '@material-ui/core'
import { PageContainer } from '../common/layout'
import ProviderForm from './ProviderForm'
import { Link } from 'react-router-dom'


const styles = theme => ({
  header:{
    color: "#214679",
    marginTop:"1.5em"
  },
  subHeader:{
    color: "#214679",
    marginBottom: "1em"
  },
  listItem:{
    marginBottom: "1em"
  },
  listHeader:{
    color: "#214679",
  },
  ul:{
    listStyleType: "none",
    paddingBottom:"1em"
  }
})

const ProvidersContainer = ({ classes }) => {
  return (
    <PageContainer>
      <Typography className={classes.header} align="left" variant="h3">Join The SeeThru Network</Typography>
      <Typography align="left" variant="subtitle1">
      <br />
         We think Behavioral Healthcare should be approachable, accessible and easy. That's why we built SeeThru to empower clinicians to focus on giving their patients the care they deserve.
      </Typography>
      <br/>
      <Typography align="left" variant="subtitle1">
        SeeThru is a platform that connects patients and clinicians. Patients search for a clinician that best fits their needs and budget;
        then book their appointment right then and there.
        Later, the patient and clinician sign back on to the platform to have the appointment in a private, HIPAA-compliant, online location.
      </Typography>
      <br/>
      <Typography className={classes.subHeader} align="left" variant="h6">
        SeeThru Allows You To:
      </Typography>

      <ul className={classes.ul}>
        <li className={classes.listItem}>
        <Typography className={classes.listHeader} align="left" variant="subtitle1">
          <b>Set Your Own Prices</b>
        </Typography>
        <Typography align="left" variant="body1">
          Once on the SeeThru platform, you decide what you want to make and what your time is worth.
        </Typography>
        </li>
        <li className={classes.listItem}>
        <Typography className={classes.listHeader} align="left" variant="subtitle1">
          <b>Make Your Practice More Accessible</b>
        </Typography>
        <Typography className={classes.listText} align="left" variant="body1">
          The SeeThru platform allows you to deliver care to those with a varying range of accessibility needs.
        </Typography>
        </li>
        <li className={classes.listItem}>
        <Typography className={classes.listHeader} align="left" variant="subtitle1">
          <b>Practice Whenever You Want</b>
        </Typography>
        <Typography className={classes.listText} align="left" variant="body1">
        Just like your prices, you set your hours. Decide when you are free to see patients and when you need time for yourself.
        </Typography>
        </li>
        <li className={classes.listItem}>
        <Typography className={classes.listHeader} align="left" variant="subtitle1">
          <b>Get New Patients</b>
        </Typography>
        <Typography className={classes.listText} align="left" variant="body1">
          SeeThru puts you in front of countless patients you would not otherwise have access to.
        </Typography>
        </li>
        <li className={classes.listItem}>
        <Typography className={classes.listHeader} align="left" variant="subtitle1">
          <b>Guaranteed Payments</b>
        </Typography>
        <Typography className={classes.listText} align="left" variant="body1">
          You are guaranteed to receive full payment for every appointment you complete without the hassle of billing.
        </Typography>
        </li>
      </ul>
        <Typography className={classes.subHeader} align="left" variant="subtitle1">
          For more information, please visit our <Link to="/provider-faq">FAQ page.</Link>
        </Typography>
      <ProviderForm />
    </PageContainer>
  )
}

export default compose(
  withStyles(styles)
)(ProvidersContainer)