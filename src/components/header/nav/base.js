import React from 'react'
import { UserNav } from './user/base'
import { StyledNav, StyledButton, StyledButtonBlue, StyledLink, StyledLinkRouter } from './styles'

const Nav = ({
  user,
  history,
  isLogged,
  isOpened,
  toggle,
  logout,
  toggleSignInModal,
  toggleSignUpModal	  
}) => (
  <StyledNav 
    onClick={ toggle }
    isOpened={ isOpened }>
    <StyledLinkRouter to='/about'>About</StyledLinkRouter>
    <StyledLinkRouter to='/faq'>FAQ</StyledLinkRouter>
    { isLogged ? 
      <UserNav logout={ () => logout(history) } user={ user } />
      :
      <>
        <StyledLinkRouter to='/for-providers'>For clinicians</StyledLinkRouter>
        <StyledButton
          onClick={ () => toggleSignInModal(true) }
          type="register">
          Log In 
        </StyledButton>
        <StyledButtonBlue primary
          onClick={ () => toggleSignUpModal(true) }
          type="signin">
          Sign Up
        </StyledButtonBlue>
      </>
    }
  </StyledNav>
)

export default Nav