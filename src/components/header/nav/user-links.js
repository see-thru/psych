import React from 'react'

import { StyledLinkRouter } from './styled-links'

export const PatientLinks = () => (
  <>
    <StyledLinkRouter to="/user/edit-photo" mobile="true">
      Edit Photo
    </StyledLinkRouter>
    <StyledLinkRouter to="/user/payment-info/methods" mobile="true">
      Payment Information
    </StyledLinkRouter> 
    <StyledLinkRouter to="/patient/booking/appointments">
      Appointments
    </StyledLinkRouter>
  </>
)

export const ProviderLinks = () => (
  <>
    <StyledLinkRouter to="/user/provider/edit-provider-profile" mobile="true">
      Edit Profile
    </StyledLinkRouter>   
    <StyledLinkRouter to="/user/edit-photo" mobile="true">
      Edit Photo
    </StyledLinkRouter>   
    <StyledLinkRouter to="/user/provider/update-schedules" mobile="true">	
      Update Schedules	
    </StyledLinkRouter>   
    <StyledLinkRouter to="/user/provider/edit-healthcare-services" mobile="true">
      Edit Healthcare Services
    </StyledLinkRouter>    
    <StyledLinkRouter to="/provider/booking/appointments/list">
      Appointments
    </StyledLinkRouter>    
  </>
)