import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import BodyClassName from 'react-body-classname'
import { toggleSignInModal, toggleSignUpModal, logout } from '../../../actions/auth'
import Nav from './base'
import MenuButton from './menu-button'
import { useToggleField } from '../../../hooks/useMergeState';

// TODO: fix the dropdown!!
const NavContainer = props => {
  const [ isOpen, toggleIsOpen ] = useToggleField()
  return (
    <div>
      { isOpen && <BodyClassName className="shadow-block" /> }
      <MenuButton 
        isActived={isOpen}
        toggle={toggleIsOpen} />
      <Nav
        isOpened={ isOpen } 
        toggle={ toggleIsOpen }
        { ...props } />
    </div>
  )
}

NavContainer.propTypes = {
  history: PropTypes.object.isRequired,
  user: PropTypes.object,
  isLogged: PropTypes.bool,
  logout: PropTypes.func.isRequired,
  toggleSignInModal: PropTypes.func.isRequired,
  toggleSignUpModal: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  user: state.AuthReducer.user,
  isLogged: state.AuthReducer.isLogged
})

export default connect(
  mapStateToProps,
  { 
    logout,
    toggleSignInModal,
    toggleSignUpModal
  }
)(NavContainer)