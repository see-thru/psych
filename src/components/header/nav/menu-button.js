import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Icon, DefaultButton } from '../../common/elements'
import Media from '../../../utils/media'
import { fonts } from '../../../utils/settings'

// Remove isActived property from DOM element - https://github.com/styled-components/styled-components/issues/305#issuecomment-266221014
const StyledMenuButton = styled(({ isActived, ...props }) => <DefaultButton {...props} /> )`
  color: #565656;
  cursor: pointer;
  display: none;
  font-size: 14px;
  font-weight: ${ fonts.weightBold };
  text-transform: uppercase;
  padding: 20px 10px;
  line-height: 20px;
  ${ 
    Media.small`
      display: block;
    `
  }
  &:focus {
    outline: 0;
  }
  svg {
    margin-left: 6px;
    position: relative;
    transform:  ${ ({ isActived }) => isActived ? 'rotate(180deg)' : 'rotate(0)' };
    transition: .3s transform;
    bottom: 4px;
  }
`

const MenuButton = ({
  toggle,
  isActived
}) => (
  <StyledMenuButton
    isActived={ isActived }
    onClick={ toggle }>
    Menu
    <Icon 
      name="menu-arrow" 
      width="6px" 
      height="3px" />
  </StyledMenuButton>
)

MenuButton.propTypes = {
  toggle: PropTypes.func.isRequired,
  isActived: PropTypes.bool
}

export default MenuButton