import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Icon } from '../../../common/elements'
import { 
  StyledUserNav,
  StyledSmallUserNav,
  StyledDropdownToggle,
  StyledMockAvatar,
  StyledDropdownList,
  StyledDropdownButton,
  StyledDropdownLink
} from './styles'
import { StyledButton, StyledLinkRouter } from '../styles'

// TODO: use hooks here
export class UserNav extends Component {
  constructor () {
    super()
    this.state = ({ isOpen : false })
    this.toggle = this.toggle.bind(this)
    this.closeDropdown = this.closeDropdown.bind(this)
  }
  
  componentDidMount() {
    document.addEventListener('mousedown', this.handleClick, false)
  }

  toggle () {
    this.setState({ isOpen : !this.state.isOpen })
  }

  closeDropdown () {
    this.setState({ isOpen : false })
  }

  // can't get onClickOutside to work. so.. https://codepen.io/graubnla/pen/EgdgZm
  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClick, false)
  }

  handleClick = (e) => {
    if (this.node.contains(e.target)) {
      return;
    }
    this.closeDropdown()
  }

  render () {
    const { isOpen } = this.state
    const { logout } = this.props

    return (
      <>
        <StyledLinkRouter to='/appointments'>	
          My Appointments
        </StyledLinkRouter>
        <StyledUserNav ref={node => { this.node = node }}>
          <StyledDropdownToggle onClick={ this.toggle }>
            <StyledMockAvatar  />
          </StyledDropdownToggle>
          { isOpen &&
            <StyledDropdownList onClick={ this.closeDropdown }>
              { false &&
                <StyledDropdownLink to="/user/edit-profile">
                  Edit Patient Profile
                </StyledDropdownLink>
              }
              <StyledDropdownButton onClick={ logout }>
                Logout <Icon fill="#999" name="logout" width="12" height="12" />
              </StyledDropdownButton>
            </StyledDropdownList>
          }
        </StyledUserNav>
        <StyledSmallUserNav>
          { false &&
            <StyledLinkRouter to='/user/edit-profile'>	
              Edit Patient Profile
            </StyledLinkRouter>
          }
          <StyledButton onClick={ logout }>	
            Logout <Icon fill="#999" name="logout" width="12" height="12" />
          </StyledButton>
        </StyledSmallUserNav>
      </>
    )
  }
} 

UserNav.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired
}

export default UserNav