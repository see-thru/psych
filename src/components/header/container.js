import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { toggleDrawer } from '../../actions/provider'
import Logo from './logo'
import NavContainer from './nav/container'
import { StyledHeader } from './styles'
import LeftIcon from '@material-ui/icons/ChevronLeft';
import MenuIcon from '@material-ui/icons/Menu';
import { withStyles, IconButton } from '@material-ui/core';
import { compose } from 'redux';

const styles = {
  button: {
    margin: '0 10px'
  }
}

const Header = ({ classes, history, location, toggleDrawer }) => {
  const [ canGoBack, setCanGoBack ] = useState(false)
  useEffect(() => {
    setCanGoBack(window.SeeThru.stackSize > 0)
  })
  // const hasServicesDrawer = RegExp(/\/provider\/[^\/]*$/).test(location.pathname)
  const hasServicesDrawer = location.pathname.startsWith('/provider/')
  return (
    <StyledHeader>
      <div className="header-content">
        <div>
          { canGoBack && 
            <IconButton className={classes.button} onClick={history.goBack} aria-label="Navigate back">
              <LeftIcon />
            </IconButton>
          }
          <Logo />
        </div>
        <div className="nav-content">
          <NavContainer history={ history } />
          { hasServicesDrawer && 
            <IconButton className={classes.button} onClick={toggleDrawer} aria-label="Open services checkout">
              <MenuIcon />
            </IconButton>
          }
        </div>
      </div>
    </StyledHeader>
  )
}

export default compose(
  withRouter,
  withStyles(styles),
  connect(
    () => ({}),
    { toggleDrawer }
  )
)(Header)