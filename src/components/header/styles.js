import styled from 'styled-components'
import { colors, values } from '../../utils/settings'

export const StyledHeader = styled.header`
  background-color: ${ colors.headerBackground };
  border-bottom: 1px solid #ddd;
  height: ${ values.headerHeight };
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 2;
  
  .header-content {
    height: inherit;
    margin: auto;
    padding: 0 25px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    .nav-content {
      display: flex;
    }
  }
`

export const StyledSearchForm = styled.div`
  background-color: ${ colors.blue }
  height: 50px;
  display: flex;
  padding: 10px 25px;
`