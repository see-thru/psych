import React from 'react'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'
import { compose } from 'redux';
import { connect } from 'react-redux'
import { Button, withStyles } from '@material-ui/core';
import { createAppointment } from '../../../actions/provider'
import { useToggleField } from '../../../hooks/useMergeState';
import { isArrayLength } from '../../../utils/arrays';
import { colors } from '../../../utils/settings'
import EmailProviderDialog from './EmailProvider'
import { usStateNames, insuranceNames } from '../../../constants/filters';

const StyledCategory = styled.div`
  h3 {
    font-size: 0.8rem;
    font-weight: 700;
    color: #999;
  }
  ul { 
    list-style: none;
    padding-left: 15px;
  }
`

const StyledOption = styled.li`
  font-size: 0.9rem;
  position: relative; 
  padding: 1px 0;
  color: ${colors.blue};
`

// TODO: Map to option name
const Category = React.memo(({ items, label }) => {
  return isArrayLength(items) ? (
    <StyledCategory>
      <h3>{ label }</h3>
      <ul>
        { items.map((item, index) => (
          <StyledOption key={index}>{ item }</StyledOption>
        ))}
      </ul>
    </StyledCategory>
  ) : null
})

const styles = {
  button: {
    color: 'white !important',
    marginBottom: '10px',
  }
}

const getNames = (items, choices) => isArrayLength(items) ? items.map(i => choices[i] || i) : [];

const freeConsultation = { name: 'Free Consultation', time: 15, cost: 0 }
const ProviderDetails = ({ classes, provider, createAppointment, history }) => {
  const [modalOpen, toggleModal] = useToggleField(false)
  return (
    <>
      <div>
        <Button className={classes.button} color="primary" variant="contained" 
          onClick={() => createAppointment(history, [freeConsultation])}>Book a free consultation</Button>
        <Button variant="outlined" onClick={toggleModal}>Any Questions?</Button>
        <hr />
        <Category items={getNames(provider.usStates, usStateNames)} label="US States" />
        <Category items={getNames(provider.insurances, insuranceNames)} label="Accepted Insurance" />
        <hr />
        <Category items={provider.issues} label="Issues" />
        <Category items={provider.modalities} label="Types of Therapy" />
        <Category items={provider.focuses} label="Client Focus" />
      </div>
      <EmailProviderDialog open={modalOpen} onClose={toggleModal} />  
    </>
  )
}

export default compose(
  connect(
    () => ({}),
    { createAppointment }
  ),
  withStyles(styles),
  withRouter
)(ProviderDetails)