import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames';
import { toggleService } from '../../../actions/provider'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ServiceCost } from '../../common/elements'
import { StyledMedicalServices, StyledServiceCard } from './styles'
import { isArrayLength } from '../../../utils/arrays';
import { withStyles, Grid, CardActions, CardContent, Collapse, Typography, IconButton } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useToggleState } from '../../../hooks/useMergeState';

const ServiceTime = ({ time }) => {
  var minutes = parseInt(time)
  if (!isNaN(minutes)) { 
    return (
      <Typography><span>{ minutes } Minutes</span></Typography>
    )
  }
}

const styles = theme => ({
  cardContent: { 
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  actions: {
    display: 'flex',
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
})

const MedicalServiceList = withStyles(styles)(({ classes, services, toggleService }) => {
  const [expanded, toggleExpanded] = useToggleState()
  return (
    <Grid container spacing={16}>
      {services.map( (s, index) => (
        <Grid item xs={12} sm={6} key={index}>
          <StyledServiceCard 
            className={classes.paper}
            type={ s.selected ? 'selected' : '' }>
            <CardContent className={classes.cardContent}>
              <div>
                <Typography gutterBottom variant="h6" component="h2">{ s.name }</Typography>
                { s.time && <ServiceTime time={s.time} /> }
              </div>
              <ServiceCost className="service-cost" cost={s.cost} />
            </CardContent>
            <CardActions className={classes.actions} disableActionSpacing>
              <>
                { s.locked || (
                  <IconButton 
                    aria-label={s.selected ? 'Service selected' : 'Click to select service' }
                    onClick={() => {toggleService(s.index)}}>
                    { s.selected ? <FontAwesomeIcon icon="check" /> : <FontAwesomeIcon icon="plus" /> }
                  </IconButton>
                )}
                { (isArrayLength(s.includes) || s.description) &&
                  <IconButton
                    className={classnames(classes.expand, {
                      [classes.expandOpen]: expanded[index],
                    })}
                    onClick={() => toggleExpanded(index)}
                    aria-expanded={expanded[index]}
                    aria-label="Show more"
                  >
                    <ExpandMoreIcon />
                  </IconButton>
                }
              </>
            </CardActions>
            <Collapse in={expanded[index]} timeout="auto" unmountOnExit>
              <CardContent>
                { isArrayLength(s.includes) && s.includes.map((item, index) => {
                  return <Typography key={index} color="textSecondary">{ item }</Typography>
                })}
                { s.description && <Typography color="textSecondary"><span dangerouslySetInnerHTML={{ __html: s.description}} /></Typography>}
              </CardContent>
            </Collapse>
          </StyledServiceCard>
        </Grid>
      ))}
    </Grid>
  )
})

const Services = ({ services, toggleService }) => {
  const [categories, setCategories] = useState([])
  useEffect(() => {
    if(isArrayLength(services)){
      let scatMap = services.reduce((cats, service) => {
        if(service.category && !cats[service.category]){
          cats[service.category] = true
        }
        return cats
      }, {})
      let scats = Object.keys(scatMap)
      if(scats.length){
        setCategories(scats)
      }
    }
  }, [services])

  return isArrayLength(services) ? (
    <StyledMedicalServices>
      <div className="title">
        <h2>Services</h2>
      </div>
      <div className="services-content">
        <MedicalServiceList 
          services={services.filter(s => !s.category)} 
          toggleService={toggleService} />

        { Array.isArray(categories) && categories.map((c, index) => (
          <div key={ index } >
            {/* <StyledMedicalService type="category">
              <div className="service-row" id={`toggleCat${index}`}>
                <span className="service-name">{ c }</span>
                <FontAwesomeIcon icon="chevron-down" />
              </div>
            </StyledMedicalService>
            <UncontrolledCollapse toggler={`toggleCat${index}`}>
            </UncontrolledCollapse> */}
            <MedicalServiceList 
              services={services.filter(s => s.category === c)} 
              toggleService={toggleService} />
          </div>
        ))}
      </div>
    </StyledMedicalServices>
  ) : null
}

const mapStateToProps = (state) => ({
  services: state.ProviderReducer.services
})

export default connect(
  mapStateToProps,
  { toggleService }
)(Services)