import styled from 'styled-components'
import { SubmitButton } from '../../common/elements'
import { ContentBox } from "../../common/layout"
import { colors } from '../../../utils/settings'
import { MaxWidth } from '../../../utils/media'
import { Card } from '@material-ui/core';

export const StyledProviderContainer = styled.div`
  margin-top: 30px;
  margin-bottom: 15px;
`

export const StyledProviderInfo = styled(ContentBox)`
  padding-top: 30px;
  font-size: 0.9rem;
  margin-bottom: 20px;

  p {
    margin-bottom: 5px;
  }
  .provider-header {
    display: flex;
    align-items: center;
    padding: 0 30px;
    .provider-image {
      flex-basis: 150px;
      img {
        border-radius: 50%;
        height: 150px;
        width: 150px;
        object-fit: cover;
      }
    }
    .provider-title {
      margin-left: 1.2rem;
      h1 {
        color: ${ colors.titleColor };
        font-size: 1.15rem;
        line-height: 1.3rem;
        font-weight: 700;
        margin: 0;
      }
      .provider-hospital {
        margin-bottom: .4rem;
      }
      .provider-specialties {
        color: gray;
        font-size: .8rem;
        margin-bottom: 10px;
        p {
          margin: 0;
        }
      }
    }
  }
  ${ MaxWidth(500)`
    .provider-header {
      display: block;
      text-align: center;
      padding: 0 10px;
      .provider-title {
        margin: 0;
      }
      .provider-image {
        margin-bottom: 15px;
      }
    }
  `}
`

export const StyledProviderDetails = styled.div`
  margin-top: 20px;
  .detail-section {
    border-top: 1px solid ${ colors.borderGray };
    padding: 10px 35px;
    > p {
      position: relative;
      display: flex;
      justify-content: space-between;
      align-items: center;
      color: ${ colors.titleColor };
      font-size: 1rem;
      font-weight: 700;
      margin-bottom: 5px;
      cursor: pointer;
      svg {
        color: ${ colors.iconGray };
      }
    }
    .collapse-content {
      padding: 5px 10px 0;
      white-space: pre-wrap;
      list-style: none;
      margin: 0;
      li {
        margin-bottom: 5px;
      }
    }
  }
`

export const StyledBookAppointment = styled.div`
  background-color: ${ colors.darkBlue };
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
  color: white;
  margin-bottom: 20px;
  box-shadow: 0px 0px 7px -1px rgba(0, 70, 121, 0.5);
  .appointment-content {
    font-size: 0.8rem;
    padding: 5px 15px;
    h2 {
      font-size: 1rem;
      font-weight: 700;
      margin-bottom: 0.3rem;
    }
    p, ul {
      margin: 0;
    }
    svg {
      margin-right: 5px;
    }
    .pick-time {
      text-align: left;
      border-radius: 3px;
      border: 1px solid transparent;
      color: inherit;
      transition: all 300ms linear;
      padding: 5px;
      &:hover {
        border-color: #ddd;
        color: #eee;
      }
      p { font-weight: 700; }
    }
  }
  ${ MaxWidth(500)`
    display: block;
    text-align: center;
    font-size: 1rem;
  `}
`

export const StyledBookButton = styled(SubmitButton)`
  background-color: ${ colors.lightBlue }
  transition: transform .2s ease-out;
  &:hover {
    transform: scale(1.05);
    background-color: ${ colors.lightBlue }
  }
  font-size: .9rem;
  padding-left: 15px;
  padding-right: 15px;
`

export const StyledMedicalServices = styled.div`
  padding-bottom: 0;
  .title {
    padding-top: 10px;
  }
`

export const StyledServiceCard = styled(Card)`
  border-top: 3px solid transparent;
  ${ props => { switch(props.type) {
    case 'selected': return `
      color: #fafafa;
      background-color: ${ colors.blue };
      border-color: ${ colors.darkBlue };
      .service-bundle {
        color: #ddd;
        text-shadow: 1px 1px rgba(0,0,0,.5);
      }
    `
    case 'category': return `
      background-color: #eee;
      border-color: #ccc;
      color: #666;
      background-color: #edf5fb;
      padding-left: 17px;
      .service-row span.service-name {
        font-size: .9rem;
      }
    `
    default: return `
      background-color: white;
      .service-bundle {
        color: #999;
      }
    `
  }}}

  .service-row {
    display: flex;
    justify-content: space-between;
    align-items: center;
    .service-name {
      font-size: 1rem;
      font-weight: 700;
      flex: 1;
      padding-right: 10px;
    }
    .service-time {
      flex-basis: 220px;
      font-size: .85rem;
      > span {
        font-weight: 700;
      }
    }
    .service-cost {
      flex-basis: 40px;
      text-align: center;
    }
    .service-toggle {
      font-size: 1.2rem;
      flex-basis: 30px;
      text-align: right;
      color: #cecece;
    }
  }
  .service-bundle {
    transition: color .2s linear;
    padding: 3px 10px 0;
    span {
      display: block;
      font-size: .9rem;
      line-height: 1.3rem;
    }
  }
  ${ MaxWidth(600)`
    .service-row {
      flex-wrap: wrap;
      position: relative;
      .service-name { 
        flex-basis: 100%;
      }
      .service-toggle {
        position: absolute;
        top: -4px;
        right: 5px;
      }
    }
    .service-bundle {
      padding-left: 0;
    }
  `}  
`