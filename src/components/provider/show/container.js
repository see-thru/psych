import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { getProvider } from '../../../actions/provider'
import { AlertContainer, Loader } from '../../common/elements'
import ProviderDetails from './ProviderDetails'
import ProviderInfo from './ProviderInfo'
import MedicalServices from './Services'
import { StyledProviderContainer } from './styles'

const ProviderContainer = ({ 
  match, getProvider, provider, error, loading
}) => {
  useEffect(() => {
    // if(!provider || (match.params.providerId !== provider._id)){
    //   getProvider(match.params.providerId)
    // }
    // Reload provider every time (just in case there were changes on admin), or use the one in state for speed?
    getProvider(match.params.providerId)
  }, [match])
  return (
    <div>
      { loading ? <Loader /> : <>
        { error && <AlertContainer>{ error }</AlertContainer> }
        { provider &&     
          <StyledProviderContainer className="container">
            <div className="row">
              <div className="col-lg-9">
                { provider.providerInfo &&
                  <ProviderInfo info={ provider.providerInfo } />
                }
                <MedicalServices />
              </div>
              <div className="col-lg-3">
                <ProviderDetails provider={provider} />
              </div>
            </div>
          </StyledProviderContainer>
        }
      </>}
    </div>
  )
}

const mapStateToProps = (state) => ({
  provider: state.ProviderReducer.data,
  error: state.ProviderReducer.error,
  loading: state.ProviderReducer.loading
})

export default connect(
  mapStateToProps,
  { getProvider }
)(ProviderContainer)