import React from 'react'
import { Button, Dialog, DialogTitle, TextField, withStyles } from '@material-ui/core';
import { useTextField } from '../../../hooks/useTextField';

const styles = theme => ({
  buttonContainer: {
    marginTop: '10px',
    textAlign: 'right',
  },
  button: {
    margin: theme.spacing.unit,
  },
  content: {
    padding: '0 30px 10px'
  }
})

const EmailProviderDialog = ({ open, onClose, classes }) => {
  const [email, changeEmail] = useTextField()
  const [body, changeBody] = useTextField()
  const onSubmit = () => {
    // emailProvider() - call backend api function to call sendgrid and set "completed" variable to view success message
    console.log('email submitted: ', email, body)
    onClose()
  }
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Email this Provider</DialogTitle>
      <div className={classes.content}>
        <TextField
          id="email-field"
          label="Email"
          className={classes.textField}
          value={email}
          onChange={changeEmail}
          margin="normal"
          fullWidth
        />
        <TextField
          id="body-field"
          label="Body"
          multiline
          rows="4"          
          className={classes.textField}
          value={body}
          onChange={changeBody}
          margin="normal"
          fullWidth
        />
        <div className={classes.buttonContainer}>
          <Button className={classes.button} onClick={onClose}>Cancel</Button>
          <Button className={classes.button} onClick={onSubmit} variant="contained" color="primary" >Submit</Button>
        </div>
      </div>
    </Dialog>
  )
}

export default withStyles(styles)(EmailProviderDialog)