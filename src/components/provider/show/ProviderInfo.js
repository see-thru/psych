import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Collapse } from 'reactstrap'
import { 
  StyledProviderInfo,
  StyledProviderDetails
} from './styles'
import { Rating, ProviderName } from '../../common/elements';
import { isArrayLength } from '../../../utils/arrays';
import { useToggleState } from '../../../hooks/useMergeState';

const ProviderSpecialties = ({ specialty, secondary }) => (
  <div className="provider-specialties">
    <p>{ specialty }</p>
    { isArrayLength(secondary) &&
      <p>Secondary Specialty: { secondary.join(' | ') }</p>
    }
  </div>
)

const ProviderInfo = ({ info }) => {
  const [showSection, toggleSection] = useToggleState({ description: true })

  const renderToggle = (key, name) => (
    <p onClick={() => {toggleSection(key)}}>
      { name }
      { showSection[key] ?
        <FontAwesomeIcon icon="chevron-up" /> : <FontAwesomeIcon icon="chevron-down" />
      }
    </p>
  )

  return (
    <StyledProviderInfo>
      <div className="provider-header">
        <div className="provider-image">
          <img src={ info.photo } alt="" />
        </div>
        <div className="provider-title">
          <ProviderName info={info} />
          { info.specialty &&
            <ProviderSpecialties specialty={info.specialty} secondary={info.secondarySpecialties} />
          }
          { info.rating &&
            <Rating rating={info.rating} />
          }
        </div>
      </div>
      <StyledProviderDetails>
        { info.description &&
          <div className="detail-section">
            { renderToggle('description', 'Description') }
            <Collapse isOpen={showSection['description']}>
              <div className="collapse-content">
              { info.description }
              </div>
            </Collapse>
          </div>
        }
        { isArrayLength(info.languages) &&
          <div className="detail-section">
            { renderToggle('languages', 'Languages') }
            <Collapse isOpen={showSection['languages']}>
              <ul className="collapse-content">
                { info.languages.map( (lang, index) => (
                  <li key={index}>{ lang }</li>
                ))}
              </ul>
            </Collapse>
          </div>
        }
        { isArrayLength(info.education) && 
          <div className="detail-section">
            { renderToggle('education', 'Education & Affiliations') }
            <Collapse isOpen={showSection['education']}>
              <ul className="collapse-content">
                { info.education.map( (item, index) => (
                  <li key={index}>{ item }</li>
                ))}
              </ul>
            </Collapse>
          </div>
        }
      </StyledProviderDetails>
    </StyledProviderInfo>
  )
}

export default ProviderInfo