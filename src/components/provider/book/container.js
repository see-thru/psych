import React, { useState, useCallback, useEffect } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Elements, StripeProvider } from 'react-stripe-elements'
import moment from 'moment'
import classnames from 'classnames'
import { withStyles, Stepper, Step, StepButton, StepContent, Radio, Avatar,
  Card, CardHeader, RadioGroup, FormControl, FormControlLabel, Divider, Typography, Button
} from '@material-ui/core'
import { DatePicker } from 'material-ui-pickers'
import { bookAppointment } from '../../../actions/appointments';
import { STRIPE_API_KEY } from '../../../constants/stripe'
import { Loader, AlertContainer } from '../../common/elements';
import AppointmentForm from './AppointmentForm';
import { isArrayLength } from '../../../utils/arrays';

const defaultAvailability = [{ from: 9, to: 17 }]
const defaultAvailabilities = {
  0: defaultAvailability,
  1: defaultAvailability,
  2: defaultAvailability,
  3: defaultAvailability,
  4: defaultAvailability,
  5: defaultAvailability,
  6: defaultAvailability
}

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '15px',
    height: '100%',
  },
  card: {
    width: '100%',
    maxWidth: '900px',
    padding: '10px 10px 25px 10px',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  stepButton: {
    outline: 'none !important',
  },
  avatar: {
    width: 60,
    height: 60,
  },
  formControlLabel: {
    marginBottom: 0,
  },
  radioButton: {
    padding: '5px',
  },
  bookingComplete: {
    padding: '20px',
  },
  button: {
    marginRight: '10px'
  },
  buttonLink: {
    color: 'white !important'
  }
})


const BookAppointmentContainer = ({ classes, appointment, availabilities, bookAppointment }) => {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState()

  // if no appointment, should probably history.push back to home page or provider page...
  const [stepIndex, setStep] = useState(0)
  const [appointmentDate, setAppointmentDate] = useState()
  const [appointmentTime, setAppointmentTime] = useState()

  const onSubmit = (stripeSource) => {
    setLoading(true)
    bookAppointment(appointment, stripeSource, { times: [{date: appointmentDate, time: appointmentTime }]}).then(() => {
      setLoading(false)
    }).catch(setError)
  }
  
  const isDayDisabled = useCallback(date => {
    return !isArrayLength(availabilities[date.day()])
  }, [availabilities])

  const getAvailableTimes = useCallback(() => {
    if(appointmentDate){
      let thisAvailabilities = availabilities[appointmentDate.day()] || []
      let appointmentTimes = []
      if(isArrayLength(thisAvailabilities)){
        thisAvailabilities.forEach(a => {
          if(a.from && a.to){
            for(var i = a.from; i < a.to; i++){ // could do intervals of "totalTime"
              let t1 = moment().hour(i).minute(0)
              // let t2 = moment().hour(i).minute(appointment.totalTime)
              let t2 = moment().hour(i + 1).minute(0)
              // appointmentTimes.push({
              //   label: t1.format('h:mm a') + ' - ' + t2.format('h:mm a'),
              //   value: t1.format()
              // })
              appointmentTimes.push(t1.format('h:mm a') + ' - ' + t2.format('h:mm a'))
            }
          }
        })
      }
      return appointmentTimes
    }
    return []
  }, [appointmentDate, appointment, availabilities])

  // default date should be first date available, starting tomorrow
  const earliest = moment().add(1, 'day')
  useEffect(() => {
    while(isDayDisabled(earliest)){
      earliest.add(1, 'day')
    }
    setAppointmentDate(earliest)
  }, [availabilities])
  const availableTimes = getAvailableTimes()
  const info = appointment.provider.providerInfo
  const providerName = info.name + (info.degrees ? `, ${info.degrees}` : '')
  const stepper = (
    <Stepper
      activeStep={stepIndex}
      orientation="vertical">
      <Step>
        <StepButton className={classes.stepButton} onClick={() => setStep(0)}>
          Choose an available date & time for your appointment{ appointmentTime && ` (${appointmentTime})`}
        </StepButton>
        <StepContent>
          <DatePicker
            keyboard
            label="Appointment date"
            format="MM/DD/YYYY"
            mask={value =>
              value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : []
            }
            value={appointmentDate}
            onChange={date => { setAppointmentDate(date); setAppointmentTime(undefined); }}
            disableOpenOnEnter
            disablePast
            animateYearScrolling
            shouldDisableDate={isDayDisabled}
          />
          <div>
            <FormControl component="fieldset" className={classes.formControl}>
              <RadioGroup
                aria-label="Appointment Time"
                name="appointmentTimes"
                className={classes.group}
                value={appointmentTime}
                onChange={e => { setAppointmentTime(e.target.value); setStep(1); }}
              >
                {availableTimes.map((time, index) => {
                  return (
                    <FormControlLabel 
                      key={index}
                      label={time}
                      value={time}
                      className={classes.formControlLabel}
                      control={<Radio className={classes.radioButton} color="primary" />} />
                  )
                })}
              </RadioGroup>
            </FormControl>
          </div>
        </StepContent>
      </Step>
      <Step disabled={ !appointmentTime }>
        <StepButton className={classes.stepButton} onClick={() => setStep(1)}>
          Confirm your patient details
        </StepButton>
        <StepContent>
          <section>
            <StripeProvider apiKey={ STRIPE_API_KEY }>
              <Elements>
                <AppointmentForm totalCost={ appointment.totalCost } onSubmit={ onSubmit } />
              </Elements>
            </StripeProvider>
          </section>
        </StepContent>
      </Step>
    </Stepper>
  )
  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar alt={info.name} src={info.photo} className={classes.avatar} />
          }
          title={providerName}
          subheader={info.specialty} // details?
        />
        <Divider />
        { error && <AlertContainer>{error}</AlertContainer>}
        { loading ? <Loader /> : !appointment.booked ? (
          stepper
        ): (
          <div className={classes.bookingComplete}>
            <Typography variant="h5" paragraph>Got it! Your booking is complete.</Typography>
            <Typography paragraph>Come back here at least 10 minutes before your appointment for your private video link.</Typography>
            <Button variant="contained" color="primary"
              className={classnames(classes.button, classes.buttonLink)}
              component={Link} to="/appointments">My appointments</Button>
            <Button variant="outlined" component={Link} to={`/provider/${appointment.provider._id}`} className={classes.button}>Back to the Provider</Button>
          </div>
        )}
      </Card>
    </div>
  )
}

BookAppointmentContainer.defaultProps = {
  availabilities: defaultAvailabilities
}

const mapStateToProps = (state) => ({
  availabilities: state.ProviderReducer.data.availabilities,
  appointment: state.ProviderReducer.appointment,
})

export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    { bookAppointment }
  )
)(BookAppointmentContainer)
