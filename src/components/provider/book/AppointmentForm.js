import React, { useState } from 'react'
import { injectStripe, CardElement } from 'react-stripe-elements'
import PatientAgreement from './PatientAgreement'
import { FormWrapper, ErrorMessage } from '../../common/form'
import { StyledPayment, ModalFooter, StyledButton } from './styles'
import { Typography } from '@material-ui/core';
import { useToggleField } from '../../../hooks/useMergeState';

const AppointmentForm = ({ totalCost, onSubmit, stripe }) => {
  const [cardError, setCardError] = useState()
  const [patientAgree, togglePatientAgree] = useToggleField()
  const handleChange = change => {
    setCardError((change.error && change.error.message) || undefined)
  }
  const handleSubmit = ev => {
    ev.preventDefault();
    if(!totalCost){
      onSubmit()
    } else {
      stripe.createSource({
        type: 'card',
        usage: 'reusable'
      }).then(result => {
        if(result.source){
          onSubmit(result.source)
        } else {
          console.error(result.error)
          const cardError = (result.error && result.error.message) || 'There was an error processing your card.'
          setCardError(cardError)
        }
      }).catch(error => {
        console.error(error)
        setCardError('There was an error processing your card.')
      })
    }
  }
  return (
    <FormWrapper onSubmit={ handleSubmit }>
      <PatientAgreement patientAgree={patientAgree} toggleAgree={ togglePatientAgree } />
      { !!totalCost && 
        <StyledPayment>
          <Typography>We collect credit card information to secure your care. If there are any adjustments to the final price, we will contact you and reconfirm before your visit.<br /><strong>You will never be charged before your appointment.</strong></Typography>
          { cardError && <ErrorMessage>{ cardError }</ErrorMessage>}
          <CardElement 
            onChange={ handleChange }
            onReady={()=>{}}
            onBlur={()=>{}}
            onFocus={()=>{}}
            style={{base: {fontSize: '1rem'}}} />
        </StyledPayment>
      }
      <ModalFooter>
        <div></div>
        <StyledButton type="submit" disabled={!patientAgree || cardError}>Book Appointment</StyledButton>
      </ModalFooter>
    </FormWrapper>
  )
}

export default injectStripe(AppointmentForm)