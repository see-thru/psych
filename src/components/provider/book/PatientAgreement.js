import React from 'react'
import { UncontrolledCollapse } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { colors } from '../../../utils/settings'
import { Typography, FormGroup, FormControlLabel, Checkbox, withStyles } from '@material-ui/core';

const styles = {
  patientAgreement: {
    marginTop: '10px',
    padding: '15px 0 10px',
    borderTop: `1px solid ${ colors.borderGray }`
  },
  formControlLabel: {
    marginBottom: 0
  },
  formBox:{
    border: "1px solid gray", 
    padding: "10px 15px", 
    maxHeight: "200px", 
    overflow: "scroll"
  },
  chevron: {
    marginLeft: "10px", 
    color: "black !important", 
    paddingTop: "2px"
  }
}

const d = new Date
const date = d.toDateString()

const PatientAgreement = ({ classes, patientAgree, toggleAgree }) => {
  return (
    <div className={classes.patientAgreement}>
      <p className="toggle-row" id="toggleAgreement" aria-label="Click to toggle agreement open">
        <Typography style={{textDecoration:"underline"}} variant="h6" component="span">Patient Concent and Privacy Notice<FontAwesomeIcon className={classes.chevron} icon="chevron-down" /></Typography>
      </p>
      <UncontrolledCollapse toggler="#toggleAgreement">
        <div className={classes.formBox}>
          <Typography variant="h6" style={{marginBottom:"1em"}}>Notice Of Privacy Policies (HIPAA Authorization)</Typography>
          <Typography paragraph > SeeThru 195 Montague 195 Montague St. <br/>Brooklyn, NY 11201</Typography>
          <Typography paragraph>Effective Date: {date}</Typography>
          <Typography paragraph>THIS NOTICE DESCRIBES HOW MEDICAL INFORMATION ABOUT YOU MAY BE USED AND DISCLOSED AND HOW YOU CAN GET ACCESS TO THIS INFORMATION. PLEASE REVIEW IT CAREFULLY.</Typography>
          <Typography paragraph>We are required by law to maintain the privacy of protected health information, this information may include: notes from your healthcare provider, your medical history, your test results, treatment notes and insurance information.</Typography>
          <Typography paragraph>We are also required to provide individuals with notice of our legal duties and privacy practices in regards to protected health information, and to notify affected individuals about breach of unsecured protected health information. This notice describes how we may use and disclose your medical information. It also describes your rights and our legal obligations with respect to your medical information. If you have any questions about this Notice, please contact our Privacy Officer at team@seethru.health.</Typography>
          <Typography paragraph>A. How This Practice May Use or Disclose Your Health Information without your permission.</Typography>
          <Typography paragraph>1. Treatment. We use medical information about you to provide you with our services or treatment and we may share your medical information with other providers involved in your care. For example, we may share your treatment results with your doctor, etc.</Typography>
          <Typography paragraph>2. Payment. We use and disclose medical information about you to obtain payment for the services we provide. For example, we give your health plan the information it requires before it will pay us. We may also disclose information to other health care providers to assist them in obtaining payment for services they have provided to you.</Typography>
          <Typography paragraph> 3. Health Care Operations. We may use and disclose medical information about you to operate this practice. For example, we may use and disclose this information to review and improve the quality of services we provide, or check on performance of our staff. Or we may use and disclose this information to get your health plan to authorize services or referrals. We may also use and disclose this information as necessary for medical reviews, legal services and audits, including fraud and abuse detection and compliance programs. We may also share your medical information with our "business associates," such as our billing service, that perform administrative services for us. We have a written contract with each of these business associates that contains terms requiring them and their subcontractors to protect the confidentiality and security of your protected health information. We may also share your information with other health care providers, health care clearinghouses or health plans that have a relationship with you.</Typography>
          <Typography paragraph>4. Appointment Reminders. We may use and disclose medical information to contact and remind you about appointments. Reminders may be sent in the mail, by email, or by phone call or voicemail message. If you do not wish to get reminders, please let us know. If you are not home, we may leave this information on your answering machine or in a message left with the person answering the phone.</Typography>
          <Typography paragraph>5. [FOR OFFICE VISITS ONLY] Sign In Sheet. We may use and disclose medical information about you by having you sign in when you arrive at our office. We may also call out your name when we are ready to see you.</Typography>
          <Typography paragraph>6. Required by Law. As required by law, we will use and disclose your health information, but we will limit our use or disclosure to the relevant requirements of the law. When the law requires us to report abuse, neglect or domestic violence, or respond to judicial or administrative proceedings, or to law enforcement officials, we will further comply with the requirements set forth below concerning those activities.</Typography>
          <Typography paragraph>7. Public Health. We may as required by law, to disclose your health information to public health authorities for purposes related to: preventing or controlling disease, injury or disability; reporting child, elder or dependent adult abuse or neglect; reporting domestic violence; reporting to the Food and Drug Administration problems with products and reactions to medications; and reporting disease or infection exposure.</Typography>
          <Typography paragraph>8. Health Oversight Activities. We may, and are sometimes required by law, to disclose your health information to health oversight agencies during the course of audits, investigations, inspections, licensure and other proceedings, subject to the limitations imposed by law.</Typography>
          <Typography paragraph>9. Judicial and Administrative Proceedings. We may, and are sometimes required by law, to disclose your health information in the course of any administrative or judicial proceeding to the extent expressly authorized by a court or administrative order. We may also disclose information about you in response to a subpoena, discovery request or other lawful process if reasonable efforts have been made to notify you of the request and you have not objected, or if your objections have been resolved by a court or administrative order.</Typography>
          <Typography paragraph>10. Law Enforcement. We may, and are sometimes required by law, to disclose your health information to a law enforcement official for purposes such as identifying or locating a suspect, fugitive, material witness or missing person, complying with a court order, warrant, grand jury subpoena and other law enforcement purposes.</Typography>
          <Typography paragraph>11. Public Safety. We may, and are sometimes required by law, to disclose your health information to appropriate persons in order to prevent or lessen a serious and imminent threat to the health or safety of a particular person or the general public.</Typography>
          <Typography paragraph>12. Specialized Government Functions. We may disclose your health information for military or national security purposes or to correctional institutions or law enforcement officers that have you in their lawful custody.</Typography>
          <Typography paragraph>13. Workers’ Compensation. We may disclose your health information as necessary to comply with workers’ compensation laws. For example, to the extent your care is covered by workers' compensation, we will make periodic reports to your employer about your condition. We are also required by law to report cases of occupational injury or occupational illness to the employer or workers' compensation insurer.</Typography>
          <Typography paragraph>14. Change of Ownership. In the event that this company is sold or merged with another organization, your health information/record will become the property of the new owner, although you will maintain the right to request that copies of your health information be transferred to practice or provider.</Typography>
          <Typography paragraph>15. Breach Notification. In the case of a breach of unsecured protected health information, we will notify you as required by law. If you have provided us with a current e-mail address, we may use e-mail to communicate information related to the breach. In some circumstances our business associate may provide the notification. We may also provide notification by other methods as appropriate.</Typography>
          <Typography paragraph>16. Psychotherapy Notes. We will not use or disclose your psychotherapy notes without your prior written authorization except for the following: 1) use by the originator of the notes for your treatment, 2) for training our staff, students and other trainees, 3) to defend ourselves if you sue us or bring some other legal proceeding, 4) if the law requires us to disclose the information to you or the Secretary of HHS or for some other reason, 5) in response to health oversight activities concerning your psychotherapist, 6) to avert a serious and imminent threat to health or safety, or 7) to the coroner or medical examiner after you die. To the extent you revoke an authorization to use or disclose your psychotherapy notes, we will stop using or disclosing these notes.</Typography>
          <Typography paragraph>B. Your Rights: You have the right to:</Typography>
          <Typography paragraph>1. Right to Request Special Privacy Protections. You can request us not to use or share your information for treatment, payment, or health care operations. You can also ask us not to share information with individuals involved in your care, e.g. family members or friends. You must make these requests in writing. We must share information when required by law. We reserve the right to accept or reject any other request, and will notify you of our decision.</Typography>
          <Typography paragraph>2. Right to Request Confidential Communications. You can request that you receive your health information in a specific way or at a specific location. For example, you may ask that we send information to a particular e-mail account or to your work address. You must make these requests in writing.</Typography>
          <Typography paragraph>3. Right to Inspect and Copy. You have the right to see your health information and request a copy of that information with limited exceptions. To access your medical information, you must submit a written request detailing what information you want access to, whether you want to inspect it or get a copy of it, and if you want a copy, your preferred form and format.</Typography>
          <Typography paragraph>4. Right to Amend or Supplement. You have a right to request that we amend or change your health information that you believe is incorrect or incomplete. You must make such requests in writing and provide a reason for the change. We don’t have to change your health information, and will provide you with information about the provider’s denial and how you can disagree with the denial. We may deny your request if we do not have the information, if we did not create the information (unless the person or entity that created the information is no longer available to make the amendment), if you would not be permitted to inspect or copy the information at issue, or if the information is accurate and complete as is. If we deny your request, you may submit a written statement of your disagreement with that decision, and we may, in turn, prepare a written rebuttal. All information related to any request to amend will be maintained and disclosed in conjunction with any subsequent disclosure of the disputed information.</Typography>
          <Typography paragraph>5. Right to an Accounting of Disclosures. You have a right to receive an accounting of disclosures of your health information made by this Organization. You must make such request in writing. This Organization does not have to account for the disclosures provided to you or pursuant to your written authorization, or as described in above paragraphs : (treatment), (payment), (health care operations), (notification and communication with family) and (specialized government functions) of Section A of this Notice of Privacy Practices or disclosures for purposes of research or public health</Typography>
          <Typography paragraph>6. Right to a Paper or Electronic Copy of this Notice. You have a right to notice of our legal duties and privacy practices with respect to your health information, including a right to a paper copy of this Notice of Privacy Practices, even if you have previously requested its receipt by e-mail.</Typography>
          <Typography paragraph>C. Changes to this Notice of Privacy Practices</Typography>
          <Typography paragraph>We reserve the right to change this Notice of Privacy Practices at any time in the future. After an amendment is made, the revised Notice of Privacy Protections will apply to all protected health information that we maintain, regardless of when it was created or received. We will keep a copy of the current notice posted on our website, and a copy will be available at each appointment.</Typography>
          <Typography paragraph>D. File Complaints</Typography>
          <Typography paragraph>You can file a complaint with us or with the government if you think that your information was used or shared in a way that is not allowed. You can also file a complaint when you were not allowed to view a copy of your information. To file a complaint with us, please contact our Privacy Officer listed at the top of this form. You can file a complaint with your regional office of the United States Office of Civil Rights. You will not be penalized in any way for filing a complaint.</Typography>
          
          <Typography variant="h6" style={{marginTop:"2em"}}>Consent Form for Telehealth</Typography><Typography paragraph></Typography>
          <Typography paragraph>I understand that telehealth or teletherapy involves the use of electronic information and communication technologies by a health care provider to deliver services to an individual when he/she is located at a different site than the provider; and hereby consent to receiving health care services to me via telehealth over secure video conferencing platform.</Typography>
          <Typography paragraph>I understand that the laws that protect privacy and the confidentiality of my medical information also apply to telehealth or teletherapy.</Typography>
          <Typography paragraph>I understand that while telehealth or teletherapy treatment has been found to be effective in treating a wide range of disorders, there is no guarantee that all treatment of all clients will be effective.</Typography>
          <Typography paragraph>I understand that there are potential risks involving technology, including but not limited to: Internet interruptions, and technical difficulties. I understand that technical difficulties with hardware, software, and internet connection may result in service interruption and that the health care provider is not responsible for any technical problems and does not guarantee that services will be available or work as expected.</Typography>
          <Typography paragraph>I understand that I am responsible for information security on my computer and in my own physical location. I understand that I am responsible for creating and maintaining my user name and password and not share these with another person. I understand that I am responsible to ensure privacy at my own location by being in a private location so other individuals cannot hear my conversation.</Typography>
          <Typography paragraph>I understand that I am required by law to be physically located in the state listed for my session on the the platform</Typography>
          <Typography paragraph>I understand that my health care provider or I can discontinue the telehealth/teletherapy services if it is felt that this type of service delivery does not benefit my needs.</Typography>
          <Typography paragraph>I have read and understand the information provided above regarding telehealth or teletherapy, have discussed it with my health care provider and all of my questions have been answered to my satisfaction. I hereby give my informed consent for the use of telehealth or teletherapy in my care.</Typography>
  
        </div>
      </UncontrolledCollapse>
      <FormGroup row>
        <FormControlLabel
          className={classes.formControlLabel}
          control={
            <Checkbox
              checked={patientAgree}
              onChange={toggleAgree}
              color="primary"
              value="patientAgreeCheckbox" />
          }
          label = "I have read, understand, and accept the PRIVACY POLICIES and CONSENT FOR TELEHEALTH disclosed in this notice."
        />
      </FormGroup>
    </div>
  )
}

export default withStyles(styles)(PatientAgreement)