import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import ImageUploader from 'react-images-upload';
import Chip from '@material-ui/core/Chip';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import * as filters from '../../../constants/filters'


const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width:"100%",
    paddingLeft:"2em"
  },
  header: {
    color:"grey",
    paddingLeft:"2em"
  },
  chip: {
    margin: theme.spacing.unit,
  },
  button:{
    margin:"1em 1em 1em 0",
    '&:focus':{
      outline:"none"
    },
    backgroundColor: "#214679",
    color:"white"
  },
  chip:{
    marginRight:"5px",
    marginTop:"1em"
  },
  label: {
    margin: "2em 0 1em 0", 
  }
});



class ClinicianIdentifiers extends React.Component {
  state = {
    states:[],
    age:"",
    typeOfProvider: [],
    issues:[],
    gender:[],
    states:[],
    modalities:[],
    focuses:[],
    experiences: []
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleSelect = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleArraySelect = event => {
    if (!this.state[event.target.name].includes(event.target.value)) {
      this.setState({ [event.target.name]: this.state[event.target.name].concat([event.target.value]) });
    }
  };

  addToStateHandler = (type, input) => event => {
    event.preventDefault()
    const newValue = [this.state[input]]
    this.setState({ [type]: this.state[type].concat(newValue) })
    this.setState({ [input]: "" })
  }

  handleDelete = (type, index) => {
    const array = [...this.state[type]]
    array.splice(index, 1)
    this.setState({
      [type]: array
    })
  }

  onFormSubmit= event => {
    event.preventDefault()
    console.log("here")
    this.props.changeTab(3)
  }

  render() {
    const { classes } = this.props;

    return (
      <Grid container spacing={24}>
        <Typography className={classes.header} variant="subtitle1">Clinician Identifiers</Typography>
        <Grid item>
          <form className={classes.container} noValidate onSubmit={this.onFormSubmit}>
            <Grid item xs={12} style={{marginBottom:"1em"}}>
              <Typography paragraph variant="caption" inline="false">Age</Typography>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth variant="outlined" className={classes.formControl}>
                <InputLabel
                  ref={ref => {
                    this.InputLabelRef = ref;
                  }}
                  htmlFor="age-selector"
                >
                </InputLabel>
                <Select
                  value={this.state.age}
                  onChange={this.handleSelect}
                  input={
                    <OutlinedInput
                      name="age"
                      id="age-selector"
                    />
                  }
                >
                  {filters.filterCats.find(filter => {return filter.id === 'age'}).options.map(option => {return( <MenuItem value={option.id}>{option.id}</MenuItem>)})}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} className={classes.label}>
              <Typography paragraph variant="caption" inline="false">Gender</Typography>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth variant="outlined" className={classes.formControl}>
                <InputLabel
                  ref={ref => {
                    this.InputLabelRef = ref;
                  }}
                  htmlFor="gender-selector"
                >
                </InputLabel>
                <Select
                  value={this.state.gender}
                  onChange={this.handleSelect}
                  input={
                    <OutlinedInput
                      name="gender"
                      id="gender-selector"
                    />
                  }
                >
                  {filters.filterCats.find(filter => {return filter.id === 'identifiers'}).options.find(opt => {return opt.name === 'Gender'}).options.map(option => {return( <MenuItem value={option.id}>{option.id}</MenuItem>)})}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} className={classes.label}>
              <Typography paragraph variant="caption" inline="false">Sexuality</Typography>
            </Grid>
            <Grid item xs={12}>
            <FormControl fullWidth variant="outlined" className={classes.formControl}>
              <InputLabel
                ref={ref => {
                  this.InputLabelRef = ref;
                }}
                htmlFor = "sexuality-selector"
              >
              </InputLabel>
              <Select
                value={this.state.gender}
                onChange={this.handleSelect}
                input={
                  <OutlinedInput
                    name = "sexuality"
                    id = "sexuality-selector"
                  />
                }
              >
                {filters.filterCats.find(filter => {return filter.id === 'identifiers'}).options.find(opt => {return opt.name === 'Sexuality'}).options.map(option => {return( <MenuItem value={option.id}>{option.id}</MenuItem>)})}
              </Select>
            </FormControl>
            </Grid>
            <Grid item xs={12} className={classes.label}>
              <Typography paragraph variant="caption" inline="false">Racial / Ethnic Identity</Typography>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth variant="outlined" className={classes.formControl}>
                <InputLabel
                  ref={ref => {
                    this.InputLabelRef = ref;
                  }}
                  htmlFor="racial-selector"
                >
                </InputLabel>
                <Select
                  value={this.state.gender}
                  onChange={this.handleSelect}
                  input={
                    <OutlinedInput
                      name = "racial"
                      id="racial-selector"
                    />
                  }
                >
                  {filters.filterCats.find(filter => {return filter.id === 'identifiers'}).options.find(opt => {return opt.name === 'Racial / Ethnic Identity'}).options.map(option => {return( <MenuItem value={option.id}>{option.id}</MenuItem>)})}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} className={classes.label}>
              <Typography paragraph variant="caption" inline="false">Faith</Typography>
            </Grid>          
            <Grid item xs={12}>
              <FormControl fullWidth variant="outlined" className={classes.formControl}>
                <InputLabel
                  ref={ref => {
                    this.InputLabelRef = ref;
                  }}
                  htmlFor="faith-selector"
                >
                </InputLabel>
                <Select
                  value={this.state.gender}
                  onChange={this.handleSelect}
                  input={
                    <OutlinedInput
                      name="faith"
                      id="faith-selector"
                    />
                  }
                >
                  {filters.filterCats.find(filter => {return filter.id === 'identifiers'}).options.find(opt => {return opt.name === 'Faith'}).options.map(option => {return( <MenuItem value={option.id}>{option.id}</MenuItem>)})}
                </Select>
              </FormControl>         
            </Grid>
            <Grid item xs={12} className={classes.label}>
              <Typography paragraph variant="caption" inline="false">Experiential</Typography>
            </Grid>    
            <Grid item xs={12} > 
              <FormControl fullWidth variant="outlined" className={classes.formControl}>
                <InputLabel
                  ref={ref => {
                    this.InputLabelRef = ref;
                  }}
                  htmlFor = "experience-input"
                >
                </InputLabel>
                <Select
                  value={this.state.focuses}
                  onChange={this.handleArraySelect}
                  input={
                    <OutlinedInput
                      name = "experiences"
                      id = "experience-select"
                    />
                  }
                >
                <MenuItem value="">
                  </MenuItem>
                  {filters.filterCats.find(filter => {return filter.id === 'identifiers'}).options.find(opt => {return opt.name === 'Experienced In'}).options.map(option => {return( <MenuItem value={option.id}>{option.id}</MenuItem>)})}
                </Select>
              </FormControl>              
              {
                this.state.experiences.map((experience, index) => {
                return(
                  <Chip
                    label = {experience}
                    className={classes.chip}
                    color="primary"
                    variant="outlined"
                    onDelete={() =>this.handleDelete('experiences', index)}
                  />
                )
              })}
            </Grid>
            <Button type="submit" className={classes.button} variant="contained" color="primary">Save Changes ></Button>
            </form>
        </Grid>
      </Grid>
    );
  }
}

ClinicianIdentifiers.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ClinicianIdentifiers);
