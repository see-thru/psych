import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import * as filters from '../../../constants/filters'


const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width:"100%",
    paddingLeft:"2em"
  },
  header: {
    color:"grey",
    paddingLeft:"2em"
  },
  chip: {
    margin: theme.spacing.unit,
  },
  button:{
    margin:"1em 1em 1em 0",
    '&:focus':{
      outline:"none"
    },
    backgroundColor: "#214679",
    color:"white"
  },
  chip:{
    marginRight:"5px",
    marginTop:"1em"
  },
  formControl: {
    minWidth: 120,
    margin:"1em 0",
      },
  selectEmpty: {
   marginTop: theme.spacing.unit * 2,
  },
  label: {
    margin: "2em 0 1em 0",
  }
});



class ProviderInfo extends React.Component {
  state = {
    open:"false",
    states:[],
    age:"",
    typeOfProvider: [],
    issues:[],
    gender:[],
    states:[],
    modalities:[],
    focuses:[],
    experiences: []
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleSelect = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleArraySelect = event => {
    if (!this.state[event.target.name].includes(event.target.value)) {
      this.setState({ [event.target.name]: this.state[event.target.name].concat([event.target.value]) });
    }
  };

  addToStateHandler = (type, input) => event => {
    event.preventDefault()
    const newValue = [this.state[input]]
    this.setState({ [type]: this.state[type].concat(newValue) })
    this.setState({ [input]: "" })
  }

  handleDelete = (type, index) => {
    const array = [...this.state[type]]
    array.splice(index, 1)
    this.setState({
      [type]: array
    })
  }

  onFormSubmit= event => {
    event.preventDefault()
    console.log("here")
    this.props.changeTab(2)
  }

  render() {
    const { classes } = this.props;
    return (
      <Grid container spacing={24}>
        <Typography className={classes.header} variant="subtitle1">Clinician Info</Typography>
        <hr/>
        <Grid item>
          <form className={classes.container} noValidate onSubmit={this.onFormSubmit}>
            <Grid item ex={12}>
              <Typography paragraph variant="caption" inline="false" style={{marginTop:"1em", paddingLeft:"-40px", marginBottom:"1em"}} >Type of Clinician</Typography>
            </Grid>
            <Grid item xs={12} style={{marginBottom:"1em"}}>
              <FormControl fullWidth variant="outlined" className={classes.formControl}>
                <InputLabel
                  ref={ref => {
                    this.InputLabelRef = ref;
                  }}
                  htmlFor = "type-of-provider-select"
                >
                </InputLabel>
                <Select
                  value={this.state.typeOfProvider}
                  onChange={this.handleArraySelect}
                  input={
                    <OutlinedInput
                      name = "typeOfProvider"
                      id="type-of-provider-select"
                    />
                  }
                >
                  {filters.filterCats.find(filter => {return filter.id === 'type of provider'}).options.map(option => {return( <MenuItem value={option.id}>{option.id}</MenuItem>)})}
                </Select>
              </FormControl>
              {
                this.state.typeOfProvider.map((type, index) => {
                return(
                  <Chip
                    label={type}
                    className={classes.chip}
                    color="primary"
                    variant="outlined"
                    onDelete={() =>this.handleDelete('typeOfProvider', index)}
                  />
                )
              })}
            </Grid>
            <Grid item ex={12}>
              <Typography paragraph variant="caption" inline="false" className={classes.label} >Licensed In</Typography>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth variant="outlined" className={classes.formControl}>
                <InputLabel
                  ref={ref => {
                    this.InputLabelRef = ref;
                  }}
                  htmlFor = "state-select"
                >
                </InputLabel>
                <Select
                  value={this.state.states}
                  onChange={this.handleArraySelect}
                  input={
                    <OutlinedInput
                      name = "states"
                      id="state-select"
                    />
                  }
                >
                  {filters.usStates.map(state => {return( <MenuItem value={state}>{state}</MenuItem>)})}
                </Select>
              </FormControl>              
              {
                this.state.states.map((state, index) => {
                return(
                  <Chip
                    label={state}
                    className={classes.chip}
                    color="primary"
                    variant="outlined"
                    onDelete={() =>this.handleDelete('states', index)}
                  />
                )
              })}
            </Grid>
            <Grid item ex={12}>
              <Typography paragraph variant="caption" inline="false" className={classes.label} >Specialties</Typography>
            </Grid>
            <Grid item xs={12}>
             <FormControl fullWidth variant="outlined" className={classes.formControl}>
              <InputLabel
                ref={ref => {
                  this.InputLabelRef = ref;
                }}
                htmlFor="issues-input"
              >
              </InputLabel>
              <Select
                value={this.state.issues}
                onChange={this.handleArraySelect}
                input={
                  <OutlinedInput
                    name="issues"
                    id="issue-select"
                  />
                }
              >
              <MenuItem value="">
                <em>None</em>
                </MenuItem>
                {filters.filterCats.find(filter => {return filter.id === 'issues'}).options.map(option => {return( <MenuItem value={option.id}>{option.id}</MenuItem>)})}
              </Select>
            </FormControl>
            {
              this.state.issues.map((issue, index)=> {
              return(
                <Chip
                  label={issue}
                  className={classes.chip}
                  color="primary"
                  variant="outlined"
                  onDelete={() =>this.handleDelete('issues', index)}
                />
              )
            })}
          </Grid>
            <Grid item ex={12}>
              <Typography paragraph variant="caption" inline="false" className={classes.label} >Modalities</Typography>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth variant="outlined" className={classes.formControl}>
                <InputLabel
                  ref={ref => {
                    this.InputLabelRef = ref;
                  }}
                  htmlFor="modality-input"
                >
                </InputLabel>
                <Select
                  value={this.state.modalities}
                  onChange={this.handleArraySelect}
                  input={
                    <OutlinedInput
                      name = "modalities"
                      id="modality-select"
                    />
                  }
                >
                <MenuItem value="">
                  </MenuItem>
                  {filters.filterCats.find(filter => {return filter.id === 'modalities'}).options.map(option => {return( <MenuItem value={option.id}>{option.id}</MenuItem>)})}
                </Select>
              </FormControl>
              {
                this.state.modalities.map((modality, index) => {
                return(
                  <Chip
                    label = {
                      modality
                    }
                    className={classes.chip}
                    color="primary"
                    variant="outlined"
                    onDelete={() =>this.handleDelete('modalities', index)}
                  />
                )
              })}
          </Grid>
            <Grid item ex={12}>
              <Typography paragraph variant="caption" inline="false" className={classes.label} >Focuses</Typography>
            </Grid>
            <Grid item xs={12}>
            <FormControl fullWidth variant="outlined" className={classes.formControl}>
              <InputLabel
                ref={ref => {
                  this.InputLabelRef = ref;
                }}
                htmlFor="focus-input"
              >
              </InputLabel>
              <Select
                value={this.state.focuses}
                onChange={this.handleArraySelect}
                input={
                  <OutlinedInput
                    name = "focuses"
                    id="focus-select"
                  />
                }
              >
              <MenuItem value="">
                </MenuItem>
                {filters.filterCats.find(filter => {return filter.id === 'focuses'}).options.map(option => {return( <MenuItem value={option.id}>{option.id}</MenuItem>)})}
              </Select>
            </FormControl>              {
                this.state.focuses.map((focus, index) => {
                return(
                  <Chip
                    label = {focus}
                    className={classes.chip}
                    color="primary"
                    variant="outlined"
                    onDelete={() =>this.handleDelete('focuses', index)}
                  />
                )
              })}
            </Grid>
            <Grid item xs={12}>
              <Button type="submit" className={classes.button} variant="contained" color="primary">Save and Continue ></Button>
            </Grid>   
          </form>
        </Grid>
      </Grid>
    );
  }
}


ProviderInfo.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(ProviderInfo);

