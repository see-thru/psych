import React from 'react';
import { connect } from 'react-redux'
import { getProvider } from '../../../actions/provider'

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';

import { VTabs, MyTab, TabContainer } from './VerticalTabs'
import ClinicianProfile from './ClinicianProfile'
import ClinicianInfo from './ClinicianInfo'
import ClinicianIdentifiers from './ClinicianIdentifiers'

const styles = theme => ({
  root: {
    flexGrow: 1,
    display:'flex',
    backgroundColor: theme.palette.background.paper,
  },
  tabRoot: {
    textTransform: 'initial',
    minWidth: 100,
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing.unit * 4,
    textAlign:"right",
    '&:hover': {
      color: '#0F1F4D',
      opacity: 1,
    },
    '&$tabSelected': {
      color: '#0F1F4D',
      fontWeight: theme.typography.fontWeightBolder,
    },
    '&:focus': {
      color: '#0F1F4D',
      outline:"none"
    },
  },
  typography: {
    margin: theme.spacing.unit * 3,
  },
  container: {
    paddingLeft: "3em", 
    paddingTop: "2em",
    marginBottom:"10em",
  },
  formContainer: {
    borderLeft: "1px solid grey",
  }
});

class ClinitianZoneContainer extends React.Component {
  
  state= {
    activeIndex: 0
  }

  handleChange = (_, activeIndex) => this.setState({ activeIndex })

  changeTab = (num) => {
    this.setState({ activeIndex: num})
  }

  render() {
    const { classes } = this.props;
    const { activeIndex } = this.state;
    return (
      <React.Fragment>
        <Grid className={classes.container} container spacing={24}>
          <Grid item xs={12}>
            <Typography variant="h2" className={classes.typography}>Clinician Portal</Typography>
          </Grid>
          <Grid item xs={12} sm={3} alignItems="right">
              <VTabs
                value={activeIndex}
                onChange={this.handleChange}
                className={classes.tabsContainer}
              >
                <MyTab
                  disableRipple
                  classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                  label = "Profile"
                />
                <MyTab
                  disableRipple
                  classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                  label="Clinical Info"
                />
                <MyTab
                  disableRipple
                  classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                  label="Identifiers"
                />
                <MyTab
                  disableRipple
                  classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                  label="Services"
                />
              </VTabs>
            </Grid>
            <Grid item xs={12} sm={9} className={classes.formContainer}>
              { activeIndex === 0 && <TabContainer><ClinicianProfile changeTab={this.changeTab} /> </TabContainer> }
              { activeIndex === 1 && <TabContainer><ClinicianInfo changeTab={this.changeTab}  /></TabContainer> }
              { activeIndex === 2 && <TabContainer><ClinicianIdentifiers changeTab={this.changeTab}  /></TabContainer> }
              { activeIndex === 3 && <TabContainer>Services</TabContainer> }
            </Grid>
          </Grid>
      </React.Fragment>
    );  
  }
}

ClinitianZoneContainer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ClinitianZoneContainer);
