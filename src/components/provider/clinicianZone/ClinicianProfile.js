import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import ImageUploader from 'react-images-upload';
import Chip from '@material-ui/core/Chip';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import * as filters from '../../../constants/filters'


const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width:"100%",
    paddingLeft:"2em"
  },
  header: {
    color:"grey",
    paddingLeft:"2em"
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  button:{
    margin:"1em 1em 1em 0",
    '&:focus':{
      outline:"none"
    },
    backgroundColor: "#214679",
    color:"white"
  },
  chip:{
    marginRight:"5px",
    marginTop:"1em",
  }
});



class ClinicianProfile extends React.Component {
  state = {
    name:"",
    practiceName:"",
    email:"",
    phoneNumber:"",
    picture:[],
    degrees:[],
    education: [],
    languages: [],
    insurances:[],
    description:"",
    websiteURL: "",
    npi:"",
    edInput: "",
    lgInput:""
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  onDrop = (pictureFiles, pictureDataURLs) => {
    this.setState({
      picture: this.state.picture.concat(pictureFiles)
    })
  }
  addToStateHandler = (type, input) => event => {
    event.preventDefault()
    const newValue = [this.state[input]]
    this.setState({ [type]: this.state[type].concat(newValue) })
    this.setState({ [input]: "" })
  }

    handleArraySelect = event => {
    if (!this.state[event.target.name].includes(event.target.value)) {
      this.setState({ [event.target.name]: this.state[event.target.name].concat([event.target.value]) });
    }
  };

  handleDelete = (type, index) => {
    const array = [...this.state[type]]
    array.splice(index, 1)
    this.setState({
      [type]: array
    })
  }

  onFormSubmit= event => {
    event.preventDefault()
    console.log("here")
    this.props.changeTab(1)
  }






  render() {
    const { classes } = this.props;

    return (
      <Grid container spacing={24}> 
         <Typography className={classes.header} variant="subtitle1">Your Profile </Typography>
        <Grid>
          <form className={classes.container} noValidate onSubmit={this.onFormSubmit}>
            <Grid item xs={12}>
              <TextField
                id="name"
                label="Name"
                fullWidth
                type="text"
                margin="normal"
                variant="outlined"
                name="name"
                value={this.state.name}
                onChange={this.handleChange('name')}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="practice-name"
                label="Practice Name"
                fullWidth
                type="text" 
                margin="normal"
                variant="outlined"
                name="practiceName"
                value={this.state.practiceName}
                name = "practiceName"
                onChange={this.handleChange('practiceName')}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="email"
                label="Email"
                fullWidth 
                type = "email"
                margin="normal"
                variant="outlined"
                name="email"
                value={this.state.email}
                onChange={this.handleChange('email')}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="phone-number"
                label="Phone Number"
                fullWidth
                margin="normal"
                variant="outlined"
                name="phoneNumber"
                value={this.state.phoneNumber}
                onChange={this.handleChange('phoneNumber')}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography variant="caption" inline="false" style={{marginTop:"1em", display:"block"}} >Profile Picture</Typography>
            </Grid>     
            <Grid item xs={12}>
              <ImageUploader
                withIcon={true}
                buttonText='Choose image'
                onChange={this.onDrop}
                imgExtension={['.jpg', '.gif', '.png', '.gif']}
                maxFileSize={5242880}
                singleImage
                withPreview
                labelStyles={{paddingLeft:"1em"}}
              />
            </Grid>
            <Grid item ex={12}>
             <Typography paragraph variant="caption" inline="false" style={{marginTop:"1em", paddingLeft:"-40px", marginBottom:"1em"}} >Education</Typography>
            </Grid>
            <Grid item xs={12}>
              {
                this.state.education.map((ed, index)=> {
                return(
                  <Chip
                    label={ed}
                    className={classes.chip}
                    color="primary"
                    variant="outlined"
                    onDelete={() =>this.handleDelete('education', index)}
                  />
                )
              })}
            </Grid>
            <Grid item xs={12}>
              <form onSubmit={this.addToStateHandler('education', "edInput")}>
                <TextField
                  style={{width:"80%", marginRight:"1em"}}
                  id="education"
                  margin="normal"
                  type="text"
                  value={this.state.edInput}
                  onChange={this.handleChange('edInput')}
                />
                <Button type="submit" variant="contained" size="small" color="default" className={classes.button}>
                Add
                  <AddIcon className={classes.rightIcon} />
                </Button>
              </form>
            </Grid>
            <Grid item ex={12}>
             <Typography paragraph variant="caption" inline="false" style={{marginTop:"1em", paddingLeft:"-40px", marginBottom:"1em"}} >Languages</Typography>
            </Grid>
            <Grid item xs={12}>
              {
                this.state.languages.map((lg, index) => {
                return(
                  <Chip
                    label={lg}
                    className={classes.chip}
                    color="primary"
                    variant="outlined"
                    onDelete={() =>this.handleDelete('languages', index)}
                  />
                )
              })}
            </Grid>
            <Grid item xs={12}>
              <form onSubmit={this.addToStateHandler('languages', "lgInput")}>
                <TextField
                  style={{width:"80%", marginRight:"1em"}}
                  id="languages"
                  margin="normal"
                  type="text"
                  value={this.state.lgInput}
                  onChange={this.handleChange('lgInput')}
                />
                <Button type="submit" variant="contained" size="small" color="default" className={classes.button}>
                Add
                  <AddIcon className={classes.rightIcon} />
                </Button>
              </form>
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="description"
                label="Description"
                style={{ }}
                fullWidth
                type="text"
                margin="normal"
                variant="outlined"
                name="description"
                value={this.state.description}
                onChange={this.handleChange('description')}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="website"
                label = "Website"
                style={{ }}
                fullWidth
                type="text"
                margin="normal"
                variant="outlined"
                name = "website"
                value={this.state.website}
                onChange={this.handleChange('website')}
            />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="npi"
                label = "NPI"
                style={{ }}
                fullWidth
                type="text"
                margin="normal"
                variant="outlined"
                name = "npi"
                value={this.state.npi}
                onChange={this.handleChange('npi')}
              />
            </Grid>
            <Grid item ex={12}>
            <Typography paragraph variant="caption" inline="false" style={{marginTop:"1em", paddingLeft:"-40px", marginBottom:"1em"}} >Insurances Execepted</Typography>
          </Grid>
            <Grid item xs={12} style={{marginBottom:"1em"}}>
            <FormControl fullWidth variant="outlined" className={classes.formControl}>
              <InputLabel
                ref={ref => {
                  this.InputLabelRef = ref;
                }}
                htmlFor = "insurance-select"
              >
              </InputLabel>
              <Select
                value={this.state.typeOfProvider}
                onChange={this.handleArraySelect}
                input={
                  <OutlinedInput
                    name = "insurances"
                    id="insurance-select"
                  />
                }
              >
                {filters.filterCats.find(filter => {return filter.id === 'insurances'}).options.map(option => {return( <MenuItem value={option.id}>{option.name}</MenuItem>)})}
              </Select>
            </FormControl>
            {
              this.state.insurances.map((type, index) => {
              return(
                <Chip
                  label={type}
                  className={classes.chip}
                  color="primary"
                  variant="outlined"
                  onDelete={() =>this.handleDelete('insurances', index)}
                />
              )
            })}
          </Grid>
            <Grid item xs={12}>
            <Button type="submit" className={classes.button} variant="contained" color="primary">Save and Continue ></Button>
          </Grid>             
          </form>          
        </Grid>
      </Grid>
    );
  }
}

ClinicianProfile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ClinicianProfile);
