import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';


export const VTabs = withStyles(theme => ({
  flexContainer: {
    flexDirection: 'column'
  },
  indicator: {
    display: 'none',
  }
}))(Tabs)

export const MyTab = withStyles(theme => ({
  selected: {
    color: '#0F1F4D',
    borderRight: '4px solid #004679',
    paddingRight:"5px"
  }
}))(Tab);

export function TabContainer(props) {
  return (
    <Typography component="div" style={{ maxWidth:"70%"}}>
      {props.children}
    </Typography>
  );
}
export default VTabs

