import React from 'react';
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { compose } from 'redux'
import { withRouter } from 'react-router-dom'
import { toggleService, createAppointment } from '../../../actions/provider'
import { isArrayLength } from '../../../utils/arrays'
import { ServiceCost, FormattedCost } from '../../common/elements'
import { Typography, Button, Divider, List, ListItem, ListItemText, 
  ListItemSecondaryAction, IconButton 
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete'

const styles = theme => ({
  root: {
    padding: '10px',
  },
  buttons: {
    margin: '15px 0',
    textAlign: 'center'
  },
  button: {
    color: 'white !important'
  },
});

const ServicesDrawer = ({ classes, services, toggleService, createAppointment, history }) => {
  const selectedServices = (services || []).filter(s => s.selected)
  const totalCost = selectedServices.reduce((total, s) => total + (s.cost || 0), 0)
  return isArrayLength(selectedServices) ? (
    <div className={classes.root}>
      <Typography variant="h6">Selected Services:</Typography>
      <List>
        { selectedServices.map((s, index) => (
          <div key={index}>
            { index > 0 && <Divider light /> }
            <ListItem>
              <ListItemText primary={s.name} secondary={<ServiceCost cost={s.cost} />} />
              <ListItemSecondaryAction>
                <>
                  { s.locked ||
                    <IconButton aria-label="Remove Service" onClick={() => toggleService(s.index)}>
                      <DeleteIcon />
                    </IconButton>
                  }
                </>
              </ListItemSecondaryAction>
            </ListItem>
          </div>
        ))}
      </List>
      
      <Typography paragraph>Total Cost: <FormattedCost cost={totalCost} /> USD</Typography>
      <div className={classes.buttons}>
        <Button variant="contained" color="primary" className={classes.button}
          fullWidth
          onClick={() => createAppointment(history, selectedServices)}>
          Book Appointment
        </Button>
      </div>
    </div>
  ) : (
    <div className={classes.root}>
      <Typography>No services selected</Typography>
    </div>
  )
}

const mapStateToProps = (state) => ({
  services: state.ProviderReducer.services,
})

export default compose(
  withRouter,
  withStyles(styles),
  connect(
    mapStateToProps,
    { toggleService, createAppointment }
  )
)(ServicesDrawer);