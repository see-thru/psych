import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer'
import IconButton from '@material-ui/core/IconButton';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { toggleDrawer } from '../../../actions/provider'
import Services from './base'
import { values } from '../../../utils/settings';

const drawerWidth = 300

const styles = theme => ({
  drawer: {
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    minHeight: values.headerHeight,
    justifyContent: 'flex-end',
  },
  drawerPaper: {
    width: drawerWidth,
  },
});

const ServicesDrawer = ({ classes, isOpen, toggleDrawer }) => {
  return (
    <nav className={classes.drawer}>
      <Drawer
        variant="persistent"
        anchor="right"
        open={isOpen}
        onClose={toggleDrawer}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={toggleDrawer}>
            <ChevronRightIcon />
          </IconButton>
        </div>
        <Divider />
        <Services />
      </Drawer>
    </nav>
  )
}

const mapStateToProps = (state) => ({
  isOpen: state.ProviderReducer.drawerOpen
})

export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    { toggleDrawer }
  )
)(ServicesDrawer)