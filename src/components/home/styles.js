import styled from 'styled-components'
import Media, { MaxWidth } from '../../utils/media';

export const StyledHome = styled.div`
  .banner-image {
    position: relative;
    img {
      max-height: 375px;
      width: 100%;
      object-fit: cover;
      min-height: 100px;
    }
    .banner-image-fade {
      background: transparent;
      background: linear-gradient(to bottom, rgba(255,255,255,0) 0%,rgba(0,80,126,0.2) 80%);
      position: absolute;
      height: 100%;
      width: 100%;
      top: 0;
      left: 0;
    }
  }
`

export const StyledBanner = styled.div`
  background: linear-gradient(to bottom, #00507e, #517990);
  position: relative;
  padding: 40px 20px;
  .banner-content {
    color: white;
    text-align: center;
    max-width: 900px;
    margin: 0 auto;
    h1 {
      font-size: 4.3rem;
      line-height: 4.6rem;
      text-shadow: 1px 1px rgba(0,0,0,0.4);
    }
    h2 {
      font-weight: 700;
      font-size: 1.5rem;
      line-height: 1.8rem;
      margin-top: 0;
      text-shadow: 1px 1px rgba(0,0,0,0.4);
    }
    > p {
      font-size: 1.3rem;
      line-height: 1.6rem;
      text-shadow: 1px 1px rgba(0,0,0,0.4);
    }
  }
  > img {
    max-width: 607px;
    top: 15px;
    position: relative;
    width: 100%;
    align-self: flex-end;
  }
  ${
    Media.medium`
      padding: 20px 10px;
      .banner-content {
        h1 {
          font-size: 3.2rem;
          line-height: 3.5rem;
        }
        h2 {
          font-size: 1.3rem;
          line-height: 1.5rem;
        }
        > p {
          font-size: 1rem;
          line-height; 1.2rem;
        }
      }
    `
  }
  ${
    MaxWidth(500)`
      .banner-content {
        h1 { 
          font-size: 2.1rem;
          line-height: 2.3rem;
        }
        h2 {
          font-size: 1.1rem;
          line-height: 1.3rem
        }
        > p {
          font-size: .9rem;
          line-height: 1rem
        }
      }
    `
  }
`

export const StyledInputs = styled.div`
  display: flex;
  justify-content: space-between;
  position: relative;
  margin: 25px 0 0;
  
  > div {
    display: inline-block;
    flex-basis: 38%;
    &:first-of-type {
      flex-basis: 60%;
    }
  }
  ${
    MaxWidth(500)`
      display: block;
      > div {
        width: 100%;
      }
    `
  }
`
