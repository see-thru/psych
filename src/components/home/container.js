import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import {toggleSignInModal} from '../../actions/auth'
import { StyledHome, StyledBanner } from './styles'
import BannerSearchForm from './BannerSearchForm';

export const HomeContainer = ({
  location,
  toggleSignInModal,
  history
}) => {
  useEffect(() => {
    if(location.hash === '#signin'){
      toggleSignInModal(true)
      history.push({ hash: '' }) // does container rerender? I don't think so...
    }
  }, [])
  return (
    <StyledHome>
      <div className="banner-image">
        <img src="/images/bh-banner.jpg" alt="SeeThru banner" />
        <div className="banner-image-fade" />
      </div>
      <StyledBanner>
        <div className="banner-content">
          <h1>Welcome to SeeThru</h1>
          <h2>Making Behavioral Health More Accessible</h2>
          <p>Find the right clinician to get the help you need when and where you want it</p>
          <BannerSearchForm />
        </div>
      </StyledBanner> 
    </StyledHome>
  )
}

export default connect(
  () => ({}),
  { toggleSignInModal }
)(HomeContainer)