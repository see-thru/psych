import React, { useCallback, useEffect } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { withRouter } from 'react-router-dom'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import { required } from 'redux-form-validators'
// import qs from 'qs'
import { newSearch, getUsState } from '../../actions/search'
import { StyledInputs } from './styles'
import { BigInput, BigTypeahead } from './inputs'
import { usStates } from '../../constants/filters';
import { Button, withStyles } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { colors } from '../../utils/settings';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    backgroundColor: colors.darkBlue,
    '&:hover': {
      backgroundColor: '#003861',
    },
  },
  buttonIcon: {
    marginRight: theme.spacing.unit,
  },
})

const Form = ({ 
  classes,
  history,
  currentUsState,
  chosenUsState,
  getUsState,
  change,
  newSearch,
  handleSubmit
}) => {
  useEffect(() => {
    if (currentUsState && usStates.includes(currentUsState)) {
      if(!chosenUsState) { change('state', currentUsState) }
    } else { // check if chosen?
      getUsState()
    }
  }, [currentUsState])

  const onSubmit = useCallback(e => {
    // const query = qs.stringify({ issue: e.searchText, state: e.state })
    const query = ''
    history.push({ 
      pathname: '/search',
      search: query ? `?${query}` : undefined
    })
    newSearch(e.state, e.searchText)
  }, [ history ]);
  
  return (
    <form
      onSubmit={ handleSubmit(onSubmit) }
      autoComplete="off">
      <StyledInputs>
        <Field
          name="searchText"
          component={ BigInput }
          placeholder="looking for a clinician who specializes in..." 
          formText="e.g. Addiction, Depression, Couples Counseling, Mental Wellness" />
        <Field
          name="state"
          options={ usStates }
          component={ BigTypeahead }
          validate={ [ required() ] }
          placeholder = "Where will you be receiving your care?"
          formText = "Care with SeeThru is currently only available  in the states listed above. Sign up to join our mailing list to find out when we land in your state." / >
      </StyledInputs>
      <Button 
        variant="contained" 
        fullWidth 
        color="primary" 
        size="large" 
        type="submit"
        className={classes.button}>
        <SearchIcon className={classes.buttonIcon} /> Find a Clinician
      </Button>
    </form>
  )
}

const formName = 'bannerForm'
const selector = formValueSelector(formName)
const mapStateToProps = state => ({
  currentUsState: state.FiltersReducer.currentUsState,
  chosenUsState: selector(state, 'state')
})

export default compose(
  reduxForm(
    { form: formName }
  ),
  connect(
    mapStateToProps,
    { newSearch, getUsState }
  ),
  withRouter,
  withStyles(styles)
)(Form)
