import React from 'react'
import styled from 'styled-components'
import { Row } from 'reactstrap'
import CurrencyFormat from 'react-currency-format'

export const StyledRow = styled(Row)`
  align-items: center;
  > div { 
    margin-bottom: 15px;
  }
`
export const StyledDiv = styled.div`
  h3 {
    font-size: 1.2rem;
  }
`
export const Cost = ({ cost }) => cost ? (
  <CurrencyFormat 
    value={cost}
    displayType={'text'}
    thousandSeparator={true}
    prefix={'$'}
    decimalScale={2} 
    fixedDecimalScale={ (cost%1) > 0 } />
) : null

export const Number = ({ value }) => value ? (
  <CurrencyFormat 
    value={value}
    displayType={'text'}
    thousandSeparator={true} />
) : null