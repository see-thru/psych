import React from 'react'
import { Col } from 'reactstrap'
import Form from './form'
import { Container } from '../common/reactstrap'
import { StyledRow, StyledDiv, Cost, Number } from './elements'

const Calculator = ({
  onReset,
  onSubmit,
  totalHours,
  costPerHour,
  costPerProcedure,
  numberOfProcedure
}) => (
  <Container>
    <StyledRow>
      <Col md="6">
        <Form
          onReset={ onReset }
          onSubmit={ onSubmit } />
      </Col>
      <Col md="6">
        <StyledDiv>
          <h3>Your Hourly Rate: <Cost cost={costPerHour} /></h3>
          <h3>Total Work Hours: <Number value={totalHours} /></h3>
          <hr />
          <h3>Cost Per Appointment: <Cost cost={costPerProcedure} /> </h3>
          <h3>Estimated Number of Appointments Per Year: <Number value={numberOfProcedure} /> </h3>
        </StyledDiv>
      </Col>
    </StyledRow>
  </Container>
)

export default Calculator
