import React, { useState } from 'react'

import GetTotalHours from '../../utils/calculator/get-total-hours'
import GetCostPerHour from '../../utils/calculator/get-cost-per-hour'
import ConvertToNumbers from '../../utils/calculator/convert-to-numbers'
import GetCostPerProcedure from '../../utils/calculator/get-cost-per-procedure'
import GetNumberOfProcedure from '../../utils/calculator/get-number-of-procedure'
import Calculator from './base'

const useCalculator = () => {
  const [results, setResults] = useState({})
  const submitValues = values => {
    const valuesConvertedToNumbers = ConvertToNumbers(
      values.annualOverhead,
      values.hoursPerWeek,
      values.weeksPerYear,
      values.minutesPerProcedure,
      values.desiredAnnualIncome
    )
    const { 
      annualOverhead,
      hoursPerWeek,
      weeksPerYear,
      minutesPerProcedure,
      desiredAnnualIncome,
     } = valuesConvertedToNumbers
  
    const hourPerProcedure = minutesPerProcedure / 60.0
    const totalHours = GetTotalHours(hoursPerWeek, weeksPerYear)
    const numberOfProcedure = GetNumberOfProcedure(totalHours, hourPerProcedure)
    const costPerHour = GetCostPerHour(desiredAnnualIncome, annualOverhead, totalHours)
    const costPerProcedure = GetCostPerProcedure(costPerHour, hourPerProcedure)
    setResults({
      totalHours,
      costPerHour,
      costPerProcedure,
      numberOfProcedure
    })
  }
  const clearValues = () => { setResults({}) }

  return [results, submitValues, clearValues]
}

const CalculatorContainer = () => {
  const [results, onSubmit, onReset] = useCalculator()
  return (
    <Calculator 
      onSubmit={onSubmit}
      onReset={onReset}
      { ...results } />
  )
}

export default CalculatorContainer
