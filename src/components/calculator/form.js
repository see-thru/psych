import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { required, numericality } from 'redux-form-validators'
import { Input, FormWrapper } from '../common/form'
import { createNumberMask } from 'redux-form-input-masks'
import { SubmitButton, DefaultButton } from '../common/elements'

const currencyMask = createNumberMask({
  prefix: '$ ',
  locale: 'en-US',
  allowEmpty: true
})

const Form = ({
  reset,
  onReset,
  onSubmit, 
  pristine, 
  submitting, 
  handleSubmit, 
}) => (
  <FormWrapper onSubmit={ handleSubmit(onSubmit) }>
    <Field
      title="Desired Annual Income"
      name="desiredAnnualIncome"
      placeholder="Enter desired annual income"
      type="tel"
      component={ Input }
      {...currencyMask}
      validate={ [
        required(),
        numericality({ '>': 0 })
      ] }
    />
    <Field
      title="Annual Overhead"
      name="annualOverhead"
      placeholder="Enter annual overhead"
      component={ Input }
      {...currencyMask}
      type="tel"
      min="0.00"
      step="0.01"
      validate={ [
        required(),
        numericality({ '>=': 0 })
      ] }
    />
    <Field
      title="Desired hours per week"
      name="hoursPerWeek"
      placeholder="Enter desired hours per week"
      component={ Input }
      type="number"
      validate={ [
        required(),
        numericality({ '>': 0, '<=': 168 })
      ] }
    />
    <Field
      title="Desired weeks per year"
      name="weeksPerYear"
      placeholder="Enter desired weeks per year"
      component={ Input }
      type="number"
      validate={ [
        required(),
        numericality({ '>': 0, '<=': 52 })
      ] }
    />
    <Field
      title="Average time allotment for most common appoinment (minutes)"
      name="minutesPerProcedure"
      placeholder="Enter minutes per appointment"
      component={ Input }
      type="number"
      validate={ [
        required(),
        numericality({ '>': 0 })
      ] }
    />
    <SubmitButton 
      type="submit" 
      disabled={ pristine || submitting }>
      Calculate
    </SubmitButton>
    <DefaultButton 
      disabled={ pristine || submitting } 
      onClick={() => { reset(); onReset() }}>
      Clear
    </DefaultButton>
  </FormWrapper>
)

export default reduxForm({
  form : 'calculatorForm'
})(Form)
