import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Route, Redirect } from 'react-router-dom'

// setState(continueTo) for Redirect?
// also, why not just use history.push?
const PrivateRoute = ({
  component: Component,
  isLogged,
  ...rest
}) => (
  <Route
    {...rest}
    render={ (props) => isLogged === true
      ? <Component {...props} />
      : <Redirect to={ { pathname: '/', state: { from: props.location }, hash: 'signin' } } />
    }
  />
)
 
const mapStateToProps = (state) => ({
  isLogged: state.AuthReducer.isLogged
})

PrivateRoute.propTypes = {
  isLogged: PropTypes.bool
}

export default connect(
  mapStateToProps
)( PrivateRoute ) 
