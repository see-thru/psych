import styled from 'styled-components'
import { colors, values } from '../../utils/settings'
import Media, { MaxWidth, breakpoints } from '../../utils/media'

export const StyledFooterContainer = styled.div`
  display: flex;
  flex-direction: column;

  ${ MaxWidth(breakpoints.collapseFooter)`
    .footer-content {
      display: block;
      .footer-contact { 
        margin-top: 20px;
      }
    }
  `}
`

export const StyledWarning = styled.div`
  background: #214679;
  color: white;
  padding: 10px 5px;
  font-size: 0.8rem;
  line-height: 1rem;
  box-shadow: 0px -1px 1px rgba(0,0,0,.2);
  p { 
    margin: 0;
    line-height: 1.6rem;
  };
  z-index: 90;
`

export const StyledFooter = styled.footer`
  background-color: ${ colors.bgFooter };
  color: white;
  flex: 1;
  display: flex;
  align-items: center;
  padding: 15px 10px;
  z-index: 90;

  .footer-content {
    flex: 1;
    display: flex;
    justify-content: space-between;
    align-items: center;
    max-width: 1300px;
    margin: 0 auto;

    .footer-contact {
      flex: 1 0 200px;
    }
  }
  ${ MaxWidth(breakpoints.collapseFooter)`
    position: static;
    height: auto;
    .footer-content {
      display: block;
      .footer-contact { 
        margin-top: 10px;
      }
    }
  `}
`;

export const StyledLinksList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  flex: 1;
    
  li {
    display: inline;
    margin: 2px 3%;
    vertical-align: top;
    width: 60%;
    ${
      MaxWidth(500)`
        margin: 2px 2%;
        width: 46%;
      `
    }
    a {
      color: white;
      text-decoration: none;
      font-size: .9rem;
      line-height: 1rem;
      transition: .2s color;
      &:hover {
        color: ${ colors.lightBorder };
      }
    }
  }
`

export const StyledCopyright = styled.p`
  color: ${ colors.textFooter };
  font-size: .9rem;
  margin: 10px 0 0;
  text-align: right;
  ${
    Media.medium`
      text-align: center;
    `
  }
`
