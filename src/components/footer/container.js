import React from 'react'
import { Link } from 'react-router-dom'
import { StyledFooterContainer, StyledWarning, StyledFooter, StyledLinksList, StyledCopyright } from './styles'
import SocialNetworks from './social-networks';
import Button from '@material-ui/core/Button';

import Modal from '@material-ui/core/Modal';


const year = new Date().getFullYear()
const Footer = () => (
  <StyledFooterContainer>
    <StyledFooter>
      <div className="footer-content">
        <StyledLinksList>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/about">About Us</Link></li>
          <li><Link to="/faq">FAQ</Link></li>
          <li><Link to="/for-providers">For Clinicians</Link></li>
          <li><Link to="/blog">Blog</Link></li>
          <li><Link to="/newsletter">Newsletter Signup</Link></li>
          
        </StyledLinksList>
        <div className="footer-contact">
          <SocialNetworks />
          <StyledCopyright>
            Copyright © { year } - SeeThru
          </StyledCopyright>
        </div>
      </div>
    </StyledFooter>
        <StyledWarning>
      <p>
        If you are in a crisis or in danger, find a treatment center or emergency room <a href="https://findtreatment.samhsa.gov/" rel="noopener noreferrer" target="_blank">here</a> or dial 9-1-1 for immediate help.
      </p>
    </StyledWarning>
  </StyledFooterContainer>
)

export default Footer