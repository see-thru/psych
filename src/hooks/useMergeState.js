import { useState } from 'react'

export const useMergeState = (initialState = {}) => {
  const [state, setState] = useState(initialState);
  const setMergedState = newState => {
    setState({
      ...state,
      newState
    })
  };
  return [state, setMergedState];
}

export const useToggleState = (initialState = {}) => {
  const [state, setState] = useState(initialState)
  const toggleField = field => {
    setState({
      ...state,
      [field]: !state[field]
    })
  }
  return [state, toggleField]
}

export const useToggleField = (initialValue = false) => {
  const [state, setState] = useState(initialValue)
  const toggle = () => setState(!state)
  return [state, toggle]
}