import { useState } from 'react'

export const useTextField = (initialState = '') => {
  const [value, setValue] = useState(initialState);
  const handleChange = e => setValue(e.target.value)
  return [value, handleChange]
}

// https://upmostly.com/tutorials/using-custom-react-hooks-simplify-forms/
export const useForm = (callback) => {
  const [values, setValues] = useState({});

  const handleSubmit = (event) => {
    if (event) { 
      event.preventDefault();
    }
    callback();
  };

  const handleChange = (event) => {
    event.persist();
    setValues(values => ({ ...values, [event.target.name]: event.target.value }));
  };

  const clearForm = () => setValues({});

  return {
    handleChange,
    handleSubmit,
    values,
    clearForm
  }
};