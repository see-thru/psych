import * as HOSTS from '../constants/hosts'

const configPsych = {
  apiKey: "AIzaSyDmQJma6QVmcns9-e6os626tMahenDGyLY", // Functions API Key
  authDomain: "seethru-psych.firebaseapp.com",
  databaseURL: "https://seethru-psych.firebaseio.com",
  projectId: "seethru-psych",
  storageBucket: "seethru-psych.appspot.com",
  messagingSenderId: "119834320561"
}

const configDev = {
  apiKey: "AIzaSyAz-nO0c-JSPPUAEBFGqYdG6S3BVqhwzeo", // Functions API Key
  projectId: "seethru-dev",
  authDomain: "seethru-dev.firebaseapp.com",
  databaseURL: "https://seethru-dev.firebaseio.com",
  storageBucket: "seethru-dev.appspot.com",
  messagingSenderId: "64013815615"
}


const getFirebaseConfig = (baseUrl = '') => {
  switch (baseUrl) {
    case HOSTS.PROD:
    case HOSTS.PROD2:
      return configPsych
    default:
      return configDev
  }
}

export default getFirebaseConfig
