/** 
 * Initialize firebase app, and export firebase services for use by the actions 
 */

import firebase from 'firebase/app'
import 'firebase/auth'
import getFirebaseConfig from './config'
import { store } from '../store'
import { loggedIn, loggedOut } from '../actions/auth';

if ( !firebase.apps.length ) {
  firebase.initializeApp(getFirebaseConfig(window.location.host))
}

let auth = firebase.auth()
let authLoaded = false

const getAuthPersisted = persist => {
  var persistence = persist ? firebase.auth.Auth.Persistence.LOCAL : firebase.auth.Auth.Persistence.SESSION
  return auth.setPersistence(persistence).then(() => {
    auth = firebase.auth()
    return auth
  })
}

const getCurrentUser = () => { // async, to avoid initialization nulls
  return new Promise(r => {
    if(authLoaded) {
      r(auth.currentUser)
    } else {
      let unsubscribe = auth.onAuthStateChanged(user => {
        unsubscribe()
        r(user)
      })
    }
  })
}

auth.onAuthStateChanged(user => { // handles initial load
  authLoaded = true
  store.dispatch(user ? loggedIn() : loggedOut())
})

export { auth, getAuthPersisted, getCurrentUser }