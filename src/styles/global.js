import { createGlobalStyle, css } from 'styled-components'
import { colors, fonts, values } from '../utils/settings'
import Media, { MinWidth, MinMax } from '../utils/media'

const fontDefs = css`
  // @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800');
`
const body = css`
  *, 
  *:after, 
  *:before {
    box-sizing: border-box; 
  }
  html {
    position: relative;
    min-height: 100%;
  }
  body {
    background-color: ${ colors.bgColor };
    font-family: ${ fonts.default }, sans-serif;
    font-size: ${ fonts.fontSize };
    line-height: ${ fonts.lineHeight };
    margin: 0;
    padding: ${ values.headerHeight } 0 0;
    &.shadow-block:before {
      ${
        Media.small`
          background-color: rgba(109, 132, 146, 0.8);
          content: '';
          height: 100%;
          left: 0;
          top: 0;
          position: fixed;
          width: 100%;
          z-index: 1;        
        `
      }
    }
  }
  h1, h2, h3, h4, h5 {
    // font-family: 'Montserrat', sans-serif;
  }
  button, a,
  a:hover,
  a:active, li a:active,
  a:visited, li a:visited, .btn:visited,
  a:focus, li a:focus {
    // border: none;
    // outline: none;
    text-decoration: none;
  }
  button {
  //   background-color: transparent;
    cursor: pointer;
  //   font-size: 1rem;
  //   font-family: ${ fonts.default }, sans-serif;
  }
  p {
    margin-bottom: .5rem;
  }
  #intercom-container {
    z-index: 9 !important;
  }  

  .full-height-container {
    display: flex;
    flex-direction: column;
    min-height: calc(100vh - ${values.headerHeight});

    main {
      flex: 1;
      // padding: 0 10px;
      // display: flex;
    }
  }
`

const custom = css`
  .ReactModal__Overlay {
    -webkit-perspective: 600;
    perspective: 600;
    opacity: 0;
    overflow-x: hidden;
    overflow-y: auto;
  }

  .ReactModal__Overlay--after-open {
    opacity: 1;
    transition: opacity 150ms ease-out;
  }

  .ReactModal__Body--open {
    overflow-y: hidden;
  }

  .ReactModal__Content {
    -webkit-transform: scale(0.5) rotateX(-30deg);
    transform: scale(0.5) rotateX(-30deg);
    ${ MinWidth(576)`
      max-width: 500px;
    `}
    &.modal-lg {
      ${ MinWidth(850)`
        max-width: 800px;
      `}
      ${ MinMax(576, 849)`
        max-width: initial;
        min-height: calc(100% - (.5rem * 2));
        margin: .5rem;
      `}
    }
  }


  .ReactModal__Content--after-open {
    -webkit-transform: scale(1) rotateX(0deg);
    transform: scale(1) rotateX(0deg);
    transition: all 150ms ease-in;
  }

  .ReactModal__Overlay--before-close {
    opacity: 0;
  }

  .ReactModal__Content--before-close {
    -webkit-transform: scale(0.5) rotateX(30deg);
    transform: scale(0.5) rotateX(30deg);
    transition: all 150ms ease-in;
  }

  .ReactModal__Content.modal-dialog {
    border: none;
    background-color: transparent;
  }

  .base-service-tooltip {
    font-size: .8rem;
    line-height: 1rem;
  }

  .section-label {
    font-size: .9rem;
    font-weight: 800;
    color: ${ colors.blue };
  }
  .toggle-row {
    display: flex;
    justify-content: space-between;
    cursor: pointer;
    svg {
      color: ${ colors.iconGray };
    }
  }
`

export const GlobalStyle = createGlobalStyle`
  ${fontDefs}
  ${body}
  ${custom}
`