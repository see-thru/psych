import * as types from '../constants/blog'

const initialState = {
    blogPosts: [],
    error:"",
    loading:false,
}

const BlogReducer = (state = initialState, action) => {
    switch ( action.type ) {
        case types.GET_BLOGS_LOADING:
            return {
                ...state,
                loading:true,
            }
        case types.GET_BLOGS_SUCCESS:
            return {
                blogPosts: action.data,
                error:"",
                loading:false
            }
        case types.GET_BLOGS_FAIL:
            return{
                ...state,
                error: action.error,
                loading:false
            }
        default:
            return state
    }
}

export default BlogReducer