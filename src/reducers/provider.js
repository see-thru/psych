import * as types from '../constants/provider'
import { isArrayLength } from '../utils/arrays';
import update from 'immutability-helper';

const initialState = {
  data: undefined,
  services: undefined,
  error: undefined,
  loading: false,
  drawerOpen: false,
  appointment: undefined,
}

const ProviderReducer = (state = initialState, action) => {
  switch ( action.type ) {
    case types.PROVIDER_LOADING:
      return { 
        ...state,
        ...initialState,
        loading: true
      }
    case types.SET_PROVIDER:
      return { 
        ...state, 
        data: action.data,
        loading: false
      }
    case types.SET_PROVIDER_SERVICES:
      return { 
        ...state, 
        services: action.data,
        // drawerOpen: isArrayLength(action.data)
      }
    case types.TOGGLE_SERVICE:
      if(!Number.isInteger(action.index) || state.services[action.index].locked){ 
        return state
      }
      const services = update(state.services, {[action.index]: {
        $apply: service => ({ ...service, selected: !service.selected })
      }});
      return {
        ...state,
        services,
        drawerOpen: isArrayLength(services)
      }
    case types.TOGGLE_DRAWER:
      return {
        ...state,
        drawerOpen: !state.drawerOpen
      }
    case types.PROVIDER_FAILED:
      return { 
        ...state, 
        error: action.error,
        loading: false
      }
    case types.SET_APPOINTMENT:
      return {
        ...state,
        appointment: action.appointment
      }
    default:
      return state
  }
}

export default ProviderReducer