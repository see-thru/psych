import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { createResponsiveStateReducer } from 'redux-responsive'
import { breakpoints } from '../utils/media'
import AuthReducer from './auth'
import SearchReducer from './search'
import FiltersReducer from './filters'
import ProviderReducer from './provider'
import AppointmentsReducer from './appointments'
import BlogReducer from './blog'

export default combineReducers({
  AuthReducer,
  SearchReducer,
  FiltersReducer,
  ProviderReducer,
  AppointmentsReducer,
  BlogReducer,
  form: formReducer,
  browser: createResponsiveStateReducer(breakpoints)
})
