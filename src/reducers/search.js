import * as types from '../constants/search'
import combineSearchData from '../utils/combine-received-data'

const initialState = {
  results: [],
  additionalResults: [],
  loading: false,
  // error,
  // hoveredMarker,
}

const SearchReducer = (state = initialState,  action) => {
  switch ( action.type ) {
    case types.SEARCH_LOADING:
      return { 
        ...state,
        loading: true,
        noMoreResults: false
      }
    case types.SET_SEARCH_RESULTS:
      let results = action.clearResults ? action.providers : combineSearchData(action.providers, state.results)
      return {
        results,
        additionalResults: action.additionalProviders || [],
        noMoreResults: action.noMoreResults,
        loading: false,
        error: undefined
      }
    case types.SEARCH_FAILED:
      return { 
        ...state, 
        // data: [],
        noMoreResults: true,
        loading: false,
        error: action.error
      }
    default:
      return state
  }
}

export default SearchReducer