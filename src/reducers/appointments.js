import * as types from '../constants/appointments'

// { data: {}, error: undefined, loading: false }
const initialState = {
  appointments: [],
  booking: {}, // { loading, error }
  checkIn: {} // { data, loading, error }
}

const AppointmentsReducer = (state = initialState, action) => {
  switch ( action.type ) {
    case types.BOOKING_LOADING:
      return {
        ...state,
        booking: { loading: true }
      }
    case types.BOOKING_FAILED:
      return {
        ...state,
        booking: { error: action.error }
      }
    case types.BOOKING_DONE:
      return {
        ...state,
        booking: {}
      }
    // case types.APPOINTMENTS_LOADING:
    //   return {
    //     ...state,
    //     appointments: { loading: true }
    //   }
    // case types.APPOINTMENTS_FAILED:
    //   return {
    //     ...state,
    //     appointments: { error: action.error }
    //   }
    case types.SET_APPOINTMENTS:
      return {
        ...state,
        appointments: action.data
      }
    case types.CHECK_IN_LOADING: 
      return {
        ...state,
        checkIn: { loading: true }
      }
    case types.CHECK_IN_FAILED: 
      return {
        ...state,
        checkIn: { error: action.error }
      }
    case types.CHECK_IN_DONE: 
      return {
        ...state,
        checkIn: { data: action.data }
      }
    // case types.BOOK_AN_APPOINTMENT_SUCCESSFULLY:
    //   error = null
    //   booked = action.data
    //   loading = false
    //   return { 
    //     ...state,
    //     error,
    //     booked,
    //     loading
    //   }   
    // case types.PATIENT_PHOTO_PENDING: 
    //   loading = true 
    //   return {
    //     ...state,
    //     loading
    //   }
    // case types.PATIENT_PHOTO_REJECTED: 
    //   loading = false 
    //   return {
    //     ...state,
    //     loading
    //   }
    // case types.PATIENT_PHOTO_SUCCESSFULLY:
    //   data = PutPatientPhotoInProvidersAppointments(
    //     state.data,
    //     action.url,
    //     action.patientId
    //   )
    //   loading = false 
    //   return {
    //     ...state,
    //     loading,
    //     data
    //   }    
    // case types.GET_BOOKING_PENDING: 
    //   booking.loading = true
    //   return {
    //     ...state,
    //     booking
    //   }
    // case types.GET_BOOKING_REJECTED: 
    //   booking = { 
    //     data: {},
    //     error: action.error,
    //     loading: false
    //   }
    //   return {
    //     ...state,
    //     booking
    //   }
    // case types.GET_BOOKING_SUCCESSFULLY: 
    //   booking = { 
    //     data: action.data,
    //     error: null, 
    //     loading: false
    //   }
    //   return {
    //     ...state,
    //     booking
    //   }      
    default:
      return state
  }
}

export default AppointmentsReducer