import * as types from '../constants/search'

const initialState = {
  state: '',
  searchText: '',
  tags: {},
  currentUsState: ''
}

const FiltersReducer = (state = initialState,  action) => {
  switch (action.type) {
    case types.SET_TAGS:
      return { 
        ...state,
        state: action.state,
        searchText: action.searchText,
        tags: action.tags || {}
      }
    case types.SET_US_STATE:
      return {
        ...state,
        currentUsState: action.state
      }
    case types.CLEAR_FILTERS:
      return {
        ...initialState
      }
    default:
      return state
  }
}

export default FiltersReducer