
// !! so that 0 doesn't get rendered by React
export const convertToUTC = date => (
  new Date(date.getTime() - date.getTimezoneOffset() * 60000)
)

// we keep the dates local (as per default JS.Date()), until saving them in the DB
export const compareDates = (d1, d2) => d1.getTime() === d2.getTime() // is .getTime() necessary?

const getAvailableTimes = (availabilities, day) => {
  return (availabilities[day] || []).map(a => ({ ...a })) // deep clone
}

export const getAvailableTimesFromDetails = (details, date, availabilities) => {
  // availabilities is a map of day -> array of times
  let availableTimes = getAvailableTimes(availabilities, date.getDay())
  let selectedTimes = details.filter(d => compareDates(d.date, date)).map(d => d.time)
  return selectedTimes.length ? 
    availableTimes.map(d => ({ ...d, selected: selectedTimes.indexOf(d.name) !== -1 })) : availableTimes
}