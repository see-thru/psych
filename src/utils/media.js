// these sizes are arbitrary and you can set them to whatever you wish
import { css } from 'styled-components'

export const breakpoints = {
  extraSmall: 500,
  small: 700,
  medium: 990,
  large: 1280,
  extraLarge: 1400,
  collapseSearch: 850,
  collapseFooter: 650
}

export const MinHide = size => css`
  @media (min-width: ${ size / 16}em){
    display: none;
  }
`

export const MaxHide = size => css`
  @media (max-width: ${ size / 16}em){
    display: none;
  }
`

export const MaxWidth = size => ( ...args ) => css`
  @media (max-width: ${ size / 16 }em) {
    ${ css(...args) }
  }
`

export const MinWidth = size => ( ...args ) => css`
  @media (min-width: ${ size / 16 }em) {
    ${ css(...args) }
  }
`

export const MinMax = (min, max) => ( ...args ) => css`
  @media (min-width: ${ min / 16 }em) and (max-width: ${ max / 16}em){
    ${ css(...args) }
  }
`

const Media = Object.keys(breakpoints).reduce((accumulator, label) => {
  accumulator[label] = MaxWidth(breakpoints[label])
  return accumulator
}, {})

export default Media
