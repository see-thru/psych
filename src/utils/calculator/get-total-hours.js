const GetTotalHours = (
  hoursPerWeek = 0,
  weeksPerYear = 0,
  decimals = 2
) => {
  if ( typeof hoursPerWeek !== 'number' ) return 'the `hoursPerWeek` parameter should be a number'
  if ( typeof weeksPerYear !== 'number' ) return 'the `weeksPerYear` parameter should be a number'
  if ( typeof decimals !== 'number' ) return 'the `decimals` parameter should be a number'

  const total = hoursPerWeek * weeksPerYear
  return Number(total.toFixed(decimals))
}

export default GetTotalHours