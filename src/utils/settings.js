export const colors = {
  text: '#333',
  link: '#4dace8',
  alert: '#bd2a2a',
  border: '#eee',
  bgDate: '#f7f7f7',
  bgFields: '#f7f7f7',
  textDate: '#999',
  bgFooter: '#214679',
  darkLink: '#23527c',
  faqTitle: '#007ab4',
  bgCounter: '#017AB5',
  textLight: '#666',
  bgUserNav: '#4DABE9',
  bgCulture: '#d5e1eb',
  textTitle: '#004679',
  textFooter: '#FFF',
  darkButton: '#004679',
  greenLight: '#86A838',
  lightBorder: '#F7F7F7',
  bgHowWeDoIt: '#395370',
  accentNavBar: '#03375f',
  textSubtitle: '#37AAE2',
  bgFilterWhat: '#00467A',
  bgFilterDark: '#003861',
  bgMissionBox: '#007ab4',
  schedulesDay: '#017AB5',
  bgCalendarTop: '#E24540',
  bgLightFooter: '#f3f3f3',
  successButton: '#4DABE9',
  lightHighlight: '#34bef3',
  homeBoxCreates: '#00aeef',
  homeBoxEnables: '#b2e300',
  navBarHoverLink: '#0984b3',
  homeBoxUtilizes: '#f9b841',
  borderDarkButton: '#0a3659',
  bgMapDescription: '#017AB5',
  borderMissionBox: '#00adee',
  borderLightFooter: '#b7b7b7',
  
  bgCalendarSelectDay: '#017AB5',
  textColorTitleQualifications: '#017AB5',
  textColorDescriptionQualifications: '#8D8D8D',
  // new values
  darkBlue: '#004679',
  blue: '#017AB5',
  lightBlue: '#00aeee',
  titleColor: '#017AB5', // blue
  bgColor: '#f7f8fa',
  placeholderFields: '#bbb',
  textMuted: '#888',
  iconGray: '#aaa',
  borderGray: '#eee',
  headerBackground: '#f7f7f7'
}

export const fonts = {
  default: 'Roboto',
  lineHeight: '1.5',
  weightBold: '700',
  fontSize: '1rem'
}

export const values = {
  headerHeight: '60px'
}