import ReactGA from 'react-ga'
import axios from 'axios'
import axiosWithAuth from '../api/axios-with-auth'
import urlGenerator from '../api/url-generator'
import { isArrayLength } from './arrays';

export const setGAUser = userId => ReactGA.set({ userId })

export const PLATFORM = {
  all: { FC: true, GA: true, ST: true },
  FC: { FC: true },
  GA: { GA: true },
  ST: { ST: true }
}

const trackAction = async (action, data = {}, ...platforms) => {
  if(!isArrayLength(platforms)) platforms = PLATFORM.all
  const { category, detail, link, label, value } = data
  if(platforms.FC){
    if(window.fcWidget.isLoaded){
      window.fcWidget.track(action, { detail, link })
    } else {
      console.error('fcWidget has not been initiated')
    }
  }
  if(platforms.GA){
    ReactGA.event({ action, category, label: label || detail, value })
  }
  if(platforms.ST){
    const ax = await axiosWithAuth().catch(() => axios)
    ax.post(urlGenerator('user/activity'), { action, detail, link })
  }
}

export default trackAction