import moment from 'moment'

const getAppointmentsByType = (
  results = [],
  type = 'upcoming'
) => {
  if ( ! Array.isArray(results) ) return 'the `results` parameter should be an array'
  let newResults
  if ( type === 'upcoming' ) {
    newResults = results.filter( x => moment(x.date).isAfter(moment(), 'day') )
  }
  if ( type === 'past' ) {
    newResults = results.filter( x => moment(x.date).isBefore(moment(), 'day') )
  }
  if ( type === 'today' ) {
    newResults = results.filter( x => moment(x.date).isSame(moment(), 'day') )
  } 
  return newResults
}

export default getAppointmentsByType