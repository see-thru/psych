
// !! so that 0 doesn't get rendered by React
export const isArrayLength = arr => Array.isArray(arr) && !!arr.length