import * as HOSTS from './hosts'

const getStripeApiKey = (baseUrl = '') => {
  switch (baseUrl) {
    case HOSTS.PROD: case HOSTS.PROD2:
      return process.env.REACT_APP_STRIPE_API_KEY_PROD
    default:
      return process.env.REACT_APP_STRIPE_API_KEY_DEV
  }
}

export const STRIPE_API_KEY = getStripeApiKey(window.location.host)