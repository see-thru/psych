const functions = require('firebase-functions')
const express = require('express')
const { Provider } = require('../helpers/schemas')
const queries = require('../helpers/queries')
const { asyncHandler } = require('../helpers/errors')
const { isProd } = require('../helpers/env')
const { markupService } = require('../helpers/utils')
const sendgrid = require('../clients/sendgrid')

// ProviderInfo fields:
const projection = {
  'providerInfo.name': 1,
  'providerInfo.degrees': 1,
  'providerInfo.specialty': 1,
  'providerInfo.secondarySpecialties': 1,
  'providerInfo.rating': 1,
  'providerInfo.photo': 1,
  'providerInfo.additionalPhotos': 1,
  'providerInfo.video': 1,
  'providerInfo.description': 1,
  'providerInfo.languages': 1,
  'providerInfo.education': 1,
  availabilities: 1,
  baseService: 1,
  location: 1,
  baseCost: 1,
  age: 1,
  insurances: 1,
  usStates: 1,
  modalities: 1,
  issues: 1,
  focuses: 1
}

const getProvider = asyncHandler(async (req, res) => {
  console.log(`GET /provider/${req.params.providerId}`)
  if(!req.params.providerId){
    res.status(400).send({ error: 'There is no Provider ID specified.'})
    return
  }
  let query = Provider.findById(req.params.providerId).select(projection).lean()
  if(isProd){
    query.where('active', true)
  }
  const provider = await query
  if(!provider){
    res.status(400).send({ error: 'There is no provider with that ID.'})
  }
  res.send(provider)
})

const getProviderServices = asyncHandler(async (req, res) => {
  console.log(`GET /provider/${req.params.providerId}/services`)
  if(!req.params.providerId){
    res.status(400).send({ error: 'There is no Provider ID specified.'})
    return
  }
  let services = await Provider.aggregate(queries.providerServices(req.params.providerId))
  services.forEach(s => { s.cost = markupService(s.cost) })
  res.send({ services })
  // if(services.length){
  // } else {
  //   return res.status(400).send({ error: 'There are no services for this provider.'})
  // }
})

const submitInfo = asyncHandler(async (req, res) => {
  console.log('POST /submitInfo', req.body)
  sendgrid.newProviderRequest(req.body)
  res.send('OK')
})

const router = express.Router()
router.get('/healthz', (req, res) => res.send({status: true}))
router.get('/provider/:providerId', getProvider)
router.get('/provider/:providerId/services', getProviderServices)
router.post('/submitInfo', submitInfo)
// .use(/(?:\/api\/provider)?/, router)
const app = express().use(['/api/provider',''], router)

exports = module.exports = functions.https.onRequest(app)