const functions = require('firebase-functions')
const ipstackKey = functions.config().ipstack.key
const express = require('express')
const rp = require('request-promise')
const { Provider, ObjectId } = require('../helpers/schemas')
const { stateAbbreviations } = require('../helpers/constants')
const { isProd } = require('../helpers/env')
const { asyncHandler } = require('../helpers/errors')
const { markupService, isArrayLength, log } = require('../helpers/utils')

const cleanSearch = searchText => {
  if(searchText){
    const common =  ["the","and","that","have","for","not","with","you","this","but","his","from","they","say","her","she","will","one","all","would","there","their","what","out","about","who","get","which","when","make","can","like","just","him","know","take","people","into","year","your","some","could","them","see","other","than","then","now","look","only","come","its","it's","over","think","also","back","after","use","two","how","our","first","well","way","even","want","because","any","these","give","most","none","really","don't","dont","didn't","didnt","sure","are","i'm","idk","etc","much","has","lot"]
    var pattern = /[\wÀ-ÿ'‘’]+/g
    // var pattern = /[\wÀ-ÿ'‘’\-]+/g
    var words = searchText.toLowerCase().match(pattern)
    if(words){
      return words.filter(word => word.length > 2 && !common.includes(word)).join(' ')
    }
  }
  return undefined
}

const searchProjection = {
  'providerInfo.name': 1,
  'providerInfo.degrees': 1,
  'providerInfo.specialty': 1,
  'providerInfo.rating': 1,
  'providerInfo.photo': 1,
  'providerInfo.description': 1,
  baseCost: 1,
  issues: 1
}

// issues, identifiers, age, insurances, modalities, focuses, specialties, experiences
function buildQuery(filters) {
  // create filters lookup
  let tags = filters.tags || {}
  let query = isProd ? { active: true } : {}
  if(filters.state){
    const usState = stateAbbreviations[filters.state]
    if(usState){
      query['usStates'] = usState
    }
  }
  if(isArrayLength(tags.identifiers)){
    query['identifiers'] = { $in: tags.identifiers }
  }
  if(isArrayLength(tags.insurances)){
    query['insurances'] = { $in: tags.insurances }
  }
  if(isArrayLength(tags.modalities)){
    query['modalities'] = { $in: tags.modalities }
  }
  if(isArrayLength(tags.age)){
    // TODO? using $gte and $lte, rather than range strings
    query['age'] = { $in: tags.age }
  }
  if(isArrayLength(tags.focuses)){
    query['focuses'] = { $in: tags.focuses }
  }
  if(isArrayLength(tags.specialties)){
    query['providerInfo.specialty'] = { $in: tags.specialties }
  }
  if(isArrayLength(tags.experiences)){
    query['experiences'] = { $in: tags.experiences }
  }
  const searchText = cleanSearch([filters.searchText || ''].concat(tags.issues || []).join(' '))
  if(searchText){
    console.log(`searchText: ${searchText}`)
    query['$text'] = { $search: searchText }
  }
  return query
}

function createCondition(ifExpression, value){
  return { $cond: { if: ifExpression, then: value, else: 0 }}
}

function buildUnfilteredAggregation(filters, providerIds){
  let $match = { 
    _id: { $nin: providerIds },
  }
  if(isProd){
    $match['active'] = true
  }
  if(filters.state){
    const usState = stateAbbreviations[filters.state]
    if(usState){
      $match['usStates'] = usState
    }
  }
  let tags = filters.tags || {}
  let conditions = []
  // Calculate a score based on matched filters. Then, sort by that.
  if(isArrayLength(tags.identifiers)){
    conditions.push(createCondition({ $in: ['$identifiers', tags.identifiers] }, 2))
  }
  if(isArrayLength(tags.insurances)){
    conditions.push(createCondition({ $in: ['$insurances', tags.insurances] }, 1))
  }
  if(isArrayLength(tags.modalities)){
    conditions.push(createCondition({ $in: ['$modalities', tags.modalities] }, 1))
  }
  if(isArrayLength(tags.age)){
    conditions.push(createCondition({ $in: ['$age', tags.age] }, 1))
  }
  if(isArrayLength(tags.focuses)){
    conditions.push(createCondition({ $in: ['$focuses', tags.focuses] }, 1))
  }
  if(isArrayLength(tags.specialties)){
    conditions.push(createCondition({ $in: ['$providerInfo.specialty', tags.specialties] }, 3))
  }
  if(isArrayLength(tags.experiences)){
    conditions.push(createCondition({ $in: ['$experiences', tags.experiences] }, 1))
  }
  const $project = Object.assign({ matchScore: { $add: conditions } }, searchProjection)
  return [{ $match }, { $project }, { $sort: { matchScore: -1 }}, { $limit: 10 }]
}

const doSearch = asyncHandler(async (req, res) => {
  console.log('POST /search', req.body)
  const filters = req.body || {}
  const skip = filters.skip || 0
  const limit = filters.limit || 10
  const query = buildQuery(filters)
  
  let providers = await Provider.find(query).select(searchProjection)
    .skip(skip).limit(limit).lean()
  let additionalProviders = []
  if(!skip && (providers.length < 6)){
    const providerIds = providers.map(p => ObjectId(p._id))
    const aggregation = buildUnfilteredAggregation(filters, providerIds)
    additionalProviders = await Provider.aggregate(aggregation)
    additionalProviders.forEach(p => { p.baseCost = markupService(p.baseCost); delete p.matchScore })
  }
  providers.forEach(p => { p.baseCost = markupService(p.baseCost) })
  return res.send({ providers, additionalProviders })
})

const getUsState = asyncHandler(async (req, res) => {
  // firebase functions assigns the client ip to the `fastly-client-ip` header
  const ipAddress = req.headers['fastly-client-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress
  console.log('GET /getUsState', ipAddress)
  if(ipAddress){
    if(ipAddress === '127.0.0.1'){
      return res.send({ state: 'Colorado' })
    }
    // todo; put this in functions env config
    const uri = `http://api.ipstack.com/${ipAddress}?access_key=${ipstackKey}&fields=region_code,region_name`
    const state = await rp({ uri, json: true })
      .then(response => response && response.region_name)
      .catch(e => { console.error('Error:', e); return undefined })
    // TODO: put the result in Redis cache?
    return res.send(state ? { state, ipAddress } : {})
  } else {
    console.error('No IP Address found in request', req.headers)
    return res.send({})
  }
})

const router = express.Router()
router.get('/healthz', (req, res) => res.send({status: true}))
router.post('/search', express.json(), doSearch)
router.get('/getUsState', getUsState)
const app = express().use(['/api/search',''], router)

exports = module.exports = functions.https.onRequest(app)