const functions = require('firebase-functions')
const { getFirebaseId, getFirebaseIdAllowEmpty } = require('../clients/admin')
const { User, Action } = require('../helpers/schemas')
const express = require('express')
const { validateUserFields } = require('../middleware/validateUserField')
const { asyncHandler } = require('../helpers/errors')
const sendgrid = require('../clients/sendgrid')

const getMe = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user.uid)
  res.send({ user })
})

const createMe = asyncHandler(async (req, res) => {
  console.log('POST /user/me', req.user, req.body)
  const userInfo = Object.assign({ _id: req.user.uid }, req.body)
  let user = new User(userInfo)
  user = await user.save()
  sendgrid.welcomePatient(user)
  return res.send({ user })
})
//TO DO
//  const demographics = asyncHandler(async (req, res) => {
//   console.log('PUT /user/demographics', req.user, req.body)
//   const userInfo = Object.assign({ _id: req.user.uid }, req.body)
//   let user = new User(userInfo)
//   user = await user.save()
//   sendgrid.welcomePatient(user)
//   return res.send({ user })
// })

const addActivity = asyncHandler(async (req, res) => {
  console.log('POST /user/activity', req.user, req.body)
  const actionInfo = Object.assign({ userId: req.user.uid }, req.body)
  let action = new Action(actionInfo)
  await action.save()
  res.send('OK')
})

const router = express.Router()
router.get('/healthz', (req, res) => res.send({status: true}))
router.get('/me', getFirebaseId, getMe)
router.post('/create', getFirebaseId, express.json(), validateUserFields, createMe)
// router.put('/demograpics', getFirebaseId, express.json(), demographics) TO DO
router.post('/activity', getFirebaseIdAllowEmpty, express.json(), addActivity)
const app = express().use(['/api/user',''], router)

exports = module.exports = functions.https.onRequest(app)