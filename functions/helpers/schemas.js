const mongoose = require('../clients/mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId;
const { isProd } = require('./env')

const ServiceSchema = mongoose.Schema({
  name: String,
  cost: Number,
  time: Number,
  includes: [String],
  isDefault: Boolean,
  category: String,
  adaCode: String,
  cptCode: String,
  description: String,
  updatedBy: String
}, {
  timestamps: true
})

const serviceRef = {
  _id: { type: ObjectId, ref: 'Service', required: true },
  name: String,
  cost: Number,
  time: Number
}

const ServiceListSchema = mongoose.Schema({
  name: String,
  services: [serviceRef],
  updatedBy: String
}, {
  timestamps: true
})

const availabilitiesRef = [{ from: Number, to: Number }]

const ProviderSchema = mongoose.Schema({
  providerInfo: {
    name: { type: String, required: true },
    practiceName: String,
    degrees: String,
    specialty: String,
    secondarySpecialties: [String],
    rating: Number,
    email: String,
    photo: { type: String, required: true },
    additionalPhotos: [String],
    video: String,
    phoneNumber: String,
    website: String,
    notes: String,
    npi: String,
    description: String,
    languages: [String],
    education: [String],
  },
  address: { type: String, required: true },
  location: {
    type: {
      type: String,
      enum: ['Point'],
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  },
  services:[serviceRef],
  serviceLists: [{ type: ObjectId, ref: 'ServiceList' }],
  availabilities: {
    0: availabilitiesRef, 1: availabilitiesRef, 2: availabilitiesRef, 3: availabilitiesRef, 4: availabilitiesRef, 5: availabilitiesRef, 6: availabilitiesRef
  },
  files: [String],
  baseCost: Number,
  // BH:
  age: String,
  gender: String,
  sexualities: [String],
  insurances: [String],
  usStates: [String],
  modalities: [String],
  issues: [String],
  tags: [String],
  focuses: [String],
  // Admin:
  active: Boolean,
  updatedBy: String
}, {
  timestamps: true
})

const UserSchema = mongoose.Schema({
  _id: String,
  firstName: String,
  lastName: String,
  email: String,
  dob: String,
  phone: String,
  updatedBy: String
}, {
  timestamps: true
})

const AppointmentSchema = mongoose.Schema({
  details: {
    times: [{
      date: String,
      time: String
    }],
    confirmedTime: Date
  },
  providerId: { type: ObjectId, ref: 'Provider' },
  patientId: { type: String, ref: 'User' },
  payment: {
    token: String,
    last4: String
  },
  stripeCustomer: String,
  services: [{
    _id: { type: ObjectId, ref: 'Service' }, // not required yet, like the other serviceRefs
    name: String,
    cost: Number,
    time: Number
  }],
  totalCost: Number,
  status: {
    type: String,
    enum: ['booked', 'checkedIn', 'confirmed']
  },
  videoLink: String,
  checkedInAt: Date,
  updatedBy: String
}, {
  timestamps: true
})

AppointmentSchema.virtual('provider', {
  ref: 'Provider',
  localField: 'providerId',
  foreignField: '_id',
  justOne: true
});

const ActionSchema = mongoose.Schema({
  userId: { type: String, ref: 'User' },
  action: String,
  detail: String,
  link: String
}, {
  timestamps: true
})

module.exports = { 
  Provider: mongoose.model('Provider', ProviderSchema),
  User: mongoose.model('User', UserSchema, isProd ? undefined : 'usersDev'),
  Appointment: mongoose.model('Appointment', AppointmentSchema, isProd ? undefined : 'appointmentsDev'),
  Service: mongoose.model('Service', ServiceSchema),
  ServiceList: mongoose.model('ServiceList', ServiceListSchema, 'serviceLists'),
  Action: mongoose.model('Action', ActionSchema, isProd ? undefined : 'actionsDev'),
  ObjectId: mongoose.Types.ObjectId
}
