const util = require('util')
module.exports = { 
  toNumber: num => {
    try {
      return Number(num)
    } catch(e){
      console.error('Couldnt parse number: ', num, e)
      return undefined
    }
  },
  log: (...args) => args.map(o => {
    console.log(util.inspect(o, { depth: null, colors: true, maxArrayLength: null }))
    return false
  }),
  prepare: model => model.toObject({ virtuals: true }),
  markupService: cost => {
    return cost ? Math.ceil(cost * 1.1) : cost
  },
  isArrayLength: arr => Array.isArray(arr) && Boolean(arr.length)
}