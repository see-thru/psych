const { ObjectId } = require('./schemas')

module.exports = {
  providerServices: providerId => [{ 
    $match: { _id: ObjectId(providerId) }
  }, { 
    $lookup: {
      from: 'serviceLists',
      let: { listIds: { $ifNull: [ '$serviceLists', [] ] } },
      pipeline: [{
        $match: {
          $expr: {
            $in: ['$_id', '$$listIds']
          }
        }
      }, {
        $unwind: '$services'
      },{
        $group: {
          _id: 0,
          // if we know there are no doubles, this will keep them sorted acc. to the list!
          listServices: { $push: '$services' } 
        }
      }, {
        $project: {
          _id: 0
        }
      }],
      as: 'listServices'
    }
  }, {
    $replaceRoot: { 
      newRoot: { $mergeObjects: [ "$$ROOT", { $arrayElemAt: ['$listServices', 0] } ]}
    }
  }, {
    $project: {
      _id: 0,
      service: { $concatArrays: [ { $ifNull: ['$services', []] }, '$listServices' ] }
    }
  }, {
    $unwind: '$service'
  }, {
    $lookup: {
      from: 'services',
      localField: 'service._id',
      foreignField: '_id',
      as: 'serviceObject'
    }
  }, {
    $replaceRoot: {
      newRoot: { $mergeObjects: [
        { $arrayElemAt: [ '$serviceObject', 0 ] },
        '$service'
      ]}
    }
  }, {
    $sort: { isDefault: -1 }
  }],

  searchProviders: () => [{
    
  }]
}