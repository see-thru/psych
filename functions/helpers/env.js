const isProd = process.env.GCP_PROJECT !== 'seethru-dev'
const helpers = {
  isProd,
  getStripeKey: () => {
    const stripeKeyDev = process.env.STRIPE_KEY_DEV
    const stripeKeyProd = process.env.STRIPE_KEY_PROD
    return isProd ? stripeKeyProd : stripeKeyDev
  }
}

module.exports = helpers
