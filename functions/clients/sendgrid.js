const sgMail = require('@sendgrid/mail')
const sgKey = require('firebase-functions').config().sendgrid.key
const isProd = require('../helpers/env').isProd

sgMail.setApiKey(sgKey)

const sendgrid = {
  sendEmail: (subject, text, html) => {
    return sgMail.send({
      to: 'vip@seethru.healthcare',
      from: {
        name: 'SeeThru App',
        email: 'app@seethru.healthcare',
      },
      subject,
      text: text || undefined,
      html: html || undefined
    })
  },
  bookingAlert: (data) => {
    const {
      appointmentId,
      patientName,
      patientEmail,
      patientPhone,
      providerName,
      providerPhone,
    } = data

    console.log(`DATA: ${JSON.stringify(data)}`)
    const link = `https://admin.seethru.healthcare/appointments/${appointmentId}/show`
    const env = isProd ? 'psych' : 'dev'
    const msg = {
      to: `vip@seethru.healthcare`,
      from: 'app@seethru.healthcare',
      text: 'New Booking Request',
      html: '<p>New Booking Request</p>',
      templateId: 'd-5b3cd74eb6004cb2abeecf7858ee0804',
      dynamic_template_data: {
        subject: `New Booking Request`,
        appointmentId: `${appointmentId}`,
        patientName: `${patientName}`,
        patientEmail: `${patientEmail}`,
        patientPhone: `${patientPhone}`,
        providerName: `${providerName}`,
        providerPhone: `${providerPhone}`,
        link: `${link}`,
        env: `${env}`
      },
    };
    sgMail.send(msg).catch(error => {
      console.error(error.toString());
    })
    // const msg = `Clinician: ${clinitianName} <br />Patient: ${patientName} <br/>Details: ${details} <br />View Appointment: <a href="${link}#${env}">${link}</a>`
    // return sendgrid.sendEmail('New Appointment', undefined, msg).catch(console.error)
  },
  // bookingConfirmationPatient: (email, name, clinician, details ) => {
  //   const msg = {
  //     to: email,
  //     from: 'team@seethru.healthcare',
  //     subject: 'Booking Request Confirmed',
  //     text: `Your booking request with ${clinician} on {details.times.date} at ${details.times.time} was recieved. Our team is working to get final confirmation and will reach out in the next buisness day.`,
  //     html: '<p>Hello HTML world!</p>',
  //     templateId: 'd-f43daeeaef504760851f727007e0b5d0',
  //     dynamic_template_data: {
  //       subject: 'Testing Templates',
  //       name: 'Some One',
  //       city: 'Denver',
  //     },
  //   };
  //   sgMail.send(msg);
  // },
  // bookingConfirmationClinician: () => {

  // },
  welcomePatient: (user) => {
    const {
      firstName,
      email
    } = user
    const msg = {
      to: `${email}`,
      from: 'team@seethru.healthcare',
      text: 'Welcome to SeeThru',
      html: '<p>Welcome to SeeThru!</p>',
      templateId: 'd-9c446af589f34b9e8dc8f1fcdfc07612',
      dynamic_template_data: {
        subject: `{{name}}, welcome to SeeThru`,
        name: `${firstName}`
      },
    };
    sgMail.send(msg).catch(error => {
      console.error(error.toString())
    });
  },
  newProviderRequest: data => {
    const {
      firstName,
      lastName,
      email,
      specialty,
      zip,
      phoneNumber,
      notes
    } = data
    const msg = `
    First Name: ${ firstName }
    Last Name: ${ lastName }
    Email: ${ email }
    Specialty: ${ specialty }
    Zip: ${ zip }
    Phone: ${ phoneNumber }
    `
    return sendgrid.sendEmail('New Provider Request', msg).catch(console.error)
  }
}

module.exports = sendgrid