const mongo_user = process.env.MONGO_USER;
const mongo_pass = process.env.MONGO_USER;
const mongo_token = process.env.MONGO_TOKEN
const mongo_db = process.env.MONGO_DB;


const uri = `mongodb://${mongo_user}:${mongo_pass}@cluster0-shard-00-00-${mongo_token}.gcp.mongodb.net:27017,cluster0-shard-00-01-${mongo_token}.gcp.mongodb.net:27017,cluster0-shard-00-02-${mongo_token}.gcp.mongodb.net:27017/${mongo_db}?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin`
const mongoose = require('mongoose')

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(uri, { useNewUrlParser: true }).then(() => {
  console.log('Successfully connected to the database');
  return
}).catch(err => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

module.exports = mongoose
