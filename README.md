This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

### Commands:

* `yarn` installs modules and dependencies.
* `yarn start` runs the app in development mode.
  - Run `firebase serve --only functions` in another terminal tab to run the backend API
* `yarn build` Builds the app for production to the build folder. It correctly bundles React in production mode and optimizes the build for the best performance.
* `yarn test` Runs the test watcher in an interactive mode.
* `yarn eject` If you aren’t satisfied with the build tool and configuration choices, you can eject at any time. This command will remove the single build dependency from your project. Note: this is a one-way operation. Once you eject, you can’t go back!
* `yarn deploy` deploys React app to firebase with currently selected mode (`firebase use`).
* `firebase deploy --only functions` deploys backend API to firebase with currently selected mode
* `yarn deploy-all` deploys React app and backend API to firebase with currently selected mode
* `yarn deploy-dev`/`yarn deploy-prod` deploys React app to firebase with respective modes.

### /src is the front-end react app

* `actions` Actions - Redux (Documentation here: https://redux.js.org/basics/actions)
* `api` connecting to the APIs
* `components` we have three types of components:
  * `Styled` We're using the styled-components library (Documentation here: https://github.com/styled-components/styled-components).
  * `Container` is a class component (javaScript class): generally contains the logic, for example, call to async actions, connect to redux, create state components, etc. All files prefixed with the container- word.
  * `Stateless` component. They are pure components that has no state and returns the same markup given the same props. In fact, each container component has at least a Stateless component.
* `constants` values that not changes. For example: endpoints, google analytics id, etc.
* `firebase` configuration and auth async actions.
* `reducers` specify how the application's state changes in response to actions sent to the store. Remember that actions only describe what happened, but don't describe how the application's state changes. (Documentation here: https://redux.js.org/basics/reducers)
* `utils` contains scripts that help separate the logic from the components

### /functions is the back-end API
* `*.f.js` files will be automatically compiled into their own functions
