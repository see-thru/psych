// scrap paper for mongo shell

// Update provider.baseCost field
db.providers.aggregate([{ 
  $lookup: {
    from: 'serviceLists',
    let: { listIds: { $ifNull: [ '$serviceLists', [] ] } },
    pipeline: [{
      $match: {
        $expr: {
          $in: ['$_id', '$$listIds']
        }
      }
    }, {
      $unwind: '$services'
    },{
      $group: {
        _id: 0,
        // if we know there are no doubles, this will keep them sorted acc. to the list!
        listServices: { $push: '$services' } 
      }
    }, {
      $project: {
        _id: 0
      }
    }],
    as: 'listServices'
  }
}, {
  $replaceRoot: {
    newRoot: { $mergeObjects: [ "$$ROOT", { $arrayElemAt: ['$listServices', 0] } ]}
  }
}, {
  $project: {
    'providerInfo.name': 1,
    services: { $concatArrays: [ { $ifNull: ['$services', []] }, '$listServices' ] }
  }
}, {
  $lookup: {
    from: 'services',
    let: { serviceIds: {
      $map: {
        input: '$services',
        as: 'service',
        in: '$$service._id'
      }
    }},
    pipeline: [{
      $match: {
        $expr: {
          $and: [
            { $in: ['$_id', '$$serviceIds'] },
            { $eq: ['$isDefault', true ] }
          ]
        }
      }
    }, {
      $limit: 1
    }, {
      $project: {
        _id: 1,
        cost: 1
      }
    }],
    as: 'defaultService'
  }
}, { // this is not taking into account the overrides
  $match: {
    defaultService: { $size: 1 }
  }
}]).forEach(p => {
  let defaultService = p.defaultService[0]
  let service = p.services.find(s => s._id.equals(defaultService._id))
  let baseCost
  if(service.hasOwnProperty('cost')){
    baseCost = service.cost
  } else if(defaultService.hasOwnProperty('cost')){
    baseCost = defaultService.cost
  }
  if(baseCost !== undefined){
    db.providers.updateOne({_id: p._id}, { $set: { baseCost }})
  }
})


db.providers.createIndex({
  issues: 'text',
  tags: 'text',
  'providerInfo.name': 'text',
  'providerInfo.specialty': 'text',
  'providerInfo.description': 'text',
  focuses: 'text',
  issues: 'text',
  sexualities: 'text'
}, { 
  weights: {
    'issues': 6,
    'tags':3,
    'providerInfo.specialty': 1,
    'providerInfo.description': 1,
    'focuses': 2,
    issues: 2,
    sexualities: 1
  },
  name: 'providers_text'
})

db.providers.dropIndex()

db.providers.createIndex( { location : "2dsphere" } )

db.providers.update({ "_id" : ObjectId("5c06b2d0c6a37f644163abf6")}, { $set: { // Ryan Carroll
  availabilities: {
    1: [
      { name: 'Morning', description: '9am-12pm' },
      { name: 'Afternoon', description: '12pm-5pm' },
      { name: 'Evening', description: '5pm-8pm' }
    ],
    2: [
      { name: 'Morning', description: '9am-12pm' },
      { name: 'Afternoon', description: '12pm-5pm' },
      { name: 'Evening', description: '5pm-8pm' }
    ],
    3: [
      { name: 'Morning', description: '9am-12pm' },
      { name: 'Afternoon', description: '12pm-5pm' },
      { name: 'Evening', description: '5pm-8pm' }
    ],
    4: [
      { name: 'Morning', description: '9am-12pm' },
      { name: 'Evening', description: '5pm-8pm' }
    ],
    5: [
      { name: 'Morning', description: '9am-12pm' },
      { name: 'Afternoon', description: '12pm-5pm' }
    ]
  }
}})

db.providers.find({
  gender: 'female',
  sexualities: {
    $in: ['lesbian', 'Bisexual']
  },
  active: true
})




var q = { 
  sexualities: { '$in': [ 'Gay', 'Lesbian' ] },
  insurances: { '$in': [ 'aetna', 'cigna' ] },
  age: { '$in': [ '30-40' ] },
}

{ usStates: 'NY',
  gender: { '$in': [ 'Female', 'Male' ] },
  sexualities: { '$in': [ 'Bisexual' ] },
  age: { '$in': [ '20-30', '30-40' ] },
  '$text': { '$search': 'test' } }

// related results:
db.providers.aggregate([{
  '$match': { 
    _id: { 
      '$nin': [ ObjectId("5bfdccf1baae0d2b1503da7f"), ObjectId("5bfdccf1baae0d2b1503da81") ] 
    },
    usStates: 'NY' 
  } 
}, { 
  '$project': {
    'providerInfo.name': 1,
    'providerInfo.degrees': 1,
    'providerInfo.specialty': 1,
    'providerInfo.rating': 1,
    'providerInfo.photo': 1,
    'providerInfo.description': 1,
    baseCost: 1,
    issues: 1,
    tagsMatched: {
      $add: [
        { $cond: { if: { $in: ['$gender', ['Female', 'Male']] }, then: 2, else: 0 }},
      ]
    }
  }
}, {
  $sort: { tagsMatched: -1 }
},{
  '$limit': 10 
}])